<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
#URL::forceScheme('https');//para navegar siempre por https://

Route::get('/', function () {
    return redirect()->to('home');
});

Route::get('alerta1', function () {
  $email = new App\Http\Controllers\EmailController();
  $email->alerta1();
});
Route::get('alerta2', function () {
  $email = new App\Http\Controllers\EmailController();
  $email->alerta2();
});
Route::get('alerta3', function () {
  $email = new App\Http\Controllers\EmailController();
  $email->alerta3();
});
Route::get('alerta4', function () {
  $email = new App\Http\Controllers\EmailController();
  $email->alerta4();
});
Route::get('alerta5', function () {
  $email = new App\Http\Controllers\EmailController();
  $email->alerta5();
});
Route::get('mensaje2', function () {
  $email = new App\Http\Controllers\EmailController();
  $email->mensaje2();
});
Route::get('mensaje3', function () {
  $email = new App\Http\Controllers\EmailController();
  $email->mensaje3();
});
Route::get('mensaje4', function () {
  $email = new App\Http\Controllers\EmailController();
  $email->mensaje4();
});
Route::get('mensaje5/{id}', function ($id) {
  $email = new App\Http\Controllers\EmailController();
  $email->mensaje5($id);
});
Route::get('mensaje6/{id}', function ($id) {
  $email = new App\Http\Controllers\EmailController();
  $email->mensaje6($id);
});
Route::get('mensaje7', function () {
  $email = new App\Http\Controllers\EmailController();
  $email->mensaje7();
});

Route::get('config',[
      'as'=>'config' ,
      'middleware' => 'auth',
      'uses' => 'ConfigController@index'
  ]);
Route::post('admin/config/update/{id}',[
      'as'=>'admin.config.update' ,
      'middleware' => 'auth',
      'uses' => 'ConfigController@update'
  ]);
Route::post('encuesta/responder',[
      'as'=>'encuesta.responder' ,
      'middleware' => 'auth',
      'uses' => 'EncController@responder'
  ]);
Route::get('push',[
      'as'=>'push' ,
      'middleware' => 'auth',
      'uses' => 'PushController@push'
  ]);

Route::get('admin/asignacion/seguimiento/{id}/edit2',[
      'as'=>'admin.asignacion.seguimiento.edit2' ,
      'middleware' => 'auth',
      'uses' => 'SegController@edit2'
  ]);

Route::post('mensaje/store',[
        'as'=>'mensaje.store' ,
        'uses' => 'MensajeController@store'
    ]);

Route::get('mensaje/show/{id}',[
        'as'=>'mensaje.show' ,
        'uses' => 'MensajeController@show'
    ]);
Route::get('qrcode/create/{id}',[
        'as'=>'qrcode.create' ,
        'uses' => 'PdfController@qrcode'
    ]);

Route::post('mensaje/show/{id}',[
        'as'=>'mensaje.show' ,
        'uses' => 'MensajeController@show'
    ]);

Route::post('estado_doc/{id}/{asigc_id}',[
        'as'=>'estado_doc' ,
        'uses' => 'HomeController@estado_doc'
    ]);

Route::resource('links', 'LinksController');

Auth::routes();

Route::get('codigo', function () {
    return redirect()->to('home');
});

Route::post('codigo',[
    'as'=>'codigo' ,
    'uses' => 'CodigosController@codigo_token'
]);

Route::get('/welcome',[
    'as'=>'welcome', function () { return redirect()->to('home'); }
]);

Route::post('/home',[
    'as'=>'home' ,
    'uses' => 'HomeController@index'
]);

Route::get('/home',[
    'as'=>'home' ,
    'uses' => 'HomeController@index'
]);

Route::get('/admin', function () { return view('admin'); });
Route::get('/reportes', [
    'as'=>'reportes' ,
    'uses' => function () { return view('reportes.index'); }
]
);
Route::get('/reportes/buscar',[
    'as'=>'reportes.buscar' ,
    'uses' => 'PdfController@buscar'
]
);
Route::post('/reportes/buscar',[
    'as'=>'reportes.buscar.post' ,
    'uses' => 'PdfController@buscar'
]
);

Route::get('rol/{id}',[
        'as'=>'rol' ,
        'middleware' => 'auth',
        'uses' => function ($id) {
        $user = \App\User::find(Auth::user()->id);
        $user->rol = $id;
        $user->save();
        return redirect()->back();//->to('home');
        }
    ]);
Route::get('modo_escritorio/{id}',[
        'as'=>'modo_escritorio' ,
        'middleware' => 'auth',
        'uses' => function ($id) {
        $user = \App\User::find(Auth::user()->id);
        $user->modo_escritorio = $id;
        $user->save();
        return redirect()->back();//->to('home');
        }
    ]);

Route::group(['prefix' => 'admin', 'as'=>'admin.'], function () {
    Route::resources([
        'usuarios' => 'UserController'
    ]);
    Route::get('usuarios/{id}/delete',[
        'as'=>'usuarios.delete' ,
        'uses' => 'UserController@destroy'
    ]);
    Route::post('usuarios/{id}',[
        'as'=>'usuarios.update' ,
        'uses' => 'UserController@update'
    ]);
    
    
    Route::resources([
        'facultad' => 'FacController'
    ]);
    Route::get('facultad/{id}/delete',[
        'as'=>'facultad.delete' ,
        'uses' => 'FacController@destroy'
    ]);
    Route::post('facultad/{id}',[
        'as'=>'facultad.update' ,
        'uses' => 'FacController@update'
    ]);
    Route::resources([
        'periodo' => 'PeriodoController'
    ]);
    Route::get('periodo/{id}/delete',[
        'as'=>'periodo.delete' ,
        'uses' => 'PeriodoController@destroy'
    ]);
    Route::post('periodo/{id}',[
        'as'=>'periodo.update' ,
        'uses' => 'PeriodoController@update'
    ]);
    
     Route::resources([
        'departamento' => 'DeptoController'
    ]);
    Route::get('departamento/{id}/delete',[
        'as'=>'departamento.delete' ,
        'uses' => 'DeptoController@destroy'
    ]);
    Route::post('departamento/{id}',[
        'as'=>'departamento.update' ,
        'uses' => 'DeptoController@update'
    ]);
    
    Route::resources([
        'programa' => 'ProgController'
    ]);
    Route::get('programa/{id}/delete',[
        'as'=>'programa.delete' ,
        'uses' => 'ProgController@destroy'
    ]);
    Route::post('programa/{id}',[
        'as'=>'programa.update' ,
        'uses' => 'ProgController@update'
    ]);
    Route::resources([
        'asignatura' => 'AsigtController'
    ]);
    Route::get('asignatura/{id}/delete',[
        'as'=>'asignatura.delete' ,
        'uses' => 'AsigtController@destroy'
    ]);
    Route::post('asignatura/{id}',[
        'as'=>'asignatura.update' ,
        'uses' => 'AsigtController@update'
    ]);
    Route::resources([
        'contenido' => 'ContAsigtController'
    ]);
    Route::get('contenido/{id}/delete',[
        'as'=>'contenido.delete' ,
        'uses' => 'ContAsigtController@destroy',
        'middleware' => 'rol:admin'
    ]);
    Route::get('contenido/{id}/create',[
        'as'=>'contenido.create' ,
        'uses' => 'ContAsigtController@create',
        'middleware' => 'rol:admin'
    ]);
    Route::post('contenido/{id}',[
        'as'=>'contenido.update' ,
        'uses' => 'ContAsigtController@update',
        'middleware' => 'rol:admin'
    ]);
    Route::group(['prefix' => 'asignacion', 
    'as'=>'asignacion.'], function () {
        Route::resources([
            'programaciontematica' => 'ProgTemController'
        ]);
        Route::post('consultar_planes_prog',[
        'as'=>'consultar_planes_prog',
        'uses' => 'AsigcController@consultar_planes_prog'
        ]);
        Route::post('{id}/programaciontematica/update',[
        'as'=>'programaciontematica.update',
        'uses' => 'ProgTemController@update'
        ]);
          Route::group(['prefix' => 'programaciontematica', 'as'=>'programaciontematica.'], function () {
               Route::get('{id}/refebiblio/{id2}/delete',[
                'as'=>'refebiblio.delete' ,
                'uses' => 'ProgTemRefeBiblioController@destroy'
                ]);
                Route::post('{id}/refebiblio/store',[
                'as'=>'refebiblio.store' ,
                'uses' => 'ProgTemRefeBiblioController@store'
                ]);
                
               Route::get('{id}/puntoadic/{id2}/delete',[
                'as'=>'puntoadic.delete' ,
                'uses' => 'ProgTemPuntoAdicController@destroy'
                ]);
                Route::post('{id}/puntoadic/store',[
                'as'=>'puntoadic.store' ,
                'uses' => 'ProgTemPuntoAdicController@store'
                ]);
          });
        Route::resources([
            'seguimiento' => 'SegController'
        ]); 
        
    Route::post('seguimiento/{id}/edit',[
        'as'=>'seguimiento.edit' ,
        'uses' => 'SegController@edit_post'
    ]);

    
    Route::resources([
            'informefinal' => 'InfoFinController'
        ]);
    });
    Route::resources([
        'asignacion' => 'AsigcController'
    ]);
    Route::get('asignacion/{id}/delete',[
        'as'=>'asignacion.delete' ,
        'uses' => 'AsigcController@destroy'
    ]);
    Route::post('asignacion/{id}',[
        'as'=>'asignacion.update' ,
        'uses' => 'AsigcController@update'
    ]);
   Route::get('asignacion/create/individual',[
        'as'=>'asignacion.createindividual' ,
        'uses' => 'AsigcController@create_individual',
        'middleware' => 'rol:admin|director'
    ]);
   Route::get('asignacion/create/plan',[
        'as'=>'asignacion.createplan' ,
        'uses' => 'AsigcController@create_plan',
        'middleware' => 'rol:admin|director'
    ]);
  /*
   Route::get('asignacion/create/plan/{cod_prog}',[
        'as'=>'asignacion.createplan' ,
        'uses' => 'AsigcController@create_plan',
        'middleware' => 'rol:admin|director'
    ]);
*/
    Route::resources([
        'planes' => 'PlanesController'
    ]);
    Route::get('planes/{id}/delete',[
        'as'=>'planes.delete' ,
        'uses' => 'PlanesController@destroy',
        'middleware' => 'rol:admin|director'
    ]);
    Route::post('planes/{id}',[
        'as'=>'planes.update' ,
        'uses' => 'PlanesController@update',
        'middleware' => 'rol:admin|director'
    ]);
    Route::get('planes/{id}/create',[
        'as'=>'planes.create' ,
        'uses' => 'PlanesController@create'
    ]);
  /*
  Route::put('post/{id}', ['middleware' => 'role:editor', function ($id) {
    //
}]);
  */
  
    Route::group(['prefix' => 'planes', 
    'as'=>'planes.'], function () {
        
        /*
        Route::get('{id}/asigt/create',[
        'as'=>'asigt.create' ,
        'uses' => 'PlanesAsigtController@create',
        'middleware' => 'rol:admin'
        ]);
        */
    });
  Route::get('planesasigt/{id}/delete',[
        'as'=>'planesasigt.delete' ,
        'uses' => 'PlanesAsigtController@destroy',
        'middleware' => 'rol:admin|director'
    ]);
    Route::resources([
          'planesasigt' => 'PlanesAsigtController'
        ]);
    Route::resources([
          'planesasigtpre' => 'PlanesPreReqController'
        ]);
   Route::get('/planesasigtpre/{planes_id}/delete/{id}',[
        'as'=>'planesasigtpre.delete' ,
        'uses' => 'PlanesPreReqController@destroy',
        'middleware' => 'rol:admin|director'
    ]);
    Route::resources([
        'control' => 'ControlFechasController'
    ]);
    Route::get('control/{id}/delete',[
        'as'=>'control.delete' ,
        'uses' => 'ControlFechasController@destroy'
    ]);
    Route::post('control/{id}',[
        'as'=>'control.update' ,
        'uses' => 'ControlFechasController@update'
    ]);
    
    Route::resources([
        'links' => 'LinksController'
    ]);
    Route::get('links/{id}/delete',[
        'as'=>'links.delete' ,
        'uses' => 'LinksController@destroy'
    ]);
    Route::post('links/{id}',[
        'as'=>'links.update' ,
        'uses' => 'LinksController@update'
    ]);
    
    Route::resources([
        'programacion' => 'ProgTemController'
    ]);
    Route::get('programacion/{id}/delete',[
        'as'=>'programacion.delete' ,
        'uses' => 'ProgTemController@destroy'
    ]);
    Route::post('programacion/{id}',[
        'as'=>'programacion.update' ,
        'uses' => 'ProgTemController@update'
    ]);
    Route::resources([
        'seguimieto' => 'SegController'
    ]);
    Route::get('seguimieto/{id}/delete',[
        'as'=>'seguimieto.delete' ,
        'uses' => 'SegController@destroy'
    ]);
    Route::get('seguimieto/seguimieto/{id}',[
        'as'=>'seguimieto.update' ,
        'uses' => 'SegController@update'
    ]);
    Route::post('seguimieto/guardar_item/{id}/{id_asigc}',[
        'as'=>'seguimieto.guardar_item' ,
        'uses' => 'SegController@guardar_item'
    ]);
    Route::post('seguimieto/actualizar_item/{id}',[
        'as'=>'seguimieto.actualizar_item' ,
        'uses' => 'SegController@actualizar_item'
    ]);
    Route::post('seguimieto/eliminar_item/{id}',[
        'as'=>'seguimieto.eliminar_item' ,
        'uses' => 'SegController@eliminar_item'
    ]);
    Route::post('seguimieto/guardar_voto',[
        'as'=>'seguimieto.guardar_voto' ,
        'uses' => 'SegController@guardar_voto'
    ]);
    Route::resources([
        'informefinal' => 'InfoFinController'
    ]);
    Route::get('informefinal/{id}/delete',[
        'as'=>'informefinal.delete' ,
        'uses' => 'InfoFinController@destroy'
    ]);
    Route::post('informefinal/{id}',[
        'as'=>'informefinal.update' ,
        'uses' => 'InfoFinController@update'
    ]);
    Route::resources([
        'codigos' => 'CodigosController'
    ]);
    Route::get('codigos/{id}/create',[
        'as'=>'codigos.create' ,
        'uses' => 'CodigosController@create'
    ]);
    Route::get('codigos/{id}/delete',[
        'as'=>'codigos.delete' ,
        'uses' => 'CodigosController@destroy'
    ]);
    Route::post('codigos/{id}',[
        'as'=>'codigos.update' ,
        'uses' => 'CodigosController@update'
    ]);
});

Route::group(['prefix' => 'reportes', 'as'=>'reportes.'], function () {
    Route::get('prog_tem', 'PdfController@prog_tem');
    Route::get('prog_tem/{id}',[
        'as'=>'prog_tem' ,
        'uses' => 'PdfController@prog_tem'
    ]);
    Route::get('seguimiento/{id}',[
        'as'=>'seguimiento' ,
        'uses' => 'PdfController@seguimiento'
    ]);
    Route::get('inf_final/{id}',[
        'as'=>'inf_final' ,
        'uses' => 'PdfController@inf_final'
    ]);
    Route::get('general',[
        'as'=>'general' ,
        'middleware' => 'norol:estudiante',
        'uses' => 'HomeController@index'
//         'uses' => 'PdfController@general'
    ]);
    Route::post('general',[
        'as'=>'general' ,
        'middleware' => 'norol:estudiante',
        'uses' => 'HomeController@index'
//         'uses' => 'PdfController@general'
    ]);
    Route::get('seg_prog', function () {
    return view('reportes.seg_prog');
    });
    Route::get('inf_final', function () {
    return view('reportes.inf_final');
    });
    Route::get('asignacion/flexibilidad',[
        'as'=>'asignacion.flexibilidad' ,
        'uses' => 'PdfController@flexibilidad'
    ]);
    Route::get('programa/planes',[
        'as'=>'programa.planes' ,
        'uses' => 'PdfController@planes'
    ]);
});
 Route::get('usuarios/perfil',[
        'as'=>'usuarios.perfil' ,
        'middleware' => 'auth',
        'uses' => 'UserController@mi_perfil'
    ]);
 Route::get('usuarios/perfil/{id}',[
        'as'=>'usuarios.ver_perfil' ,
        'middleware' => 'auth',
        'uses' => 'UserController@show'
    ]);
 Route::get('num',[
        'as'=>'num' ,
        'middleware' => 'auth',
        'uses' => function(){
          return Funciones::num();
        }
    ]);
  Route::post('usuarios/cambiar_clave',[
      'as'=>'usuarios.cambiar_clave' ,
      'middleware' => 'auth',
      'uses' => 'UserController@cambiar_clave'
  ]);
#Route::get('pdf', 'PdfController@invoice');
Route::resources(['notificaciones'=>'NotificacionesController']);
Route::resources(['uploads'=>'UploadsController']);
Route::get('events', 'EventController@index');