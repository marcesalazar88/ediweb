<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SegVal extends Model
{
      public $timestamps = false;
    protected $table = 'seg_val';
    protected $fillable = [
        'seg_id',
        'aspecto',
        'tipo',
        'valoracion',
        'voto',
        'replica'
        ];
    public $fillcolumn = [
        'seg_id'=>'seg_id',
        'aspecto'=>'aspecto',
        'tipo'=>'tipo',
        'valoracion'=>'valoracion',
        'voto'=>'voto',
        'replica'=>'replica'
        ];
    //
    public function seg()
    {//Verificado
        return $this->belongsTo('App\Seg');
    }
    public function segaspval()
    {//Verificado
        return $this->belongsTo('App\SegAspVal','aspecto','cod');
    }
}
