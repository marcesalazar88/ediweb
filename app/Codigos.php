<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Codigos extends Model
{
    public $timestamps = false;
    protected $table = 'codigos';
    protected $fillable = [
        'token',
        'referencia_asignacion',
        'user_id'
    ];
    public $fillcolumn = [
        'token'=>'Código',
        'asigc_id'=>'Asignación',
        'user_id'=>'ID Usuario'
    ];
public function user()
    {//Verificado
        return $this->belongsTo('App\User');
    }
public function asigcs()
    {//Verificado
        return $this->belongsTo('App\Asigc');
    }
}
