<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    public $timestamps = false;
    protected $table = 'config';
    protected $fillable = [
         'cambiar_rol', 'autorregistro', 'limpiar_cache', 'notificar_email', 'hora_notificacion'
    ];
    public $fillcolumn = [
          'cambiar_rol' => 'cambiar_rol' ,
          'autorregistro' => 'autorregistro' ,
          'limpiar_cache' => 'limpiar_cache' ,
          'notificar_email' => 'notificar_email' ,
          'hora_notificacion' => 'hora_notificacion'
    ];
}
