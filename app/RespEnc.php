<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RespEnc extends Model
{
    public $timestamps = false;
    protected $table = 'resp_enc';
    protected $fillable = [
        'preg_enc_id',
        'respuesta',
        'usu_id' 
    ];

    public $fillcolumn = [
        'preg_enc_id' => 'preg_enc_id',
        'respuesta' => 'respuesta',
        'usu_id' => 'usu_id' 
    ];
  
    public function user_fk()
      {//Verificado
          return $this->belongsTo('App\User','usu_id','ident_usu');
      }
}
