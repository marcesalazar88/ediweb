<?php

if (!function_exists('str_convert_line_breaks')) {
    function str_convert_line_breaks($string = '', $as_html = true) {
        if (empty($string)) {
            return $string;
        }

        $replace_string = ($as_html) ? "<br>" : PHP_EOL;
        $formatted_string = preg_replace("/\r\n|\r|\n/", $replace_string, $string);
        return ($as_html) ? clean($formatted_string) : $formatted_string;
    }
}
if (!function_exists('puntos_suspensivos2')) {
public static function puntos_suspensivos2($string, $length=NULL)
    {
        //Si no se especifica la longitud por defecto es 50
        if ($length == NULL)
            $length = 50;
        //Primero eliminamos las etiquetas html y luego cortamos el string
        $stringDisplay = substr(strip_tags($string), 0, $length);
        //Si el texto es mayor que la longitud se agrega puntos suspensivos
        if (strlen(strip_tags($string)) > $length)
            $stringDisplay .= ' ...';
        return $stringDisplay;
    }
}   