<?php

namespace App;

class Crud
{
    public function buscar($request,$model,$campo_personalizado = '',$ba=''){
        if ($campo_personalizado == ''){ 
            $campo_personalizado = []; 
        }else{
            $model->fillcolumn = $campo_personalizado;
        }
        $resultados = 10;
        if (isset($_GET['resultados'])){
            $resultados = $_GET['resultados'];
        }
      /*
        $orderby = 'id';
        if (isset($_GET['orderby'])){
            $resultados = $_GET['orderby'];
        }
      */
        $comparador = 'and';//or|and
        if (isset($_GET['comparador'])){
            $comparador = $_GET['comparador'];
        }
        $valor_clave = '';
        if (isset($_GET['ba'],$_GET['valor_clave']) and $_GET['ba']=='1'){
            $valor_clave = base64_decode($_GET['valor_clave']);
            $valor_clave = json_decode($valor_clave,true);
        }else if (isset($_GET['valor_clave'])){
            $valor_clave = mb_strtolower(trim($_GET['valor_clave']),'UTF-8');
        }
        if ($ba!=''){
            $valor_clave = $ba;
        }
        $salida = $model
            ->select('*')
            ->where(function($q) use ($model, $valor_clave) {
               
                if (is_array($valor_clave)){
                    foreach ($valor_clave as $id => $valor){
                            if ($valor!= ""){
                            $q->where(function($query) use ($id, $valor){
                                    $query->whereRaw('LOWER(`'.$id.'`) LIKE ? ',[''.$valor.'']);
                                });
                            }
                           
                        }//foreach
                }else if ($valor_clave!=''){
                  $cont= 1;       
                    //$valor_clave_arr = explode(" ",$valor_clave);
                    //foreach ($valor_clave_arr as $idc => $valor_clave_i){
                        foreach ($model->fillcolumn as $id => $valor){
                            if ($cont== 1){
                            $q->where(function($query) use ($id, $valor_clave){
                                    $query->whereRaw('LOWER(`'.$id.'`) LIKE ? ',['%'.$valor_clave.'%']);
                                });
                            }else{
                                $q->orWhere(function($query) use ($id, $valor_clave) {
                                        $query->whereRaw('LOWER(`'.$id.'`) LIKE ? ',['%'.$valor_clave.'%']);
                                    });
                            }
                            $cont++;
                        }//foreach
                   // }//foreach
                }//if
            })
            ->when((!isset($_GET['paginate']) or (isset($_GET['paginate']) and $_GET['paginate']==true)), function ($query) use ($resultados){
              //dd($resultados);
                    return $query->paginate($resultados);
              }, function ($query) {
                    return $query->get();
              });
          
            
            //->get();
          
           // if (isset($_GET['paginate']) and $_GET['paginate']==false){
            /*
            
            ->when($orderby, function ($query, $orderby) {
                    return $query->orderBy($orderby);
              }, function ($query) {
                    return $query->orderBy('id');
              })
              */
          
            //dd($salida->toSql());
            $salida->app_url = env('APP_URL');
        return $salida;
    }
}
?>