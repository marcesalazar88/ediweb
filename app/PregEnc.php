<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PregEnc extends Model
{
    public $timestamps = false;
    protected $table = 'preg_enc';
    protected $fillable = [
        'pregunta',
        'lugar'
    ];

    public $fillcolumn = [
        'pregunta' => 'pregunta',
        'lugar' => 'lugar'
    ];

}
