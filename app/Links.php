<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Links extends Model
{
    public $timestamps = false;
    protected $table = 'links';
    protected $fillable = [
        'nombre',
        'url',
        'target',
        'icono'
    ];

    public $fillcolumn = [
        'nombre'=>'nombre',
        'url'=>'url',
        'target'=>'target',
        'icono'=>'icono'
    ];

}
