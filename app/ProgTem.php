<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProgTem extends Model
{
    public $timestamps = false;
    protected $table = 'prog_temt';
    protected $fillable = [
            'asigc_id',
            'fecha_act_progc_temt',
            'metodologia',
            'crit_eva',
            'horas_practicas',
            'horas_teoricas',
            'horas_adicionales',
            'rev_direct',
            'firm_docnt',
            'firm_est',
            'fecha_aprobacion',
            'fecha_revision_doc',
            'codigo',
            'estado',
            'control_fecha_id'
        ];
    public $fillcolumn = [
        'asigc_id'=>'asigc_id',
        'fecha_act_progc_temt'=>'fecha_act_progc_temt',
        'metodologia'=>'metodologia',
        'crit_eva'=>'crit_eva',
        'horas_practicas'=>'horas_practicas',
        'horas_teoricas'=>'horas_teoricas',
        'horas_adicionales'=>'horas_adicionales',
        'rev_direct'=>'rev_direct',
        'firm_docnt'=>'firm_docnt',
        'firm_est'=>'firm_est',
        'fecha_aprobacion' => 'Fecha Aprobacion',
        'fecha_revision_doc' => 'Fecha Revision Docente',
        'codigo' => 'Codigo',
        'estado' => 'Estado',
        'control_fecha_id' => 'Control de Fechas'
        ];
        
        public function asigc()
    {//Verificado
        return $this->belongsTo('App\Asigc','asigc_id','id');
    }
    public function segs()
    {//Verificado
        return $this->hasMany('App\Seg','prog_temt_id','id');
    }
     public function refe_biblios()
    {//Verificado
        return $this->hasMany('App\ProgTemRefeBiblio','prog_tem_id','id');
    }
     public function punto_adics()
    {//Verificado
        return $this->hasMany('App\ProgTemPuntoAdic');
    }
}
