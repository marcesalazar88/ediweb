<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Asigc extends Model
{
    public $timestamps = false;
    protected $table = 'asigc';
    protected $fillable = [
         'ident_docnt',
         'cod_asigt',
         'per_acad',
         'cod_prog',
         'flex',
         'grupo',
         'observaciones'
    ];
    public $fillcolumn = [
        'ident_docnt'=>'Identificación',
        'cod_asigt'=>'Código de Asignatura',
        'per_acad'=>'Semestre',
        'cod_prog'=>'Programa',
        'flex'=>'Flexibilidad',
        'grupo'=>'Grupo',
        'observaciones'=>'Observaciones'
    ];
    
    public function prog()
    {//Verificado
        return $this->belongsTo('App\Prog','cod_prog','cod_prog');
    }
    

    
    public function docente_fk()
    {//Verificado
        return $this->belongsTo('App\User','ident_docnt','ident_usu');
    }
    
    public function estudiante_fk()
    {//Verificado
        return $this->belongsTo('App\User','estudiante','ident_usu');
    }
    
    public function asigt()
    {//Verificado
        return $this->belongsTo('App\Asigt','cod_asigt', 'codigo_asigt');
    }
    
    public function periodo()
    {//Verificado
        return $this->belongsTo('App\Periodo','per_acad', 'periodo');
    }
    
     public function progtems()
    {//Verificado
        return $this->hasMany('App\ProgTem','asigc_id','id');
    }
     public function infofins()
    {//Verificado
        return $this->hasMany('App\InfoFin','id_asigt','id');
    }
     public function codigos()
    {//Verificado
        return $this->hasMany('App\Codigos');
    }

}
