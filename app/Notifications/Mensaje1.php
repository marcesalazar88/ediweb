<?php

namespace App\Notifications;



use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;
use App\Depto;
use App\Funciones;
use App\Periodo;

class Mensaje1 extends Notification
{
    //use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
     
     public $token;


    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
     //App\Notifications\Mensaje1::toMail()
    public function toMail($notifiable)
    {
      $texto="";
      $periodo_activo = Funciones::periodo_activo();
      $periodo= Periodo::where("periodo",$periodo_activo)->get()->first();
      $depto_id = 1;
      $depto = Depto::find($depto_id);
      $director_id= $departamento->director;
      $director = User::find($director_id);
$texto.="Para: Profesores Dpto. ".$depto->nom_depto." Universidad de Nariño<br>";
$texto.="De ".$director->name.", Director Dpto. ".$depto->nom_depto."<br>";
$texto.="Asunto: Observaciones semestre ".$periodo_activo."br>";
$texto.="<br>";
$texto.="Reciban Uds. un abrazo de bienvenida en la iniciación del período académico
".$periodo_activo.".<br>";
$texto.="Por  $notifiable un desarrollo adecuado de la labor docente, teniendo como objetivos los procesos de
cada programa sus las acciones respecto de la Acreditación Institucional; sin embargo, algunos procedimientos
requieren orientación para el desarrollo de los procesos de manera, correcta y oportuna aspectos
importantes para el Departamento y para la Universidad. Esta comunicación sin duda alguna es de
mucha utilidad para los docentes que por primera vez ejercen su profesión en esta Unidad Académica.
Como primera medida informamos al presente los formatos que dirigen los procesos o los
evidencian, con sus respectivos plazos de entrega y las indicaciones pertinentes:";
$texto.="<br>";
$texto.= "1. ".$periodo->Acuerdo_fechas." emanado por el Honorable Consejo Superior, la cual es de obligatorio
cumplimiento para los docentes de la Institución. En ella se establece fechas tan importantes
como la iniciación del semestre ".$periodo->fecha_inicio." y eso significa que todos debemos
iniciar clases esa fecha (Archivo adjunto).
2. A continuación me refiero a algunos aspectos relevantes que como docentes DEBEMOS tener
en cuenta para contribuir a un funcionamiento integral y óptimo del Departamento. Esta
comunicación podría denominarse la “carta de navegación” para el presente semestre.
3. INDICACIONES GENERALES
• Sin excepciones, el semestre inicia ".$periodo->fecha_inicio.". Por favor, comencemos de
manera puntual con nuestro compromiso institucional.
• De presentarse en general, cualquier situación anómala entre docente y estudiantes, favor
manifestarla en la Dirección del Departamento, a fin de tomar los correctivos de manera
oportuna.
• Finalmente son INMODIFICABLES LOS HORARIOS y las fechas estipuladas en los procesos de
formación Académica.
• Programa y el cronograma establecido más adelante para las actividades durante el semestre.

4. PROGRAMAS DE ASIGNATURAS

• El contenido a desarrollarse en cada asignatura será el suministrado oficialmente por el
Departamento. Sin embargo, si existiese algo susceptible de mejorar en el contenido, éste
cambio deberá ser concertado con la Dirección del Departamento y del Programa en donde se
ofrezca.
• Es indispensable que Ud. revise muy bien el documento y diligencie que envía como programa
de la asignatura.
• Según el Estatuto Estudiantil vigente, el docente en el primer día de clase debe socializar el
programa de la asignatura y concertar con el grupo la forma en que se realizará la evaluación.
5. SEGUIMIENTO AL DESARROLLO DE LAS ASIGNATURAS
• La Facultad de Ciencias Exactas y Naturales mediante Acuerdo 030 de 2016 eligió la Opción 2
para el cumplimiento de este requisito.
• El docente debe diligenciar el formato, el representante estudiantil del curso, revisa y aprueba
los aspectos de la asignatura, hasta el {{fecha_seguimiento}}

6. INFORMES FINALES SOBRE EL DESARROLLO DE CADA CURSO (Archivo adjunto).
• Por cada asignatura ofrecida, el docente deberá diligenciar un informe detallado de la forma en que
transcurrió el curso: observaciones, sugerencias, porcentajes de cumplimiento de la asignatura,
aprobados y reprobados, ete. incluso, si por situaciones ajenas al docente no se pudo desarrollar
el total de la temática, es necesario conocer esta situación para implementar algún correctivo
posterior.

DE NUESTRO COMPROMISO Y CUMPLIMIENTO DEPENDE QUE NUESTRA UNIVERSIDAD Y NUESTROS

PROGRAMAS SEAN RECONOCIDOS.";
         return (new MailMessage)
            ->subject('Recuperar contraseña')
            ->greeting('Hola')
            ->line('Usted está recibiendo este correo electrónico porque recibimos una solicitud de recuperación de contraseña para su cuenta.')
            ->action('Recuperar contraseña', url(config('app.url').route('password.reset', $this->token, false)))
            ->line('Si no solicitó la recuperación de contraseña, haga caso omiso a este mensaje.')
            ->salutation('Saludos, '. config('app.name'));
    }
    /**
     * url('/')
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
