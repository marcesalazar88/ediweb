<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prog extends Model
{
    public $timestamps = false;
    protected $table = 'prog';
    protected $fillable = [
        'nom_prog',
        'cod_prog',
        'depto_id',
        'sede',
        'periodo_ingreso'
    ];
    public $fillcolumn = [
        'nom_prog'=>'Nombre',
        'cod_prog'=>'Código de programa',
        'depto_id'=>'Departamento',
        'sede'=>'Sede',
        'periodo_ingreso'=>'Periodo de ingreso'
    ];
 public function depto()
    {//Verificado
        return $this->belongsTo('App\Depto');
    }
 public function asigcs()
    {//Verificado
        return $this->hasMany('App\Asigc','cod_prog','cod_prog');
    }
    public function planes()
    {//Verificado
        return $this->hasMany('App\Planes','cod_prog','cod_prog');
    }
      
}
