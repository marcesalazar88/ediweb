<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanesAsigt extends Model
{
    //`id`, `planes_id`, `semestre`, `asigt_id` FROM `planes_asigt` 
    public $timestamps = false;
    protected $table = 'planes_asigt';
    protected $fillable = [
      'planes_id',
      'semestre',
      'asigt_id'
    ];

    public $fillcolumn = [
      'planes_id' => 'ID Planes',
      'semestre' => 'Semestre',
      'asigt_id' => 'ID Asigt'
    ];
 public function plan()
    {//Verificado
        return $this->belongsTo('App\Planes');
    }
  public function asigt()
    {//Verificado
        return $this->belongsTo('App\Asigt','asigt_id', 'codigo_asigt');
    }
  public function prerequisitos()
    {//Verificado
        return $this->hasMany('App\PlanesPreReq');
    }
 
}
