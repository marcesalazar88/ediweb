<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'usu';
    protected $fillable = [
        'ident_usu', 'rol', 'roles', 'tel', 'name', 'nombre', 'apellido', 'email', 'password', 'remember_token', 'created_at', 'updated_at' 
    ];
    public $fillcolumn = [
        'ident_usu'=>'Identificacion',
        'rol'=>'Rol',
        'roles'=>'Roles',
        'tel'=>'Telefono',
        'name'=>'Nombre',
        'nombre'=>'Nombres',
        'apellido'=>'Apellido',
        'email'=>'Correo Electronico',
        'password'=>'Contrasena'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at'
    ];//
        public function codigos()
    {//Verificado
        return $this->hasMany('App\Codigos');
    }
  public function deptos()
    {//Verificado
        return $this->hasMany('App\Depto','director','ident_usu');
    }
  
  public function notificaciones_fk()
    {//Verificado
        return $this->hasMany('App\Notificaciones','destino','ident_usu');
    }
    public function asigcs()
    {//Verificado
        return $this->hasMany('App\Asigc');
    }
    public function sendPasswordResetNotification($token)
    {
       $this->notify(new ResetPassword($token));
    }
}
