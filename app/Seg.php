<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seg extends Model
{
      public $timestamps = false;
    protected $table = 'seg';
    protected $fillable = [
        'prog_temt_id', 
        'observ_est', 
        'observ_docnt',
        'estudiante', 
        'docente', 
        'codigo', 
        'estado', 
        'estado_est'
        ];
    public $fillcolumn = [
        'prog_temt_id' => 'prog_temt_id',
        'observ_est' => 'observ_est',
        'estudiante'=>'estudiante',
        'docente'=>'docente',
        'observ_docnt' => 'observ_docnt',
        'codigo'  => 'codigo', 
        'estado'  => 'estado', 
        'estado_est'  => 'estado_est'
        ];
    public function prog_tem()
    {//Verificado
        return $this->belongsTo('App\ProgTem','prog_temt_id','id');
    }
    public function controlfecha()
    {//Verificado
        return $this->belongsTo('App\ControlFechas');
    }
        public function docente_fk()
    {//Verificado
        return $this->belongsTo('App\User','docente','ident_usu');
    }
    
    public function estudiante_fk()
    {//Verificado
        return $this->belongsTo('App\User','estudiante','ident_usu');
    }
     public function seg_vals()
    {//Verificado
        return $this->hasMany('App\SegVal','seg_id','id');
    }
    public function infofins()
    {//Verificado
        return $this->hasMany('App\InfoFin');
    }
}
