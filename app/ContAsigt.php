<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContAsigt extends Model
{
       public $timestamps = false;
    protected $table = 'cont_asigt';
    protected $fillable = [        'cod_asigt',
        'hras_cont_asigt',
        'tem_cap_cont_asigt'
    ];
    public $fillcolumn = [
        'cod_asigt'=>'Código Asignatura',
        'hras_cont_asigt'=>'Horas de contenido',
        'tem_cap_cont_asigt'=>'Tema o Capítulo'
    ];
    public function asigt()
    {//Verificado
        return $this->belongsTo('App\Asigt','cod_asigt', 'codigo_asigt');
    }
}
