<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class PeriodoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
       switch (Request::get('submit')) {
            case 'Actualizar':
                $rules = [
                    //'periodo' => 'required|string|max:255|unique:periodo,periodo,'.\Request::get('periodo'),
                    'estado' => 'required',
                ];
                break;
        
            default:
                $rules = [
                    'periodo' => 'required|string|max:5|unique:periodo',
                    'estado' => 'required',
                ];
                break;
        }

        return $rules;
    }

    public function messages()
    {
        switch (Request::get('submit')) {
            case 'Actualizar':
                $rules = [
                'periodo.required'  => 'El periodo es obligatorio',
                'periodo.max' => 'El periodo no puede tener más de 5 caracteres, Utilice un formato similar a 2019B',
                'periodo.unique' => 'El Registro de este departamento ya existe',
                'estado.required'  => 'El campo facultad es obligatorio'
        ];
                break;
        
            default:
                $rules = [
                'periodo.required'  => 'El periodo es obligatorio',
                'periodo.max' => 'El periodo no puede tener más de 5 caracteres, Utilice un formato similar a 2019B',
                'periodo.unique' => 'El Registro de este departamento ya existe',
                'estado.required'  => 'El campo facultad es obligatorio'
        ];
                break;
        }
    return $rules;
    }
}
