<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class PuntoAdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item' => 'required|string|max:255',
            'valor' => 'required|integer|max:255'
        ];
    }
/*
  "password" => "123456"
  "password_confirmation" => "123456"
*/
    public function messages()
    {
        return [
            
            'item.required'  => 'El contenido es obligatorio',
            'item.string'  => 'El contenido debe ser un texto',
            'item.max' => 'El contenido no puede tener más de 255 caracteres',
            
            'valor.required'  => 'El contenido es obligatorio',
            'valor.integer'  => 'El contenido debe ser un número',
            'valor.max' => 'El contenido no puede tener más de 255 caracteres'
        ];
    }
}
