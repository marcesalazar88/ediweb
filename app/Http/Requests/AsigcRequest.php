<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;
use App\User;

class AsigcRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        #return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (Request::get('submit')) {
            case 'Actualizar':
                $rules = [
            'ident_docnt' => 'required|exists:usu,ident_usu',
            'cod_asigt' => 'required|exists:asigt,codigo_asigt',
            'per_acad' => 'required|exists:periodo,periodo',
            'cod_prog' => 'required|exists:prog,cod_prog',
            'flex' => [
                'required',
                Rule::in(['SI', 'NO']),
            'grupo' => 'required',
            'observaciones' => 'max:2000'
            ]
        ];
                break;
        
            default:
                $rules = [
            'ident_docnt' => 'required|exists:usu,ident_usu',
            'cod_asigt' => 'required|exists:asigt,codigo_asigt',
            'per_acad' => 'required|exists:periodo,periodo',
            'cod_prog' => 'required|exists:prog,cod_prog',
            'flex' => 'required',
                Rule::in(['SI', 'NO']),
            'grupo' => 'required',
            'observaciones' => 'max:2000'
            ];
                break;
        }

        return $rules;
        
    }

    public function messages()
    {
        return [
            'ident_docnt.required'  => 'El Docente es obligatorio',
            'cod_asigt.required' => 'La asignatura es obligatoria',
            'per_acad.required' => 'El periodo académico es obligatorio',
            'cod_prog.required' => 'El programa es obligatorio',
            'flex.required' => 'El campo flexibilidad es obligatorio',
            'flex.in' => 'El valor del campo flexibilidad no es válido, debe ser SI o NO',
            'ident_docnt.in' => 'El docente no es válido, debe ser un docente registrado en el sistema'
        ];
    }
}
