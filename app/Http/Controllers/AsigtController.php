<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Crud;
use App\Asigt;
use App\ContAsigt;
use App\User;
use App\Prog;
use App\Depto;
use App\Fac;
use App\Http\Requests\AsigtRequest;
use App\Http\Requests\DptoRequestUpdate;
use Redirect;
class AsigtController extends Controller
{

public function __construct()
{
    $this->middleware('auth', ['only' => ['index','create','edit','destroy','store','update']]);//auth|guest
    $this->middleware('rol:admin', ['only' => ['index','create','edit','destroy','store','update']]);//admin|docente|estudiante
  //function show -> permitido para todos
}

public function index(Request $request){
     $this->middleware('auth');//auth|guest
     $crud = new Crud();
     $this->asigt = new Asigt();
     $resultado = $crud->buscar($request,$this->asigt);
     $json = $request->get('json');
     if ($json){
     return $resultado;
     }else{
     return view('admin.asignatura.index')->with(["resultado"=>$resultado]);
     }
}
public function create(){
     $this->middleware('auth');//auth|guest
     return view('admin.asignatura.create');
}
public function show($id){
    $asigt = Asigt::find($id);
    if (isset($asigt->codigo_asigt)){
        $asigt->contasigts();
        $asigt->asigcs();
        foreach ($asigt->asigcs as $id => $asigc){
            $asigt->asigcs[$id]->docente_fk();
            $asigt->asigcs[$id]->estudiante_fk();
            $asigt->asigcs[$id]->prog();
            $asigt->asigcs[$id]->prog->depto();
            $asigt->asigcs[$id]->prog->depto->fac();
        }
        return view('admin.asignatura.show')->with(['asigt'=>$asigt]);
    }else{
        return Redirect::to('admin/asignatura')->with('danger', 'No existen registros de esta asignatura.');
    }
}
public function destroy($id){
     $this->middleware('auth');//auth|guest
    $user = Asigt::find($id);
    $user->delete();
    if ($user->exists === false)
    return redirect()->route('admin.asignatura.index')->with('notice', 'La Asignatura ha sido eliminada correctamente.');
    else
    return redirect()->route('admin.asignatura.index')->with('notice', 'Error, La Asignatura no ha sido eliminada.');
}
public function edit($id){
     $this->middleware('auth');//auth|guest
    $asigt = Asigt::find($id);
    $tipo_identificacion = array();
    return view('admin.asignatura.editar')->with(["asigt"=>$asigt]);
}
public function store(AsigtRequest $request){
    #dd($request);
    $this->middleware('auth');//auth|guest
    $asigt = new Asigt;
    $asigt->codigo_asigt = $request->codigo_asigt;
    $asigt->nom_asigt = $request->nom_asigt;
    $asigt->sem_ofrece_asigt = implode(", ",$request->sem_ofrece_asigt);
    $asigt->ihs_asigt = $request->ihs_asigt;
    $asigt->n_cred_asigt = $request->n_cred_asigt;
    $asigt->just_asigt = $request->just_asigt;
    $asigt->obj_gen = $request->obj_gen;
    $asigt->obj_esp = $request->obj_esp;
    $asigt->area = $request->area;
    $result = $asigt->save();
    if($result){
     if ($request->detalles=="SI")
            return redirect()->route('admin.asignatura.show', [$asigt->id])->with('success', 'La Asignatura ha sido registrada correctamente.');
        else
            return redirect()->route('admin.asignatura.index')->with('success', 'La Asignatura ha sido registrada correctamente.');
    }else{
    return  redirect()->route('admin.asignatura.index')->with('danger', 'Error, La Asignatura no ha sido registrada.');
    }
}
public function update(Request $request,$id){
    #dd($request);
    $this->middleware('auth');//auth|guest
    $asigt = Asigt::find($id);
    $asigt->codigo_asigt = $request->codigo_asigt;
    $asigt->nom_asigt = $request->nom_asigt;
    $asigt->sem_ofrece_asigt = implode(",  ",$request->sem_ofrece_asigt);
    $asigt->ihs_asigt = $request->ihs_asigt;
    $asigt->n_cred_asigt = $request->n_cred_asigt;
    $asigt->just_asigt = $request->just_asigt;
    $asigt->obj_gen = $request->obj_gen;
    $asigt->obj_esp = $request->obj_esp;
    $asigt->area = $request->area;
    $result = $asigt->save();
    if($result){
        if ($request->detalles=="SI")
            return redirect()->route('admin.asignatura.show', [$request->asigt_id])->with('success', 'La Asignatura ha sido modificada correctamente.');
        else
            return redirect()->route('admin.asignatura.index')->with('success', 'La Asignatura ha sido modificada correctamente.');
   }else{
    return Redirect::to('admin/asignatura')->with('danger', 'Error, La Asignatura no ha sido modificada.');
   }
}

}