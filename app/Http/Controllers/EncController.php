<?php

namespace App\Http\Controllers;


use App\PregEnc;
use App\RespEnc;
use Illuminate\Http\Request;
use Redirect;
use Auth;
class EncController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');//auth|guest
       
    }
  
    public function index(Request $request)
    {
        
    }

  
    public function responder(Request $request)
    {
      $resp_enc = RespEnc::firstOrNew(array('preg_enc_id' => $request->preg_enc_id, 'usu_id' => Auth::user()->ident_usu));
      $resp_enc->respuesta = $request->respuesta;
        $result = $resp_enc->save();
        if ($result)
        return "1";
        else
        return "0";
    }

  
}