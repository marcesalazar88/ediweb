<?php

namespace App\Http\Controllers;

use App\Planes;
use App\PlanesPreReq;
use App\Crud;
use App\Prog;
use Illuminate\Http\Request;
use App\Http\Requests\PlanesRequest;
use App\Http\Requests\PlanesRequestUpdate;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Illuminate\Support\Facades\Storage;
class PlanesPreReqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public $planes;
     public function __construct()
    {
        //$this->middleware('auth');//auth|guest
    }
    public function index(Request $request)
    {
         $crud = new Crud();
         $prog = new Prog();
         $this->planes = new Planes();
         $resultado = $crud->buscar($request,$this->planes);
          foreach ($resultado as $id => $resultado_i){
            $resultado_i->prog();
            $resultado[$id]->nom_prog = $resultado[$id]->prog->nom_prog;
          }
          #dd($resultado->app_url);
          #dd($resultado);
         if (isset($_REQUEST['json'])){
         return $resultado;
         }else{
         return view('admin.planes.index')->with(["resultado"=>$resultado, "prog"=>$prog->all()]);
         }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($cod_prog='')
    {
        if ($cod_prog!=''){
        $prog = Prog::where('cod_prog',$cod_prog)->get();
        return view('admin.planes.asigt.create')->with(["prog"=>$prog->first()]);
        }else{
        return redirect()->back()->with('warning', 'Error, Usted está intentando ingradar de manera incorrecta, verifique su información.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //dd($request->all());
      $planesprereq = new PlanesPreReq();
      $planesprereq->planes_asigt_id = $request->planes_asigt_id;
      $planesprereq->pre = $request->cod_asigt;
        $result = $planesprereq->save();
        if($result){
        return redirect()->route('admin.planes.show', ['id' => $request->planes_id])->with('success', 'El prerrequisito de la asignatura ha sido registrado correctamente.');
        }else{
        return redirect()->route('admin.planes.show', ['id' => $request->planes_id])->with('danger', 'Error, El prerrequisito de la asignatura no ha sido registrado.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fac  $planes
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fac  $planes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fac  $planes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fac  $planes
     * @return \Illuminate\Http\Response
     */
    public function destroy($planes_id,$id)
    {
    //dd($id);
    $planesasigt = PlanesPreReq::find($id);
    if($planesasigt->exists):
    $planesasigt->delete();
        if ($planesasigt->exists === false){
        return redirect()->route('admin.planes.show', ['id' => $planes_id])->with('success', 'El prerrequisito ha sido eliminado correctamente.');
        }else{
        return redirect()->route('admin.planes.show', ['id' => $planes_id])->with('danger', 'Error, El prerrequisito no ha sido eliminado.');
        }
    else:
    return redirect()->back()->with('danger', 'Error, El prerrequisito no ha sido eliminado.');
    endif;
    }
}
