<?php

namespace App\Http\Controllers;

use App\Planes;
use App\PlanesPreReq;
use App\Crud;
use App\Prog;
use Illuminate\Http\Request;
use App\Http\Requests\PlanesRequest;
use App\Http\Requests\PlanesRequestUpdate;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Illuminate\Support\Facades\Storage;
class PlanesAsigtReqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public $planes;
          public function __construct()
{
    $this->middleware('auth', ['only' => ['create','edit','destroy','store','update']]);//auth|guest
    $this->middleware('rol:admin', ['only' => ['create','edit','destroy','store','update']]);//admin|docente|estudiante
  //function index, show -> permitido para todos
}
    public function index(Request $request)
    {
         $crud = new Crud();
         $prog = new Prog();
         $this->planes = new Planes();
         $resultado = $crud->buscar($request,$this->planes);
          foreach ($resultado as $id => $resultado_i){
            $resultado_i->prog();
            $resultado[$id]->nom_prog = $resultado[$id]->prog->nom_prog;
          }
          #dd($resultado->app_url);
          #dd($resultado);
         if (isset($_REQUEST['json'])){
         return $resultado;
         }else{
         return view('admin.planes.index')->with(["resultado"=>$resultado, "prog"=>$prog->all()]);
         }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($cod_prog='')
    {
        if ($cod_prog!=''){
        $prog = Prog::where('cod_prog',$cod_prog)->get();
        return view('admin.planes.asigt.create')->with(["prog"=>$prog->first()]);
        }else{
        return redirect()->back()->with('warning', 'Error, Usted está intentando ingradar de manera incorrecta, verifique su información.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //dd($request->all());
      $planesprereq = new PlanesPreReq();
      $planesprereq->planes_asigt_id = $request->planes_asigt_id;
      $planesprereq->pre = $request->cod_asigt;
        $result = $planesprereq->save();
        if($result){
        return redirect()->route('admin.planes.show', ['id' => $request->planes_id])->with('success', 'El prerrequisito de la asignatura ha sido registrado correctamente.');
        }else{
        return redirect()->route('admin.planes.show', ['id' => $request->planes_id])->with('danger', 'Error, El prerrequisito de la asignatura no ha sido registrado.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fac  $planes
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      #dd($id);
       $prog = Prog::find($id);
        if (isset($prog->id)){
            $prog->planes();
            $prog->depto();
            $prog->depto->fac();
            $fac = $prog->depto->fac;
            $prog->asigcs();
            $planes = $prog->planes;
            foreach ($prog->asigcs as $id => $asigc){
                $prog->asigcs[$id]->docente_fk();
                $docente_fk = $prog->asigcs[$id]->docente_fk;
                $prog->asigcs[$id]->estudiante_fk();
                $estudiante_fk = $prog->asigcs[$id]->estudiante_fk;
                $prog->asigcs[$id]->asigt();
                $asigt = $prog->asigcs[$id]->asigt;
            }
            return view('admin.planes.show')->with(['prog'=>$prog]);
        }else{
            return Redirect::to('admin/programa/planes')->with('danger', 'No existen registros de este programa.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fac  $planes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$planes = Planes::find($id);
        //return view("admin.planes.editar")->with(["planes"=>$planes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fac  $planes
     * @return \Illuminate\Http\Response
     */
    public function update(PlanesRequest $request, $id)
    {
        /*
        $planes = Planes::find($id);
        $planes->nombre = $request->nombre;
        $planes->adjunto = $request->adjunto;
        $planes->fecha = $request->fecha;
        $result = $planes->save();
        if($result)
        return Redirect::to('admin/planes')->with('success', 'El plan ha sido mdificada correctamente.');
        else
        return Redirect::to('admin/planes')->with('danger', 'Error, El plan no ha sido mdificada.');
        */
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fac  $planes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    //dd($id);
    $planesasigt = PlanesAsigt::find($id);
    if($planesasigt->exists):
    $planesasigt->delete();
        if ($planesasigt->exists === false){
        return redirect()->route('admin.planes.show', ['id' => $planesasigt->planes_id])->with('success', 'El plan ha sido eliminado correctamente.');
        }else{
        return redirect()->route('admin.planes.show', ['id' => $planesasigt->planes_id])->with('danger', 'Error, El plan no ha sido eliminado.');
        }
    else:
    return redirect()->back()->with('danger', 'Error, El plan no ha sido eliminado.');
    endif;
    }
}
