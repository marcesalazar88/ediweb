<?php

namespace App\Http\Controllers;

use App\Notificaciones;
use App\Crud;
use Auth;
use App\User;
use App\Funciones;
use App\Fecha;
use Illuminate\Http\Request;
#use App\Http\Requests\Request;
use Illuminate\Support\Notificacionesades\Validator;
use Redirect;
class NotificacionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public $notificaciones;
     public function __construct()
    {
        $this->middleware('auth');//auth|guest
       // $this->middleware('rol:admin');//admin|docente|estudiante
    }
  

    public function index(Request $request)
    {
      $resultados = 5;
      $_GET['paginate']=false;
      if (isset($_GET['resultados'])){
          $resultados = $_GET['resultados'];
      }
         $crud = new Crud();
         $this->Notificaciones = new Notificaciones();
         $resultado = $crud->buscar($request,$this->Notificaciones);
         $resultado2 = [];
       foreach ($resultado as $id => $resultado_i){
         if($resultado[$id]->destino==Auth::user()->ident_usu){
         $resultado2[]=$resultado_i->id;
         }
       }
      $resultado = Notificaciones::whereIn('id',$resultado2)->latest('id')->paginate($resultados);
      foreach ($resultado as $id => $resultado_i){
        $resultado[$id]->destino_fk();
        $resultado[$id]->mensaje = Funciones::puntos_suspensivos($resultado[$id]->mensaje,100);
        $resultado[$id]->destino = $resultado[$id]->destino_fk->name;
        $resultado[$id]->fecha = Fecha::formato_fecha_mes_hora($resultado[$id]->created_at);
        #dd($resultado[$id]->fecha);
        }

      //dd($resultado);
         //dd($resultado->all());
         if (isset($_REQUEST['json'])){
            return $resultado;
         }else{
            return view('admin.notificaciones.index')->with(["resultado"=>$resultado]);
         }
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('admin.notificaciones.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
  public function enviar_notificacion($destino, $mensaje)
    {
        $notificaciones = new Notificaciones;
        $notificaciones->destino = $destino;
        $notificaciones->mensaje = $mensaje;
        $result = $notificaciones->save();
        return $result;
  }
    public function store(Request $request)
    {
        $result = $this->enviar_notificacion($request->remite, $request->destino, $request->mensaje);
        if($result)
        return Redirect::to('admin/notificaciones')->with('success', 'La Notificacion ha sido registrada correctamente.');
        else
        return Redirect::to('admin/notificaciones')->with('danger', 'Error, La Notificacion no ha sido registrada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notificaciones  $notificaciones
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $notificacion = Notificaciones::find($id);
      if($notificacion->estado=='pendiente') 
      Funciones::notif_estado($id);
      return view("admin.notificaciones.show")->with(["notificacion"=>$notificacion]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notificaciones  $notificaciones
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $notificaciones = Notificaciones::find($id);
        return view("admin.notificaciones.editar")->with(["Notificaciones"=>$notificaciones]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notificaciones  $notificaciones
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $notificaciones = Notificaciones::find($id);
        $notificaciones->remite = $request->remite;
        $notificaciones->destino = $request->destino;
        $notificaciones->mensaje = $request->mensaje;
        $result = $notificaciones->save();
        if($result)
        return Redirect::to('admin/notificaciones')->with('success', 'La Notificacion ha sido mdificada correctamente.');
        else
        return Redirect::to('admin/notificaciones')->with('danger', 'Error, La Notificacion no ha sido mdificada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notificaciones  $notificaciones
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $notificaciones = Notificaciones::find($id);
    $notificaciones->delete();
    if ($notificaciones->exists === false)
    return Redirect::to('admin/notificaciones')->with('success', 'La Notificacion ha sido eliminada correctamente.');
    else
    return Redirect::to('admin/notificaciones')->with('danger', 'Error, La Notificacion no ha sido eliminada.');
    }
}
