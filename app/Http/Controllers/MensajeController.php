<?php

namespace App\Http\Controllers;

use App\Mensaje;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Redirect;
use Auth;
use App\Fecha;
use App\Funciones;
class MensajeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function __construct()
    {
        //$this->middleware('auth');//auth|guest
    }
    public function index(Request $request)
    {
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $mensaje = new Mensaje;
        $mensaje->asigc_id = $request->asigc_id;
        $mensaje->de = $request->de;
        $mensaje->texto = $request->texto;
        $mensaje->rol = Auth::user()->rol;
        $result = $mensaje->save();
        if($result){
          return ['estado'=>1];
        }else{
          return ['estado'=>0];
        }
        //return Redirect::to('admin/facultad')->with('success', 'La Facultad ha sido registrada correctamente.');
        //else
        //return Redirect::to('admin/facultad')->with('danger', 'Error, La Facultad no ha sido registrada.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fac  $fac
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      // $dato = $request->get('mensaje');
         //dd($dato);
         //return 22;
      $retorno =  Mensaje::where('asigc_id',$id)->orderBy('id', 'ASC')->get();
      $datos = '';
         foreach ($retorno as $mensajei):
      $hace = Fecha::hace($mensajei->created_at);
        
if(Auth::user()->ident_usu == $mensajei->de_fk->ident_usu):
$datos .= '<li class="right clearfix">'.
            '<span class="chat-img pull-right">'.
              '<img src="https://placehold.it/50/FA6F57/fff&text=YO" alt="User Avatar" class="img-circle" />'.
            '</span>'.
            '<div class="chat-body clearfix">'.
               '<div class="header">'.
                  '<small class=" text-muted" title="'.Fecha::formato_fecha_hora($mensajei->created_at).'"><span class="glyphicon glyphicon-time"></span>'.$hace.'</small>'.
                     '<strong class="pull-right primary-font">'.
                      $mensajei->de_fk->nombre.' '.$mensajei->de_fk->apellido." (".Funciones::rol($mensajei->rol).")".
                      '</strong>'.
                                '</div>'.
                                '<p>'.
                                    utf8_decode($mensajei->texto).
                                '</p>'.
                            '</div>'.
                        '</li>';
else:
$datos .= '<li class="left clearfix">'.
            '<span class="chat-img pull-left">'.
              '<img src="https://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />'.
            '</span>'.
             '<div class="chat-body clearfix">'.
                '<div class="header">'.
                   '<strong class="primary-font">'.
                        htmlentities($mensajei->de_fk->nombre.' '.$mensajei->de_fk->apellido).
                   '</strong>'.
  '<small class=" text-muted" title="'.Fecha::formato_fecha_hora($mensajei->created_at).'"><span class="glyphicon glyphicon-time"></span>'.$hace.'</small>'.
                                '</div>'.
                                '<p>'.
                                   htmlentities($mensajei->texto).
                                '</p>'.
                            '</div>'.
                        '</li>';
endif;
endforeach;
         //$datos2 = "<li>" . implode("</li><li>", $datos) . "</li>";
         echo base64_encode($datos);die();
      /*
         $mensajes = Mensaje::where('asigc_id',$id)->orderBy('id', 'ASC')->get();
         $respuesta = $mensajes->all();   
         //dd($respuesta);
         return $respuesta;
         //return json_encode($respuesta);
    */
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fac  $fac
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fac  $fac
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fac  $fac
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      
    }
}
