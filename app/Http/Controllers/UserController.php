<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\User;
use App\Crud;
use Redirect;
use Hash;
use Auth;
class UserController extends Controller {

public $user;
public $palabra_clave_buscar = '';
public $excepciones_buscar = ['password'];
public $dato = '';//aux
public $resultados = 2;

   public function __construct()
    {
        $this->middleware('auth');//auth|guest
        #$this->middleware('rol:admin');//admin|docente|estudiante
        $this->middleware('rol:admin', ['only' => ['index','show','create','edit','destroy','store','update']]);//admin|docente|estudiante
    }

public function mi_perfil(){
    return view('auth.passwords.perfil');
}

public function cambiar_clave(Request $request){
        $user = User::find(Auth::user()->id);
        $user->nombre = $request->nombre;
        $user->apellido = $request->apellido;
        $user->name = $request->nombre." ".$request->apellido;
        $user->tel = $request->tel;
        $user->email = $request->email;
if(Hash::check($request->nocomplete1, $user->password)){
            if ($request->nocomplete2!="" and $request->nocomplete3!=""){
                      if($request->nocomplete2 == $request->nocomplete3){
                          if(Hash::check($request->nocomplete1, $user->password)){
                              $user->password = bcrypt($request->nocomplete2);//password_confirmation middleware
                              $user->save();
                              return redirect()->to('home')->with('success', 'La contraseña ha sido cambiada correctamente.');
                          }else{
                      #dd($request->all());

                          return redirect()->to('usuarios/mi_perfil')->with('warning', 'Error, La contraseña de confirmación no es correcta.');
                          }
                      }
            }
       $user->save();
       return redirect()->to('home')->with('success', 'Los datos han sido actualizados correctamente.');
            //->back();
  }else{
                  return redirect()->to('usuarios/mi_perfil')->with('warning', 'Error, La contraseña del usuario no es correcta.');
  }

}

public function index(Request $request){
     $crud = new Crud();
     $this->user = new User();
     $resultado = $crud->buscar($request,$this->user);
     if (isset($_REQUEST['json'])){
     $cont = ob_get_clean();
     return $resultado;
     }else{
     return view('admin.usuarios.index')->with(["resultado"=>$resultado]);
     }
}
public function create(){
     return view('auth.register');
}
public function show($id){
    $user = User::where('ident_usu',$id)->get()->first();
    $user = User::find($id);
    #dd($user);
    $tipo_identificacion = array('nombre_tipo_identificacion'=>'CC', 'id' => 1);
    return view('admin.usuarios.show')->with(["user"=>$user,"tipo_identificacion"=>$tipo_identificacion]);
}
public function destroy($id){
    $user = User::find($id);
    if (Auth::user()->id != $id){
    $user->delete();
    if ($user->exists === false)
    return Redirect::to('admin/usuarios')->with('success', 'El usuario ha sido eliminado correctamente.');
    else
    return Redirect::to('admin/usuarios')->with('danger', 'Error, El usuario no ha sido eliminado.');
    }else{
    return Redirect::to('admin/usuarios')->with('danger', 'Error, El usuario no ha sido eliminado, elimine este usuario desde otra cuenta de Administrador.');
    }
}
public function edit($id){
    $user = User::find($id);
    $tipo_identificacion = array();
    return view('admin.usuarios.editar')->with(["user"=>$user,"tipo_identificacion"=>$tipo_identificacion]);
}
public function store(UserRequest $request){
    //dd($request->all());
    $user = new User;
    $user->ident_usu = $request->ident_usu;
    $user->nombre = $request->nombre;
    $user->apellido = $request->apellido;
    $user->name = $request->nombre." ".$request->apellido;
    $user->rol = $request->rol;
    if (isset($request->roles) and !empty($request->roles)){
      $user->roles = implode(",",$request->roles);
    }else{
       $user->roles = $request->rol;
    }
    $user->tel = $request->tel;
    $user->email = $request->email;
    $user->password = bcrypt($request->password);//password_confirmation middleware
    if ($user->save()){
    return Redirect::to('admin/usuarios')->with(['notice'=>'El Usuario se ha registrado satisfactoriamente']);
    }else{
    return Redirect::to('admin/usuarios')->with(['notice'=>'Error, El Usuario no se ha registrado']);
    }
}
      /**
     * Función update
     * Actualiza la información del usuario
     *
     * @param  Request  $request
     *          $request recibe los datos de la petición
     * @return void
     */
public function update(Request $request)
{
    $user = User::find($request->id);
    $user->ident_usu = $request->ident_usu;
    $user->rol = $request->rol;
    if (isset($request->roles) and !empty($request->roles)){
     // dd($request->roles);
      $user->roles = implode(",",$request->roles);
}
    $user->tel = $request->tel;
    $user->nombre = $request->nombre;
    $user->apellido = $request->apellido;
    $user->name = $request->nombre." ".$request->apellido;
    $user->email = $request->email;
    if ($request->password!="") $user->password = bcrypt($request->password);//password_confirmation middleware
    $user->save();
    return Redirect::to('admin/usuarios')->with('notice', 'El usuario ha sido modificado correctamente.');
}

}
