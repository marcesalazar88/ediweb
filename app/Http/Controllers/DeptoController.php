<?php

namespace App\Http\Controllers;

use App\Depto;
use App\Fac;
use App\User;
use App\Crud;
use Illuminate\Http\Request;
use App\Http\Requests\DptoRequest;
use App\Http\Requests\DptoRequestUpdate;
use Redirect;
class DeptoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     2* @return \Illuminate\Http\Response
     */
     public $depto;
     public function __construct()
    {
        $this->middleware('auth');//auth|guest
        $this->middleware('rol:admin');//admin|docente|estudiante
    }
    public function index(Request $request)
    {
         $crud = new Crud();
         $this->depto = new Depto();
         $resultado = $crud->buscar($request,$this->depto);
         foreach ($resultado as $id => $resultadoi){
             $resultadoi->fac();
             $resultado[$id] = $resultadoi;
             $resultado[$id]->nom_fac = $resultadoi->fac->nom_fac;
             
         }
         $json = $request->get('json');
         if ($json){
         return $resultado;
         }else{
         return view('admin.departamento.index')->with(["resultado"=>$resultado]);
         }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fac = Fac::orderBy('nom_fac')->pluck('nom_fac', 'id');
        $usuarios = new User();
        return view('admin.departamento.create')->with(['fac'=>$fac,'usuarios'=>$usuarios->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DptoRequest $request)
    {
        //dd($request->all());
        $depto = new Depto;
        $depto->id = $request->id;
        $depto->nom_depto = $request->nom_depto;
        $depto->fac_id = $request->fac_id;
        $depto->director = $request->director;
        $result = $depto->save();
        if ($result)
        return Redirect::to('admin/departamento')->with('success', 'El departamento ha sido registrado correctamente.');
        else
        return Redirect::to('admin/departamento')->with('danger', 'Error, El departamento no ha sido registrado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fac  $depto
     * @return \Illuminate\Http\Response
     
            $prog->asigcs[$id]->director_fk();
            $director_fk = $prog->asigcs[$id]->docente_fk;
     */
    public function show($id)
    {
        return Redirect::to('admin/departamento');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fac  $depto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //dd($id);
        $depto = Depto::find($id);
        $fac = Fac::orderBy('nom_fac')->pluck('nom_fac', 'id');
        $usuarios = new User();
        return view('admin.departamento.editar')->with(["depto"=>$depto,"fac"=>$fac,'usuarios'=>$usuarios->all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fac  $depto
     * @return \Illuminate\Http\Response
     */
    public function update(DptoRequest $request, $id)
    {
        $depto = Depto::find($id);
        $depto->nom_depto = $request->nom_depto;
        $depto->fac_id = $request->fac_id;
        $depto->director = $request->director;
        $result = $depto->save();
        if ($result)
        return Redirect::to('admin/departamento')->with('success','El departamento ha sido modificado correctamente.');
        else
        return Redirect::to('admin/departamento')->with('danger','Error, El departamento no ha sido modificado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fac  $depto
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    $fac = Depto::find($id);
    $fac->delete();
    if ($fac->exists === false)
    return Redirect::to('admin/departamento')->with('notice', 'El departamento ha sido eliminado correctamente.');
    else
    return Redirect::to('admin/departamento')->with('notice', 'Error, El departamento no ha sido eliminado.');
    }
}