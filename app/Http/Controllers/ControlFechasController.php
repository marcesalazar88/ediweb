<?php

namespace App\Http\Controllers;

use App\ControlFechas;
use App\Crud;
use App\Fecha;
use App\User;
use Illuminate\Http\Request;
use App\Periodo;
use App\Depto;
use App\ProgTem;
use App\Seg;
use App\InfoFin;
use Auth;
use App\Http\Requests\ControlFechasRequest;

class ControlFechasController extends Controller
{
  public function __construct()
  {
      $this->middleware('auth');//auth|guest
      //$this->middleware('rol:admin|director');//admin|docente|director|estudiante
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
  public $formatos = [
         'prog_temt'=>'FOA-FR-07 Programación Temática',
         'seg'=>' FOA-FR-14 Seguimiento',
         'info_fin'=>'FOA-FR-13 Informe Final'
       ];
       
    public function index(Request $request)
    {
      $resultados = 10;
        if (isset($_GET['resultados'])){
            $resultados = $_GET['resultados'];
        }
        if (Auth::user()->rol=="admin"){
          $deptos = new Depto();
          $deptos = $deptos->all();
          
        }else if (Auth::user()->rol=="director"){       
            $id_user = Auth::user()->id;
            $user = User::find($id_user);
            $user->deptos();
            foreach($user->deptos as $deptoi){
              $deptos_asigc[]=$deptoi->id;
            }
        $deptos = Depto::whereIn('id',$deptos_asigc)->get();
        }
        foreach($deptos as $deptosi){
          $deptos_arr[]=$deptosi->id;
        }
         $crud = new Crud();
         $this->controlfechas = new ControlFechas();
         $_GET['paginate']=false;
         $resultado1 = $crud->buscar($request,$this->controlfechas);
         $resultado_ids=[];
         foreach ($resultado1 as $id => $resultado_i){
         $resultado_ids[] = $resultado1[$id]->id;
         }
        $resultado = Controlfechas::whereIn('id',$resultado_ids)
        ->whereIn('depto_id',$deptos_arr)
        ->paginate($resultados);

   foreach ($resultado as $id => $resultado_i){
    $resultado_i->depto();
    $resultado[$id]->depto->nom_depto;
    $resultado[$id]->fecha_aprobacion = Fecha::formato_fecha($resultado[$id]->fecha_aprobacion);
    $resultado[$id]->fecha_revision_doc = Fecha::formato_fecha($resultado[$id]->fecha_revision_doc);
    $resultado[$id]->fecha_revision_est = Fecha::formato_fecha($resultado[$id]->fecha_revision_est);
    if (isset($this->formatos[$resultado[$id]->formato])) $resultado[$id]->formato = $this->formatos[$resultado[$id]->formato];
  }
         $json = $request->get('json');
         if ($json){
         return ($resultado);
         die();
         }else{
         return view('admin.control.index')->with(["resultado"=>$resultado]);
         }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $periodo_activo = Periodo::where('estado','1')->get()->first();
        $periodos = new Periodo();
        if (Auth::user()->rol=="admin"){
                $deptos = new Depto();
                $deptos = $deptos->all();
        }else if (Auth::user()->rol=="director"){    
                  $id_user = Auth::user()->id;
                  $user = User::find($id_user);
                  $user->deptos();
                  foreach($user->deptos as $deptoi){
                    $deptos_asigc[]=$deptoi->id;
                  }
          $deptos = Depto::whereIn('id',$deptos_asigc)->get();
        }else{
          return  redirect()->route('home');
        }
              
        return view('admin.control.create')->with(["periodo_activo"=>$periodo_activo,"periodos"=>$periodos->all(),"deptos"=>$deptos,'formatos'=>$this->formatos]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ControlFechasRequest $request)
    {
    # dd($request);
    $controlfechas = new ControlFechas();
    $controlfechas->depto_id = $request->depto_id;
    $controlfechas->nombre_control = $request->nombre_control;
    $controlfechas->fecha_aprobacion = $request->fecha_aprobacion;
    $controlfechas->fecha_revision_doc = $request->fecha_revision_doc;
    $controlfechas->fecha_revision_est = $request->fecha_revision_est;
    $controlfechas->per_acad = $request->per_acad;
    $controlfechas->formato = $request->formato;
    $result = $controlfechas->save();
    if($result){
        return redirect()->route('admin.control.index')->with('success', 'El control de fechas ha sido registrado correctamente.');
    }else{
        return  redirect()->route('admin.control.index')->with('danger', 'Error, El control de fechas no ha sido registrado.');
    }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ControlFechas  $controlFechas
     * @return \Illuminate\Http\Response
     */
    public function show(ControlFechas $controlFechas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ControlFechas  $controlFechas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $controlFechas = ControlFechas::find($id);
        $periodo_activo = Periodo::where('estado','1')->get()->first();
        $periodos = new Periodo();
        if (Auth::user()->rol=="admin"){
                $deptos = new Depto();
                $deptos = $deptos->all();
        }else if (Auth::user()->rol=="director"){       
                  $id_user = Auth::user()->id;
                  $user = User::find($id_user);
                  $user->deptos();
                  foreach($user->deptos as $deptoi){
                    $deptos_asigc[]=$deptoi->id;
                  }
          $deptos = Depto::whereIn('id',$deptos_asigc)->get();
        }
        return view('admin.control.editar')->with(["controlFechas"=>$controlFechas, "periodo_activo"=>$periodo_activo, "periodos"=>$periodos->all(), "deptos"=>$deptos,'formatos'=>$this->formatos]);
  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ControlFechas  $controlFechas
     * @return \Illuminate\Http\Response
     */
    public function update(ControlFechasRequest $request, $id)
    {
       //dd($id);
        $controlfechas = ControlFechas::find($id);
        $controlfechas->depto_id = $request->depto_id;
        $controlfechas->nombre_control = $request->nombre_control;
        $controlfechas->fecha_aprobacion = $request->fecha_aprobacion;
        $controlfechas->fecha_revision_doc = $request->fecha_revision_doc;
        $controlfechas->fecha_revision_est = $request->fecha_revision_est;
        $prog_tems = ProgTem::where('control_fecha_id',$id)->get();
        foreach($prog_tems as $prog_tem){
        $prog_tem->fecha_aprobacion = $request->fecha_aprobacion;
        $prog_tem->fecha_revision_doc = $request->fecha_revision_doc;
        $prog_tem->save();
        }
        $segs = Seg::where('control_fecha_id',$id)->get();
        foreach($segs as $seg){
        $seg->fecha_aprobacion = $request->fecha_aprobacion;
        $seg->fecha_revision_doc = $request->fecha_revision_doc;
        $seg->fecha_revision_est = $request->fecha_revision_est;
        $seg->save();
        }
        $info_fins = InfoFin::where('control_fecha_id',$id)->get();
        foreach($info_fins as $info_fin){
        $info_fin->fecha_aprobacion = $request->fecha_aprobacion;
        $info_fin->fecha_revision_doc = $request->fecha_revision_doc;
        $info_fin->save();
        }
        
        $controlfechas->per_acad = $request->per_acad;
        $controlfechas->formato = $request->formato;
        $result = $controlfechas->save();
        if($result){
        return redirect()->route('admin.control.index')->with('success', 'El control de fechas ha sido actualizado correctamente.');
        }else{
        return  redirect()->route('admin.control.index')->with('danger', 'Error, El control de fechas no ha sido actualizado.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ControlFechas  $controlFechas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $controlFechas = ControlFechas::find($id);
        $controlFechas->delete();
        if ($controlFechas->exists === false)
        return redirect()->route('admin.control.index')->with('notice', 'El control de fechas ha sido eliminado correctamente.');
        else
        return redirect()->route('admin.control.index')->with('notice', 'Error, El control de fechas no ha sido eliminado.');
    }
}
