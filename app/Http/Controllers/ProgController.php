<?php

namespace App\Http\Controllers;

use App\Prog;
use App\Depto;
use Illuminate\Http\Request;
use App\Http\Requests\ProgRequest;
use App\Crud;
use Redirect;
class ProgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     
    public function __construct()
    {
        $this->middleware('auth');//auth|guest
        $this->middleware('rol:admin');//admin|docente|estudiante
    }
    public function index(Request $request)
    {
         $crud = new Crud();
         $this->prog = new Prog();
         $resultado = $crud->buscar($request,$this->prog);
            foreach ($resultado as $id => $resultadoi){
             $resultadoi->depto();
             $resultado[$id] = $resultadoi;
             $resultado[$id]->nom_depto = $resultadoi->depto->nom_depto;
             
            }
         $json = $request->get('json');
         if ($json){
         return $resultado;
         }else{
         return view('admin.programa.index')->with(["resultado"=>$resultado]);
         }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $depto = Depto::orderBy('nom_depto')->pluck('nom_depto', 'id');
        return view('admin.programa.create')->with(['depto'=>$depto]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProgRequest $request)
    {
        //dd($request->all());
        $prog = new Prog;
        $prog->nom_prog = $request->nom_prog;
        $prog->cod_prog = $request->cod_prog;
        $prog->depto_id = $request->depto_id;
        $prog->sede = $request->sede;
        $result = $prog->save();
        if($result)
        return Redirect::to('admin/programa')->with('success', 'El programa ha sido registrado correctamente.');
        else
        return Redirect::to('admin/programa')->with('danger', 'Error, El programa no ha sido registrado.');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Prog  $prog
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prog = Prog::find($id);
    if (isset($prog->id)){
        $prog->planes();
        $prog->depto();
        $prog->depto->fac();
        $fac = $prog->depto->fac;
        $prog->asigcs();
        $planes = $prog->planes;
        foreach ($prog->asigcs as $id => $asigc){
            $prog->asigcs[$id]->docente_fk();
            $docente_fk = $prog->asigcs[$id]->docente_fk;
            $prog->asigcs[$id]->estudiante_fk();
            $estudiante_fk = $prog->asigcs[$id]->estudiante_fk;
            $prog->asigcs[$id]->asigt();
            $asigt = $prog->asigcs[$id]->asigt;
        }
        return view('admin.programa.show')->with(['prog'=>$prog]);
    }else{
        return Redirect::to('admin/programa')->with('danger', 'No existen registros de este programa.');
    }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Prog  $prog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prog = Prog::find($id);
        $depto = Depto::orderBy('nom_depto')->pluck('nom_depto', 'id');
        return view('admin.programa.editar')->with(['prog'=>$prog,'depto'=>$depto]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Prog  $prog
     * @return \Illuminate\Http\Response
     */
    public function update(ProgRequest $request, $id)
    {
        //dd($request);
        $prog = Prog::find($id);
        $prog->nom_prog = $request->nom_prog;
        $prog->cod_prog = $request->cod_prog;
        $prog->depto_id = $request->depto_id;
        $prog->sede = $request->sede;
        $result = $prog->save();
        if($result)
        return Redirect::to('admin/programa')->with('success', 'El programa ha sido modificado correctamente.');
        else
        return Redirect::to('admin/programa')->with('danger', 'Error, El programa no ha sido modificado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Prog  $prog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prog = Prog::find($id);
        $prog->delete();
        if ($prog->exists === false)
        return Redirect::to('admin/programa')->with('success', 'El programa ha sido eliminado correctamente.');
        else
        return Redirect::to('admin/programa')->with('danger', 'Error, El programa no ha sido eliminado.');
    }
}
