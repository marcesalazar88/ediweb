<?php
namespace App\Http\Controllers;
use App\Push; use Illuminate\Http\Request; use Redirect; use Storage; use DateTime;
class PushController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth');//auth|guest
    }
   public static function push(Request $request)
    {
    $filename  = 'push.txt';//ruta del archivo 
    $msg = isset($request->msg) ? date("UTC") : '';// Se almacena un nuevo mensaje en el archivo, ejemplo timestamp
    if ($msg != '')
    {
      $fecha = new DateTime();
      Storage::disk('publico')->put($filename, $fecha->getTimestamp());//escribimos el fichero
      $last_modified = Storage::disk('publico')->lastModified($filename);
      $response = array('estado'=>1,'timestamp' => $last_modified);
      return json_encode($response);//devolvemos el valor timestamp para notificar el cambio
      die();
    }
    // Bucle infinito hasta que el archivo de datos no se modifica 
    $lastmodif = isset($request->timestamp) ? $request->timestamp : 0;
      //si tiene algun dato se asigna a $lastmodif, si no se iguala a 0
    $currentmodif = Storage::disk('publico')->lastModified($filename);//extrae la ultima fecha de modificacion
    while ($currentmodif <= $lastmodif) // comprobar si el archivo de datos se ha modificado
    {
      usleep(10000); // dormir 10 ms para descargar la CPU
      clearstatcache();
      $currentmodif = Storage::disk('publico')->lastModified($filename);
      //si la fecha/hora de modificacion es diferente a la del archivo, se rompe el ciclo y avanza despues del while
    }
    $response = array('timestamp' => $currentmodif);
    flush();//Vaciar el búfer de salida del sistema
    return json_encode($response);
    }   
}
