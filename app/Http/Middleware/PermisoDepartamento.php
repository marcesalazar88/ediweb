<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App/User;

class PermisoDepartamento
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $prog_id)
    {
      /*
      if($prog_id!=2000){
          return redirect('home')->with('warning', 'Error, Usted está intentando ingradar de manera incorrecta, verifique su información.');
      }
      */
      return $next($request);
      /*
    if (Auth::check()){
    if (Auth::user()->rol=="admin"){  
      return $next($request);
    }else if (Auth::user()->rol=="director"){
        $id_user = Auth::user()->id;
        $user = User::find($id_user);
        $user->deptos();
        $deptos_asigc=[];
        $planes_asigc=[];
        $progs_asigc=[];
        foreach($user->deptos as $deptoi){
          $deptos_asigc[]=$deptoi->id;
          $deptoi->progs();
          foreach($deptoi->deptos as $progi){
          $progs_asigc[]=$progi->id;
          //$progi->planes();
          
          }
        }
        if (in_array($prog_id,$progs_asigc)){
          return $next($request);
        }else{
          return redirect('home')->with('warning', 'Error, Usted está intentando ingradar de manera incorrecta, verifique su información.');
        }
    }else{
       return redirect('home')->with('warning', 'Error, Usted está intentando ingradar de manera incorrecta, verifique su información.');
    }
          
  }else{
      return redirect('home')->with('warning', 'Error, Usted está intentando ingradar de manera incorrecta, verifique su información.');
    }
    */
    }
}
