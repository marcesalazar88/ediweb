<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanesPreReq extends Model
{
    //SELECT `id`, `planes_id`, `semestre`, `asigt_id` FROM `planes_asigt` 
    //`id`, `planes_id`, `asigt`, `pre` FROM `planes_pre_req`
    public $timestamps = false;
    protected $table = 'planes_pre_req';
    protected $fillable = [
      'planes_asigt_id',
      'pre'
    ];

    public $fillcolumn = [
      'planes_asigt_id' => 'ID Planes Asigt',
      'pre' => 'ID Asigt Pre'
    ];
 public function asigt()
    {//Verificado
        return $this->belongsTo('App\Asigt','pre', 'codigo_asigt');
    }
}
