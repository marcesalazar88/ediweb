<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodigosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
         Schema::create('codigos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('token')->length(190)->unique();
            $table->integer('user_id')->unsigned();
            $table->integer('asigc_id')->unsigned();
            
            $table->foreign('user_id')
                    ->references('id')
                    ->on('usu')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
                    
             $table->foreign('asigc_id')
                    ->references('id')
                    ->on('asigc')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('codigos');
    }
}
