<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('planes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->length(190)->unique();
            $table->string('adjunto');
            $table->date('fecha');
            $table->string('cod_prog')->length(12);
            $table->text('observaciones');
           
           
            $table->foreign('cod_prog')
                    ->references('cod_prog')
                    ->on('prog')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
      
         Schema::create('planes_asigt', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('planes_id')->length(10);
            $table->integer('semestre');
            $table->string('asigt_id')->length(10);
            /*
            $table->foreign('planes_id')
                    ->references('id')
                    ->on('planes')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
           */
           $table->foreign('asigt_id')
                    ->references('codigo_asigt')
                    ->on('asigt')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
      
         Schema::create('planes_pre_req', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('planes_asigt_id');
            $table->string('pre')->length(10);
            
           /*
            $table->foreign('planes_asigt_id')
                    ->references('id')
                    ->on('planes_asigt')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
 */
            $table->foreign('pre')
                    ->references('codigo_asigt')
                    ->on('asigt')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
            
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planes_pre_req');
        Schema::dropIfExists('planes_asigt');
        Schema::dropIfExists('planes');
    }
}
