<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('links', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->length(190)->unique();
            $table->string('url');
            $table->string('target');
            $table->string('icono');
        });
         $data_links[] = [
        'id'=>'1',
        'nombre'=>'Universidad de Nariño',
        'url'=>'http://www.udenar.edu.co/',
        'target'=>'_blank',
        'icono'=>'<span class="glyphicon glyphicon-list"></span>'
         ];
         $data_links[] = [
        'id'=>'2',
        'nombre'=>'Departamento de Matematicas y Estadística',
        'url'=>'http://dematyes.udenar.edu.co/',
        'target'=>'_blank',
        'icono'=>'<span class="glyphicon glyphicon-list-alt"></span>'
         ];
         $data_links[] = [
        'id'=>'3',
        'nombre'=>'Programa Licenciatura en Informática',
        'url'=>'http://licinfo.udenar.edu.co/',
        'target'=>'_blank',
        'icono'=>'<span class="glyphicon glyphicon-list-alt"></span>'
         ];
         $data_links[] = [
        'id'=>'4',
        'nombre'=>'Satisfacción del sistema de información web',
        'url'=>'https://docs.google.com/forms/d/e/1FAIpQLSetTc75hpkd9Lp4C3VipktEIkrF4vrHlEaHXchXl-AlDmOiVA/viewform',
        'target'=>'_blank',
        'icono'=>'<span class="glyphicon glyphicon-stats"></span>'
         ];
         $data_links[] = [
        'id'=>'5',
        'nombre'=>'Manual Usuario Docente',
        'url'=>'https://drive.google.com/file/d/17p1fPROwPTHsvmFMyyJCBvzxHd1bdbey/view?usp=sharing',
        'target'=>'_blank',
        'icono'=>'<span class="glyphicon glyphicon-bookmark"></span>'
         ];
        \DB::table('links')->insert($data_links);
    }

/*
*/

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
