<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('prog', function (Blueprint $table) {
            $table->string('cod_prog')->length(12)->unique();
            $table->string('nom_prog')->length(190)->unique();
            $table->integer('depto_id')->length(10)->unsigned();
            $table->string('sede');
            $table->enum('periodo_ingreso',['A','B']); 
            $table->increments('id');
            $table->foreign('depto_id')
                    ->references('id')
                    ->on('depto')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prog');
    }
}
