<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeriodoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('periodo', function (Blueprint $table) {
            $table->string('periodo')->length(5)->unique();
            $table->enum('estado',['1','0']); 
            $table->enum('calendario',['A','B']);
            $table->date('fecha_inicio')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->string('acuerdo')->length(255)->nullable();
            $table->text('adjunto')->nullable();
           
            $table->primary('periodo');
        });
      
        $sug_per_acad = date("Y");
        $calendario = (date("m")<=6) ? 'A' : 'B';
        $sug_per_acad .= $calendario;
                                
        $data_usu[] = [
             'periodo'=>$sug_per_acad,
             'estado'=>'1',
             'calendario'=>$calendario
        ];
        \DB::table('periodo')->insert($data_usu);
      
        
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('periodo');
    }
}
