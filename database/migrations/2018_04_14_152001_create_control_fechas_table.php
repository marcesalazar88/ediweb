<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlFechasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
       public function up()
    {
         Schema::create('control_fechas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre_control');
            $table->date('fecha_aprobacion');
            $table->date('fecha_revision_doc');
            $table->date('fecha_revision_est')->nullable();
            $table->string('per_acad')->length(5);
            $table->integer('depto_id')->length(10)->unsigned();
            $table->enum('formato',['prog_temt', 'seg', 'info_fin']);
           
            $table->foreign('per_acad')
                    ->references('periodo')
                    ->on('periodo')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
           
            $table->foreign('depto_id')
                    ->references('id')
                    ->on('depto')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('control_fechas');
    }
}
