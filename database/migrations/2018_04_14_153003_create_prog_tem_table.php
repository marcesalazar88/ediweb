<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProgTemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
 
        //
      
    public function up()
    {
         Schema::create('prog_temt', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asigc_id')->unsigned()->length(10)->unique();//`asigc_id` INTEGER(10) NULL DEFAULT NULL
            $table->date('fecha_act_progc_temt')->nullable();//`fecha_act_progc_temt` DATE NULL DEFAULT NULL,
            $table->text('metodologia')->nullable();
            $table->text('crit_eva')->nullable();
            $table->integer('horas_practicas')->nullable();
            $table->integer('horas_teoricas')->nullable();
            $table->integer('horas_adicionales')->nullable();
            $table->string('rev_direct')->length(12)->nullable();
            $table->string('firm_docnt')->length(12)->nullable();
            $table->string('firm_est')->length(12)->nullable();
            $table->date('fecha_aprobacion')->nullable();
            $table->date('fecha_revision_doc')->nullable();
            $table->integer('control_fecha_id')->unsigned()->nullable();
            $table->string('codigo')->nullable();
            $table->enum('estado',['Pendiente', 'En Proceso', 'Listo']);
            $table->foreign('asigc_id')
                    ->references('id')
                    ->on('asigc')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
             
        });
        Schema::create('prog_tem_punto_adic', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prog_tem_id')->unsigned()->length(10);
            $table->text('item');
            $table->text('valor');
            
            $table->foreign('prog_tem_id')
                    ->references('id')
                    ->on('prog_temt')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
        Schema::create('prog_tem_refe_biblio', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('prog_tem_id')->unsigned()->length(10);
            $table->text('refe_biblio');
            $table->foreign('prog_tem_id')
                    ->references('id')
                    ->on('prog_temt')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('prog_tem_refe_biblio');
         Schema::dropIfExists('prog_tem_punto_adic');
         Schema::dropIfExists('prog_temt');
    }
}
