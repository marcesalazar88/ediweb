String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
function soloNumeros(e){
  //onKeyPress="return soloNumeros(event)"
  var key = window.Event ? e.which : e.keyCode
  //alert (key);
  return (key >= 48 && key <= 57);
}
if ($('[solonumeros]')){
$(document).find('[solonumeros]').each(function() {
    $(this).keypress(function(e){
     return soloNumeros(event);
    });
});
}
function dismiss_alert(){
    var da = document.querySelector(".close[data-dismiss=alert]");
    if(da) da.click();
}
function ajustar_pagina(response,fn){
$(".pagination li.active").removeClass('active');
$("#page_"+parseInt(response.current_page)+"").addClass('active');
if (response.per_page < response.total){
    var num_pag = Math.ceil(parseInt(response.total) / parseInt(response.per_page));
    var totales = document.createElement('div');
    var total = response.total; 
    var totales = document.createElement('div');
    totales.innerHTML += `Total Resultados: ${total}`;
    var paginar = document.createElement('ul');
    paginar.setAttribute('class','pagination');
    var prev = response.current_page-1;
    var next = response.current_page+1;
    if (response.current_page=='1'){
    paginar.innerHTML += `<li class="disabled"><a rel="prev">Anterior</a></li>`;
    }else{
    paginar.innerHTML += `<li><a onclick="buscar_${fn}('','${prev}');" rel="prev">Anterior</a></li>`;
    }
    for (var pag = 1; pag <= num_pag; pag++) {
        //if (pag<=15){
            
            var li = '';
            li += '<li ';
            if (response.current_page==pag) li += ' class="active" ';
            li +=`><a onclick="buscar_${fn}('','${pag}');"`;
            li +=`>${pag}</a></li>`;
            paginar.innerHTML += li;
        /*
            
        }else{
            if (pag==num_pag){
                var li = '';
                li += '<li><a>...</a></li>';
                li += '<li ';
                if (response.current_page==num_pag) li += ' class="active" ';
                li +=`><a onclick="buscar_${fn}('','${num_pag}');"`;
                li +=`>${pag}</a></li>`;
                paginar.innerHTML += li
                break;
            }
        }
        */
    }
    if (response.current_page==num_pag){
    paginar.innerHTML += `<li class="disabled"><a rel="next">Siguiente</a></li>`;
    }else{
    paginar.innerHTML += `<li><a onclick="buscar_${fn}('','${next}');" rel="next">Siguiente</a></li>`;
    }
    totales.style.textAlign = 'center';
    //$( totales).insertBefore( "#area_pagination" );
    $("#txt_resultados").append(totales);
    $("#area_pagination").html(paginar);
}else{
    $("#area_pagination").html('');
}
}
    
function buscar_usuarios(valor='', page=1){
console.log(valor);
var modulo = 'usuarios';
foreign = {};
otro = {
    'tipo_identificacion_id':{
        'data':{
        0:'id',
        1:'nombre_tipo_identificacion'
        },
        'label':{
        0:'ID',
        1:'Nombre Tipo de Identificación'
        },
        'campo':'tipo_identificacion'
    },
};
/*
    'tipo_identificacion_id':'Tipo de Identificación',
    'tipo_identificacion_id':'tipo_identificacion_id',
    */
label_data = {
        'ident_usu':'Identificación',
        'rol':'Rol',
        'tel':'Teléfono',
        'name':'Nombre',
        'email':'Correo Electrónico',
};

response_data = {
        'ident_usu':'ident_usu',
        'rol':'rol',
        'tel':'tel',
        'name':'name',
        'email':'email',
};
all_data = {
    pk:'id',
    'label_data':label_data,
    'response_data':response_data,
    'foreign':foreign,
};
buscar(modulo,all_data,valor='', page);
}
function buscar_planes(valor='', page=1){
var a5d546a503db8d=false;
var obj_a5d546a503db8d = document.getElementById('a5d546a503db8d');
if (obj_a5d546a503db8d){ 
  a5d546a503db8d=obj_a5d546a503db8d.value=='true';
}
var modulo = 'planes';
foreign = {
    'cod_prog':{'tabla':'prog', 'campo':'nom_prog'},
};

label_data = {
        'nombre':'Nombre',
        'adjunto':'Acuerdo',
        'fecha':'Fecha',
        'cod_prog':'Programa'
};

response_data = {
        'nombre':'nombre',
        'adjunto':'adjunto',
        'fecha':'fecha',
        'cod_prog':'cod_prog'
};
all_data = {
    pk:'id',
    'label_data':label_data,
    'response_data':response_data,
    'foreign':foreign,
};
detalle = true;
buscar(modulo,all_data,valor='', page, detalle,a5d546a503db8d);
}
function buscar_notificaciones(valor='', page=1){
var modificar=false;
var a5d546a503db8d=false;

var modulo = 'notificaciones';
foreign = {
   /* 'remite':{'tabla':'usu', 'campo':'ident_usu'},
    'destino':{'tabla':'usu', 'campo':'ident_usu'},*/
};

label_data = {
        'mensaje':'Mensaje',
        'fecha':'Fecha',
        'estado':'Estado'
};

response_data = {
        'mensaje':'mensaje',
        'fecha':'fecha',
        'estado':'estado'
};
all_data = {
    pk:'id',
    'label_data':label_data,
    'response_data':response_data,
    'foreign':foreign,
};
detalle = true;
nuevo=false;
eliminar=false;
  
buscar(modulo,all_data,valor='', page, detalle,a5d546a503db8d,modificar,nuevo,eliminar);
}
function buscar_facultad(valor='', page=1){
var modulo = 'facultad';
foreign = {};

label_data = {
    'id':'Código',
    'nom_fac':'Nombre'
};

response_data = {
        'id':'id',
        'nom_fac':'nom_fac'
};

all_data = {
    pk:'id',
    'label_data':label_data,
    'response_data':response_data,
    'foreign':foreign,
};
buscar(modulo,all_data,valor='', page);
}
function buscar_periodo(valor='', page=1){
var modulo = 'periodo';
foreign = {};

label_data = {
    'periodo':'Periodo',
    'estado':'Estado'
};

response_data = {
        'periodo':'periodo',
        'estado':'estado'
};

all_data = {
    pk:'periodo',
    'label_data':label_data,
    'response_data':response_data,
    'foreign':foreign,
};
buscar(modulo,all_data,valor='', page);
}
function buscar_asignatura(valor='', page=1){
var modulo = 'asignatura';
foreign = {};

label_data = {
    'codigo_asigt':'Código',
    'nom_asigt':'Nombre',
    'sem_ofrece_asigt':'Semestre',
    'ihs_asigt':'IHS',
    'n_cred_asigt':'N° de Creditos',
    //'just_asigt':'Justificación',
    //'obj_gen':'Objetivo General',
    //'obj_esp':'Objetivos Específicos',
    'area':'Área'
};

response_data = {
        'codigo_asigt':'codigo_asigt',
        'nom_asigt':'nom_asigt',
        'sem_ofrece_asigt':'sem_ofrece_asigt',
        'ihs_asigt':'ihs_asigt',
        'n_cred_asigt':'n_cred_asigt',
      //  'just_asigt':'just_asigt',
      //  'obj_gen':'obj_gen',
      //  'obj_esp':'obj_esp',
        'area':'area'
};

all_data = {
    pk:'id',
    'label_data':label_data,
    'response_data':response_data,
    'foreign':foreign,
};
detalle = true;
buscar(modulo,all_data,valor='', page, detalle);
}
function buscar_programa(valor='', page=1){
var modulo = 'programa';
foreign = {};

label_data = {
    'cod_prog':'Código',
    'nom_prog':'Nombre',
    'nom_depto':'Departamento',
    'sede':'Sede',
    'periodo_ingreso' : 'Periodo de ingreso',
};

response_data = {
    'cod_prog':'cod_prog',
    'nom_prog':'nom_prog',
    'nom_depto':'nom_depto',
    'sede':'sede',
    'periodo_ingreso':'periodo_ingreso',
};
all_data = {
    pk:'id',
    'label_data':label_data,
    'response_data':response_data,
    'foreign':foreign,
};
detalle = true;
buscar(modulo,all_data,valor='', page, detalle);
}
function buscar_asignacion(valor='', page=1){
    var modulo = 'asignacion';
foreign = {};

label_data = {
        'nom_asigt':'Asignatura',
        'docente':'Docente',
        'per_acad':'Periodo Académico',
        'nom_prog':'Programa'
};

response_data = {
    'nom_asigt':'nom_asigt',
    'docente':'docente',
    'per_acad':'per_acad',
    'nom_prog':'nom_prog'
};

all_data = {
    pk:'id',
    'label_data':label_data,
    'response_data':response_data,
    'foreign':foreign,
};
buscar(modulo,all_data,valor='', page, true);
}
function buscar_control(valor='', page=1){
var modulo = 'control';
foreign = {
   'depto_id':{'tabla':'depto', 'campo':'nom_depto'},
};

label_data = {
    'formato':'Formato',
    'nombre_control':'Nombre',
    'fecha_aprobacion':'Fecha de Revision de Docente Inicial',
    'fecha_revision_doc':'Fecha de Revision de Docente Final',
    'fecha_revision_est':'Fecha de Revison de Estudiante',
    'per_acad':'Periodo Académico',
    'depto_id':'Departamento'
};

response_data = {
    'formato':'formato',
    'nombre_control':'nombre_control',
    'fecha_aprobacion':'fecha_aprobacion',
    'fecha_revision_doc':'fecha_revision_doc',
    'fecha_revision_est':'fecha_revision_est',
    'per_acad':'per_acad',
    'depto_id':'depto_id'
};

all_data = {
    pk:'id',
    'label_data':label_data,
    'response_data':response_data,
    'foreign':foreign,
};
buscar(modulo,all_data,valor='', page);
}function buscar_links(valor='', page=1){
var modulo = 'links';
foreign = {};

label_data = {
     'nombre':'Nombre',
        'url':'Enlace',
        'target':'Abrir en',
        'icono':'Icono'
};

response_data = {
     'nombre':'nombre',
        'url':'url',
        'target':'target',
        'icono':'icono'
};

all_data = {
    pk:'id',
    'label_data':label_data,
    'response_data':response_data,
    'foreign':foreign,
};
buscar(modulo,all_data,valor='', page);
}
function buscar_departamento(valor='', page=1){
var modulo = 'departamento';
foreign = {};

label_data = {
    'id':'Código',
    'nom_depto':'Departamento',
    'nom_fac':'Facultad'
};

response_data = {
    'id':'id',
    'nom_depto':'nom_depto',
    'nom_fac':'nom_fac'
};

all_data = {
    pk:'id',
    'label_data':label_data,
    'response_data':response_data,
    'foreign':foreign,
};
buscar(modulo,all_data,valor='', page);
}

function buscar(modulo,all_data,valor='', page=1, detalle = false, a5d546a503db8d = true, modificar = true, nuevo = true, eliminar = true){
if (valor=='') 
var obj_valor = document.getElementById('buscar');
if (obj_valor)
var buscar_arreglo = document.getElementById('buscar_arreglo');
if (buscar_arreglo && buscar_arreglo.value=="SI"){
    buscar_arreglo_value = '1';
}else{
    buscar_arreglo_value = '0';
}
valor = obj_valor.value;
var obj_resultados = document.getElementById('num_resultados')
var resultados = (obj_resultados) ? obj_resultados.value : 10;
if (resultados>0){
var num_data = all_data.length;
var parametros = {};
        $.ajax({
                dataType: 'json',
                data:  parametros,
                url:   `?json=1&ba=${buscar_arreglo_value}&page=${page}&resultados=${resultados}&valor_clave=${valor}`,
                type:  'get',
                beforeSend: function () {
                    $("#txt_resultados").html(`<tr><td colspan="${num_data}">Procesando, espere por favor...</td></tr>`);
                },
                success:  function (response) {
                  //console.log(response);
                    //var datos = response;//ojo Crud devuelve .data con paginate
                    document.querySelector('#txt_resultados').innerHTML =  tabla(response,modulo,all_data,detalle, a5d546a503db8d,modificar,nuevo,eliminar);
                    ajustar_pagina(response,modulo);
                    setTimeout(function(){
                        dismiss_alert();
                    },4000);
                }
        });
}
}
/*
function ficha(response,modulo,all_data,detalle,a5d546a503db8d){
var tabla = '';
var datos = response.data;
var rdatos = all_data.response_data;
var num_data = all_data.length;
var label_data = all_data.label_data;
tabla += '<table class="table fill-table csstable cssresponsive">';
tabla += '<thead><tr>';
for (crow in label_data){
    tabla += `<th>${label_data[crow]}</th>`;
}
tabla += '<th class="noprint" >Acciones</th>';
tabla += `<th class="noprint thbotones sinbordertop" data-label="Nuevo"><a class="th-btn btn lista btn-primary" href="${modulo}/create" title="Nuevo">Nuevo</a></th>`;
tabla += '</tr></thead><tbody>';
if(datos)
for (data_row = 0; data_row < datos.length; data_row ++){
tabla += '<tr>';
    //console.log(datos[data_row]);
    for (row in rdatos){
    if (response_data[row] == 'adjunto'){
        tabla += `<td data-label="${label_data[row]}"><a href="${datos[data_row][row]}"><img src="/img/pdf.png"></a></td>`;
    }else{
        if (all_data.foreign[row]){
            tabla += `<td data-label="${label_data[row]}">${datos[data_row][all_data.foreign[row]['tabla']][all_data.foreign[row]['campo']]}</td>`;
        }else{
            tabla += `<td data-label="${label_data[row]}">${datos[data_row][row]}</td>`;
        }
    }
    }
    var dpk = datos[data_row][all_data.pk];
    tabla += `<td class="noprint"><a class="btn lista btn-info" href="${modulo}/${dpk}/edit">Modificar</a></td>`;
    tabla += `<td class="noprint"><a class="btn lista btn-danger" onclick="return confirm('¿Esta ud seguro que quiere eliminar el registro?')" href="${modulo}/${dpk}/delete">Eliminar</a></td>`;
    tabla += '</tr>';
}
tabla += '</tbody></table>';
return tabla;
}
  */
function tabla(response,modulo,all_data,detalle,a5d546a503db8d,modificar,eliminar,nuevo){
var tabla = '';
var datos = response.data;
var rdatos = all_data.response_data;
var num_data = all_data.length;
var label_data = all_data.label_data;
tabla += '<table class="table fill-table table-hover csstable cssresponsive">';
tabla += '<thead><tr>';

for (crow in label_data){
    if(modulo=='notificaciones'  && label_data[crow] == 'Estado'){
              
    }else if (modulo == 'asignacion' && label_data[crow] == "Estudiante"){
    
    }else{
        tabla += `<th>${label_data[crow]}</th>`;    
    }
}
  
if(a5d546a503db8d){
tabla += '<th class="noprint" >Acciones</th>';
if (nuevo){
if (modulo == 'planes'){
 var aux_cod_prog = document.getElementById('buscar_programa').value;
tabla += `<th class="noprint thbotones" data-label="Nuevo">
<a class="th-btn btn lista btn-primary hidden-xs" href="${modulo}/${aux_cod_prog}/create" title="Nuevo">Nuevo</a>
<a class="badge hidden-sm hidden-md hidden-lg" style="color: #fff;
    background-color: #3097D1;
    border-color: #2a88bd;
    height: 42px;
    width: 42px;
    /*border-radius: 50%;*/
    text-align: center;
    vertical-align: middle;
    vertical-align: -webkit-baseline-middle;
    font-size: 37px;" href="${modulo}/${aux_cod_prog}/create" title="Nuevo">
+
</a>
</th>`;
}else{
tabla += `<th class="noprint thbotones" data-label="Nuevo">
<a class="th-btn btn lista btn-primary hidden-xs" href="${modulo}/create" title="Nuevo">Nuevo</a>
<a class="badge hidden-sm hidden-md hidden-lg" style="color: #fff;
    background-color: #3097D1;
    border-color: #2a88bd;
    height: 42px;
    width: 42px;
    /*border-radius: 50%;*/
    text-align: center;
    vertical-align: middle;
    vertical-align: -webkit-baseline-middle;
    font-size: 37px;" href="${modulo}/create" title="Nuevo">
+
</a>
</th>`;
}
}

}
tabla += '</tr></thead><tbody>';
if(datos)
for (data_row = 0; data_row < datos.length; data_row ++){
var dpk = datos[data_row][all_data.pk];
var estado = datos[data_row]['estado'];
if(modulo=='notificaciones')
tabla += `<tr onclick="window.location.href='${modulo}/${dpk}'" class="${estado} hoverable">`;
else
tabla += '<tr>';
    //console.log(datos[data_row]);
    for (row in rdatos){
        /*
    if (all_data.foreign[row])
    tabla += `<td data-label="${label_data[row]}">${datos[data_row][all_data.foreign[row]['campo']]}</td>`;
    else
    tabla += `<td data-label="${label_data[row]}">${datos[data_row][row]}</td>`;
    */
    if (response_data[row] == 'adjunto'){
        tabla += `<td data-label="${label_data[row]}"><a href="${datos[data_row][row]}" target="_blank"><img src="/img/pdf.png"></a></td>`;
    }else{
       
        if (all_data.foreign[row]){
            tabla += `<td data-label="${label_data[row]}">${datos[data_row][all_data.foreign[row]['tabla']][all_data.foreign[row]['campo']]}</td>`;
        }else{
            var datoaux = datos[data_row][row];
          //console.log(response_data[row]);
            if(modulo=='notificaciones'  && response_data[row] == 'estado'){
              
            }else if (modulo == 'asignacion' && response_data[row] == 'estudiante'){
                var datoest = datos[data_row][row]; if (datoest==null) datoest ='';
            }else{
            tabla += `<td data-label="${label_data[row]}">${datos[data_row][row]}`;
             if (response_data[row] == 'codigo' &&  datoaux == ''){
                if (datoest == ''){
                    tabla += `<br><a class="btn lista btn-info" href="codigos/${datos[data_row]['id']}/create">Generar</a>`;
                }else{
                    
                    tabla += `${datoest}`;
                }
             }
             tabla += `</td>`;
            }
        }
        
    }
    }
    var dpk = datos[data_row][all_data.pk];
     
    tabla += `<td class="noprint">`;
    if(a5d546a503db8d){
    if(detalle)
    if(modulo != "notificaciones")
    tabla += `<a class="btn lista btn-secondary" href="${modulo}/${dpk}">Detalles</a><br>`;
    if(modificar) tabla += `<a class="btn lista btn-info" href="${modulo}/${dpk}/edit">Modificar</a><br>`;
    tabla += `</td>`;
    if (nuevo){
    tabla += `<td class="noprint"><a class="btn lista btn-danger" onclick="return confirm('¿Esta ud seguro que quiere eliminar el registro?')" href="${modulo}/${dpk}/delete">Eliminar</a></td>`;
    }
    }
    tabla += '</tr>';
}
tabla += '</tbody></table>';
return tabla;
}
function required_en_formulario(id_formulario,color,elemento){
    if ($("#"+id_formulario)){
    $("#"+id_formulario).find(':input').each(function() {
    if(this.type!="hidden" && this.type!="button" ){
    var estado = document.getElementById(this.id);
    if(estado && estado.required==true){ $("#"+this.id).before("<font style='font-size: 14pt;' color='"+color+"'>"+elemento+"</font>");  }  
    }
    });  
    }
}
function required_en_formulario_for(id_formulario,color,elemento){
        if ($("#"+id_formulario)){
        $("#"+id_formulario).find(':input').each(function() {
        if(this.type!="hidden" && this.type!="button" ){
        var estado = document.getElementById(this.id);
        if(estado && estado.required==true){
            var destino = $("[for="+this.id+"]");
            if (destino) $("[for="+this.id+"]").append(" <font color='"+color+"'>"+elemento+"</font>");
        }  
        }
        });  
    }
}

function password_en_formulario(id_formulario){
$("#"+id_formulario).find(':input').each(function() {
if(this.type=="password"){
  $("#"+this.id).after("<label><input type='checkbox' onclick=\"document.getElementById('"+this.id+"').type = document.getElementById('"+this.id+"').type == 'text' ? 'password' : 'text'\"> Ver</label>");
}
});
}
//
/*
Creada por: Manuel Cerón
Fecha: 11/10/2017
Dependencias: Jquery
Ejemplo de uso en HTML
<div class="form-group">
    <label>Deporte</label>
    
    <input type="text" name="deportes[]" mas_multiple="deporte" contenedor="span" ubicacion_mas="before" placeholder="Deporte">

</div>
<div class="form-group">
    <label>Telefono</label>
    
    <telefonos mas_multiple="telefono" contenedor="div" ubicacion_mas="after">
    <input type="text" name="telefono[]" placeholder="Telefono"/>
    <input type="text" name="lugar_telefono[]" placeholder="Lugar"/>
    </telefonos>
    
</div>
*/
function quitar(objeto){
    var padre = objeto.parentNode;
    var abuelo = padre.parentNode;
    //console.log(abuelo);
    if (abuelo.childElementCount>1){
        abuelo.removeChild(padre);
    }
}
function quitar_nodo(objeto){
    var padre = objeto.parentNode;
    //console.log(padre);
    if (padre.childElementCount && padre.childElementCount>1){
        padre.removeChild(objeto);
    }
}
function clonar_v1(grupo){
    var ref = $("[mas_multiple="+grupo+"]:last");
    var clonado = $(ref).clone();
    var index = $(ref).index();
    index = parseInt(index)+1;
    $(ref).attr("id", grupo+index);
    //console.log($(ref));
    $("[mas_multiple="+grupo+"]:last").after(clonado);
    $("[mas_multiple="+grupo+"]:last").find("span").each(function() {
        $(this).attr("id", "txt"+grupo+index);
    });
    var count_i = $("[mas_multiple="+grupo+"]:last").attr("i");
    if (count_i){
    $(this).attr("i",parseInt(count_i)+1);
    count_i = parseInt(count_i)+1;
    $("[mas_multiple="+grupo+"]:last").attr("i",count_i);
    $("[mas_multiple="+grupo+"]:last").find("[name*="+grupo+"_]").each(function() {
       var name_old = $(this).attr("name");
       var name_new = name_old.replaceAll(grupo+"_"+(parseInt(count_i)-1),grupo+"_"+count_i);
       $(this).attr("name",name_new); 
       //console.log(name_new);
    });
    }
    $("[mas_multiple="+grupo+"]:last").find("input").each(function() {
        $(this).val("");
        $(this).attr("id", grupo+index);
        $(this).keyup();
        $(this).focus();
    });
}
function clonar(grupo){
    var ref = $("[mas_multiple="+grupo+"]:last");
    var version = $(ref).attr("version");
    version = version ? version : "1";
    if (version=="1"){
        clonar_v1(grupo);
    }else if (version=="2"){
        clonar_v2(grupo);
    }
    //refrescar_funciones_input();
}
function clonar_v2(grupo){
    var ref = $("[mas_multiple="+grupo+"]:last");
    var clonado = $(ref).clone();
    var index = $(ref).index();
    index = parseInt(index)+1;
    $(ref).attr("id", grupo+index);
    //console.log($(ref));
    $("[mas_multiple="+grupo+"]:last").after(clonado);
    $("[mas_multiple="+grupo+"]:last").find("span").each(function() {
        $(this).attr("id", "txt"+grupo+index);
    });
    var count_i = $("[mas_multiple="+grupo+"]:last").attr("i");
    var count_ia = count_i;
    if (count_i){
    $(this).attr("i",parseInt(count_i)+1);
    count_i = parseInt(count_i)+1;
    $("[mas_multiple="+grupo+"]:last").attr("i",count_i);
    $("[mas_multiple="+grupo+"]:last").find("[name*="+grupo+"_]").each(function() {
       var name_old = $(this).attr("name");
       var name_new = name_old.replaceAll(grupo+"_"+count_ia,grupo+"_"+count_i);
       $(this).attr("name",name_new); 
       //console.log(name_new);
       
             var onchange_old = $(this).attr("onchange");
           if (onchange_old){
           var onchange_new = onchange_old.replaceAll(grupo+"_"+count_ia,grupo+"_"+count_i);
           $(this).attr("onchange",onchange_new);
           }
           
           var onclick_old = $(this).attr("onclick");
           if (onclick_old){
            var onclick_new = onclick_old.replaceAll(grupo+"_"+count_ia,grupo+"_"+count_i);
            $(this).attr("onclick",onclick_new); 
           }
           
    });
    }
    $("[mas_multiple="+grupo+"]:last").find(".actualizar").each(function() {
        var class_old = $(this).attr("class");
        if (class_old){
        if (class_old.indexOf(grupo+"_"+count_ia) > -1)
        var class_new = class_old.replaceAll(grupo+"_"+count_ia,grupo+"_"+count_i);
        $(this).attr("class",class_new); 
        }
    });
    $("[mas_multiple="+grupo+"]:last").find("input").each(function() {
        $(this).val("");
        $(this).keyup();
    });
     $("[mas_multiple="+grupo+"]:last").find("[id*="+grupo+"_]").each(function() {
        var id_old = $(this).attr("id");
        if (id_old.indexOf(grupo+"_"+count_ia) > -1)
        var id_new = id_old.replaceAll(grupo+"_"+count_ia,grupo+"_"+count_i);
        $(this).attr("id",id_new);
    });
     $("[mas_multiple="+grupo+"]:last").find("label").each(function() {
        var id_old = $(this).attr("id");
        if (id_old.indexOf(grupo+"_"+count_ia) > -1)
        var id_new = id_old.replaceAll(grupo+"_"+count_ia,grupo+"_"+count_i);
        $(this).attr("id",id_new);
    });
    $("[mas_multiple="+grupo+"]:last").find("[onclick*=parentNode]").each(function() {
        $(this).click();
    });
    $("[mas_multiple="+grupo+"]:last").find("input").filter(":first").focus();
  
}
function clonar_elemento(grupo,obj){
    var ref = $("[mas_multiple="+grupo+"]:last");
    var version = $(ref).attr("version");
    version = version ? version : "1";
    if (version=="1"){
        clonar_elemento_v1(grupo,obj);
    }else if (version=="2"){
        clonar_elemento_v2(grupo,obj);
    }
}
function clonar_elemento_v1(grupo,obj){
    var ref = obj.parentNode.parentNode.parentNode.parentNode;
    //console.log(ref);
   $(ref).find("[mas_multiple="+grupo+"]:last").each(function() {
       //console.log(this);
        var grupo =  $(this).attr("mas_multiple");
        var clonado = $(this).clone();
        var index = $(this).index();
        index = parseInt(index)+1;
        $(this).attr("id", grupo+index);
        //console.log($(this));
        $(this).after(clonado);
        $(this).next().find("span").each(function() {
            $(this).attr("id", "txt"+grupo+index);
        });
        var count_i = $(this).attr("i");
        if (count_i){
        count_i = parseInt(count_i)+1;
        $(this).next().attr("i",count_i);
        $(this).next().find("[name*="+grupo+"_]").each(function() {
            var name_old = $(this).attr("name");
            var name_new = name_old.replace(grupo+"_"+(parseInt(count_i)-1),grupo+"_"+count_i);
            $(this).attr("name",name_new);
        });
         $(this).next().find("input").each(function() {
            $(this).val("");
            $(this).attr("id", grupo+count_i);
            $(this).keyup();
            $(this).focus();
        });
        }else{
        $(this).next().find("input").each(function() {
            $(this).val("");
            $(this).attr("id", grupo+index);
            $(this).keyup();
            $(this).focus();
        });
        }
        
    
    });
}
function clonar_elemento_v2(grupo,obj){
    var ref = obj.parentNode.parentNode.parentNode.parentNode;
    //console.log(ref);
   $(ref).find("[mas_multiple="+grupo+"]:last").each(function() {
       //console.log(this);
        
        var clonado = $(this).clone();
        var index = $(this).index();
        index = parseInt(index)+1;
        $(this).attr("id", grupo+index);
        //console.log($(this));
        $(this).after(clonado);
        $(this).next().find("span:not([no_id_index])").each(function() {
            $(this).attr("id", "txt"+grupo+index);
        });
        var count_i = $(this).attr("i");
        var count_ia = count_i;
        if (count_i){
        count_i = parseInt(count_i)+1;
        $(this).next().attr("i",count_i);
        $(this).next().find("[name*="+grupo+"_]").each(function() {
            var name_old = $(this).attr("name");
            var name_new = name_old.replaceAll(grupo+"_"+count_ia,grupo+"_"+count_i);
            $(this).attr("name",name_new);
            
           var onchange_old = $(this).attr("onchange");
           if (onchange_old){
           var onchange_new = onchange_old.replaceAll(grupo+"_"+count_ia,grupo+"_"+count_i);
           $(this).attr("onchange",onchange_new);
           }
           
           var onclick_old = $(this).attr("onclick");
           if (onclick_old){
            var onclick_new = onclick_old.replaceAll(grupo+"_"+count_ia,grupo+"_"+count_i);
            $(this).attr("onclick",onclick_new); 
           }
           
         
       
        });
         $(this).next().find("[id*="+grupo+"_]").each(function() {
            var id_old = $(this).attr("id");
            var id_new = id_old.replaceAll(grupo+"_"+count_ia,grupo+"_"+count_i);
            $(this).attr("id",id_new);
        });
        }
        $(this).next().find("input").each(function() {
                 var class_old = $(this).attr("class");
               if (class_old){
                var class_new = class_old.replaceAll(grupo+"_"+count_ia,grupo+"_"+count_i);
                $(this).attr("class",class_new); 
               }
            $(this).val("");
            $(this).keyup();
        });
        $(this).next().find("input").filter(":first").focus();
        
    
    });
    
}


$(document).ready(function() {
if ($('[mas_multiple]')){
    
$(document).find('[mas_multiple]').each(function() {
    var contadori = $(this).attr("i");
    var estado = $(this).attr("estado");
    if (estado!="activo"){
    var version = $(this).attr("version");
    version ? version : "1";
    if (version==="1"){
        /**/
    mas_multiple = $(this).attr("mas_multiple");
    selector = $(this).attr("contenedor") ? $(this).attr("contenedor") : 'div' ;
    ubicacion_mas = $(this).attr("ubicacion_mas") ? $(this).attr("ubicacion_mas") : 'before' ;
    $(this).removeAttr("mas_multiple");
    $(this).removeAttr("contenedor");
    $(this).removeAttr("ubicacion_mas");
    span1 = document.createElement('span');
    button_mas = document.createElement('button');
    button_mas.type="button";
    $(button_mas).addClass("button_mas");
    $(button_mas).addClass("badge");
    $(button_mas).addClass("btn-success");
    $(button_mas).html("+");
    var onclick_mas = document.createAttribute('onclick');
    onclick_mas.value="clonar_v1('"+mas_multiple+"');";
    button_mas.setAttributeNode(onclick_mas);
    button_menos = document.createElement('button');
    button_menos.type="button";
    $(button_menos).addClass("button_menos");
    $(button_menos).addClass("badge");
    $(button_menos).addClass("btn-danger");
    $(button_menos).html("-");
    
    var onclick_menos = document.createAttribute('onclick');
    onclick_menos.value="quitar(this);";
    button_menos.setAttributeNode(onclick_menos);

    span2 = document.createElement(selector);
    $(span2).attr("mas_multiple",mas_multiple);
    $(span2).attr("estado","activo");
    $(span2).attr("i",contadori);
    $(span2).attr("version",version);
    elemento = $(this).clone();
    $(span2).append(elemento);
    if (ubicacion_mas=='before'){
    $(this).before('<br>');
    $(this).before(button_mas);   
    }else if (ubicacion_mas=='after'){
    $(this).after('<br>');   
    $(this).after(button_mas);   
    }
    $(span2).append(button_menos);
    $(span1).append(span2);
    $(this).replaceWith(span1);
        /**/
    }else if (version==="2"){
    mas_multiple = $(this).attr("mas_multiple");
    selector = $(this).attr("contenedor") ? $(this).attr("contenedor") : 'div' ;
    ubicacion_mas = $(this).attr("ubicacion_mas") ? $(this).attr("ubicacion_mas") : 'before' ;
    $(this).removeAttr("mas_multiple");
    $(this).removeAttr("contenedor");
    $(this).removeAttr("ubicacion_mas");
    span1 = document.createElement('span');
    button_mas = document.createElement('button');
    button_mas.type="button";
    $(button_mas).addClass("button_mas");
    $(button_mas).addClass("badge");
    $(button_mas).addClass("btn-success");
    $(button_mas).html("+");
    var onclick_mas = document.createAttribute('onclick');
    onclick_mas.value="clonar('"+mas_multiple+"');";
    button_mas.setAttributeNode(onclick_mas);
    button_menos = document.createElement('button');
    button_menos.type="button";
    $(button_menos).addClass("button_menos");
    $(button_menos).addClass("badge");
    $(button_menos).addClass("btn-danger");
    $(button_menos).html("-");
    
    var onclick_menos = document.createAttribute('onclick');
    onclick_menos.value="quitar(this);";
    button_menos.setAttributeNode(onclick_menos);

    span2 = document.createElement(selector);
    $(span2).attr("mas_multiple",mas_multiple);
    $(span2).attr("estado","activo");
    $(span2).attr("i",contadori);
    $(span2).attr("version",version);
    elemento = $(this).clone();
    $(span2).append(elemento);
    if (ubicacion_mas=='before'){
    $(this).before('<br>');
    $(this).before(button_mas);   
    }else if (ubicacion_mas=='after'){
    $(this).after('<br>');   
    $(this).after(button_mas);   
    }
    $(span2).append(button_menos);
    $(span1).append(span2);
    $(this).replaceWith(span1);
    }
    }//fin estado activo
});//fin mas_multiple
}
});//fin ready
    function alplicar_estilos(destino=''){
      console.log(destino);
             if(document.getElementById("switch-state" + destino).checked==true){
                document.getElementById("enproceso"+ destino).style.color='#000';
                document.getElementById("enproceso"+ destino).style.backgroundColor='#eeeeee';
                document.getElementById("enproceso"+ destino).style.borderColor='#eeeeee';
    
                document.getElementById("listo"+ destino).style.color='#FFF';
                document.getElementById("listo"+ destino).style.backgroundColor='#2ab27b';
                document.getElementById("listo"+ destino).style.borderColor='#259d6d';
             
            }else{
                document.getElementById("listo"+ destino).style.color='#000';
                document.getElementById("listo"+ destino).style.backgroundColor='#eeeeee';
                document.getElementById("listo"+ destino).style.borderColor='#eeeeee';
    
                document.getElementById("enproceso"+ destino).style.color='#FFF';
                document.getElementById("enproceso"+ destino).style.backgroundColor='#cbb956';
                document.getElementById("enproceso"+ destino).style.borderColor='#c5b143';
              
            }
           }
  function guardar_estado(formato, campo, valor,estado,campo_form,url,destino=''){
      var parametros = {
      formato : formato,
      campo : campo,
      valor : valor
    }
        $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                dataType: 'json',
                data:  parametros,
                url:   url,
                type:  'post',
                beforeSend: function () {
                },
                success:  function (response) {
                  if (response == "1"){
                        document.getElementById(campo_form+destino).checked=estado;
                        alplicar_estilos(destino);
                  }else{
                   
                  return false;
                  }
                }
        });
}