@extends('layouts.app')
@section('vars')
{!! $page_title = 'Usuarios'; 
!!}
@endsection
@section('barra_buscar_cuerpo')
<nav class="navbar navbar-default">
  <ul class="nav navbar-nav navbar-right">
      @include('layouts.partials.buscar')
  </ul>
</nav>
@endsection
@section('scripts')
<script>
$(document).ready(function(){
$("#buscar").keyup(function(){
  buscar_usuarios();
});
$("#buscar").change(function(){
  buscar_usuarios();
});
$("#search-btn").click(function(){
  buscar_usuarios();
});
$("#num_resultados").keyup(function(){
  buscar_usuarios();
});
$("#num_resultados").change(function(){
  buscar_usuarios();
});
$("#menu_usuarios").addClass('active');
  buscar_usuarios();
});
</script>
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> {{ $page_title }}</li>
</ol>
@endsection
@section('content')
<center><h1>{{ $page_title }}</h1></center>
<div class='row'>
  <div class='col-md-12'>
  <span id="txt_resultados"></span>
 <div id="area_pagination"></div>
    
    
  </div><!-- /.col -->
  <div class='col-md-6'>

  </div><!-- /.col -->

</div><!-- /.row -->
@endsection