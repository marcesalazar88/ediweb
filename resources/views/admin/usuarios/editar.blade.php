@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Usuarios'; 
$route = 'usuarios'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ route('admin.usuarios.index') }}"> {{ $page_title }}</a></li>
    <li class="active"> Modificar</li>
</ol>
@endsection
@section('scripts')
<script> 
  function opselectroles(){
    $('#select1').html(`<option value=""> 
                                  Seleccione un rol predeterminado
                              </option>`);
        $('.chk_roles').each(function( index ) {
          if (this.checked){
            var optionValue = $(this).val();
            var optionText = $("#label_"+optionValue).html();
            var selected = '';
            var rol_actual = $('#rol').val();
            console.log(rol_actual);
            if (rol_actual==optionValue){
                 selected = ' selected ';
                }
            $('#select1').append(`<option ${selected} value="${optionValue}"> 
                                   ${optionText} 
                              </option>`); 
          }
  });
}
$(document).ready(function(){
  opselectroles();
  $('.chk_roles').change(function(){
    opselectroles();
  }); 
});
</script> 

<script>
    $(document).ready(function() {
        //password_en_formulario("admin_usuarios_create");
        required_en_formulario_for("admin_usuarios_create","red","*")
    });
    
    function validar_area_password(obj){
        document.getElementById('area_password').style.display = obj.checked ? 'block' : 'none';
        document.getElementById('password').value='';
        document.getElementById('password-confirm').value='';
        $('#password').prop('required',obj.checked);
        $('#password-confirm').prop('required',obj.checked);
    }
</script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    <form method="POST" id="admin_usuarios_create" action="{{ route('admin.usuarios.update',[
                    'usuario' => $user->id,
                    ]) }}">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <h1 class="text-center">Modificar Usuario</h1>

    <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
        {{ csrf_field() }}
        <div class="form-group{{ $errors->has('ident_usu') ? ' has-error' : '' }}">
            <label for="ident_usu" class="col-md-4 control-label">Identificación</label>

            <div class="col-md-6">
                <input id="ident_usu" type="text" class="form-control" name="ident_usu" value="{{ $errors->has('ident_usu') ?  old('ident_usu') : $user->ident_usu }}" required autofocus>

                @if ($errors->has('ident_usu'))
                    <span class="help-block">
                        <strong>{{ $errors->first('ident_usu') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('nombre') ? ' has-error' : '' }}">
            <label for="nombre" class="col-md-4 control-label">Nombre</label>

            <div class="col-md-6">
                <input id="nombre" type="text" class="form-control" name="nombre" value="{{ $errors->has('nombre') ?  old('nombre') : $user->nombre }}" required autofocus>

                @if ($errors->has('nombre'))
                    <span class="help-block">
                        <strong>{{ $errors->first('nombre') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('apellido') ? ' has-error' : '' }}">
            <label for="apellido" class="col-md-4 control-label">Apellido</label>

            <div class="col-md-6">
                <input id="apellido" type="text" class="form-control" name="apellido" value="{{ $errors->has('apellido') ?  old('apellido') : $user->apellido }}" required autofocus>

                @if ($errors->has('apellido'))
                    <span class="help-block">
                        <strong>{{ $errors->first('apellido') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('rol') ? ' has-error' : '' }}">
            <label for="rol" class="col-md-4 control-label">Rol(es)</label>

            <div class="col-md-6">
              <label><input <?php 
$user_roles = explode(",",$user->roles);
if (count($errors)>0){
     if (in_array('admin',old('roles'))){ ?> checked <?php }//endif;
}else{
      if (in_array('admin',$user_roles)){ ?> checked <?php }//endif;
}//endif;
                            ?> type="checkbox" name="roles[]" class="chk_roles" id="roles_admin" value="admin"><span id="label_admin">Administrador</span></label>
              <br>
              <label><input <?php 
$user_roles = explode(",",$user->roles);
if (count($errors)>0){
     if (in_array('director',old('roles'))){ ?> checked <?php }//endif;
}else{
      if (in_array('director',$user_roles)){ ?> checked <?php }//endif;
}//endif;
                            ?>  type="checkbox" name="roles[]" class="chk_roles" id="roles_director" value="director"><span id="label_director">Director</span></label>
              <br>
              <label><input <?php 
$user_roles = explode(",",$user->roles);
if (count($errors)>0){
     if (in_array('docente',old('roles'))){ ?> checked <?php }//endif;
}else{
      if (in_array('docente',$user_roles)){ ?> checked <?php }//endif;
}//endif;
                            ?>  type="checkbox" name="roles[]" class="chk_roles" id="roles_docente" value="docente"><span id="label_docente">Docente</span></label>
              <br>
              <label><input <?php 
$user_roles = explode(",",$user->roles);
if (count($errors)>0){
     if (in_array('estudiante',old('roles'))){ ?> checked <?php }//endif;
}else{
      if (in_array('estudiante',$user_roles)){ ?> checked <?php }//endif;
}//endif;
                            ?> type="checkbox" name="roles[]" class="chk_roles" id="roles_estudiante" value="estudiante"><span id="label_estudiante">Estudiante</span></label>

                @if ($errors->has('rol'))
                    <span class="help-block">
                        <strong>{{ $errors->first('rol') }}</strong>
                    </span>
                @endif
                <input type="hidden" id="rol"value="{{$user->rol}}">
              <select name="rol" id="select1" class="form-control" required>
                </select> 
            </div>
        </div>
        <div class="form-group{{ $errors->has('tel') ? ' has-error' : '' }}">
            <label for="tel" class="col-md-4 control-label">Teléfono</label>

            <div class="col-md-6">
                <input id="tel" type="text" class="form-control" name="tel" value="{{ $errors->has('tel') ?  old('tel') : $user->tel }}" required autofocus>

                @if ($errors->has('tel'))
                    <span class="help-block">
                        <strong>{{ $errors->first('tel') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Correo Electrónico</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ $errors->has('email') ?  old('email') : $user->email }}" required>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>
<div class="form-group">
<label><input type="checkbox" onclick="validar_area_password(this);"> Cambiar Contraseña</label>
<span id="area_password" style="display:none">
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                            <div class="col-md-6">
                                
<div class="input-group">
<input type="password" name="password" class="form-control" id="password"  placeholder="Contraseña"  data-toggle="tooltip" data-trigger="manual" data-title="Las Mayúsculas están activadas">
<span id="eye_password" title="Pulse aquí para descubir su clave" onclick="document.getElementById('password').type = document.getElementById('password').type == 'text' ? 'password' : 'text'; document.getElementById('eye_password_icon').className = document.getElementById('eye_password_icon').className == 'glyphicon glyphicon-eye-open' ? 'glyphicon glyphicon-eye-close' : 'glyphicon glyphicon-eye-open' " class="input-group-addon"><span  id="eye_password_icon" class="glyphicon glyphicon-eye-open"></span></span>
</div>


                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Contraseña') }}</label>

                            <div class="col-md-6">
<div class="input-group">
<input type="password" name="password_confirmation" class="form-control" id="password_confirmation"  placeholder="Contraseña"  data-toggle="tooltip" data-trigger="manual" data-title="Las Mayúsculas están activadas">
<span id="eye_password" title="Pulse aquí para descubir su clave" onclick="document.getElementById('password_confirmation').type = document.getElementById('password_confirmation').type == 'text' ? 'password' : 'text'; document.getElementById('eye_password_icon').className = document.getElementById('eye_password_icon').className == 'glyphicon glyphicon-eye-open' ? 'glyphicon glyphicon-eye-close' : 'glyphicon glyphicon-eye-open' " class="input-group-addon"><span  id="eye_password_icon" class="glyphicon glyphicon-eye-open"></span></span>
</div>

                                @if ($errors->has('password-confirm'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password-confirm') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
</span>
</div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
<a href="{{ route('admin.usuarios.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
<input type="submit" name="submit" value="Actualizar" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
