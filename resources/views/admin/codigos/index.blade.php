@extends('layouts.app')
@section('vars')
{!! $page_title = 'Departamento'; 
!!}
@endsection
@section('barra_buscar')
    @include('layouts.partials.buscar')
@endsection
@section('scripts')
<script>
$(document).ready(function(){
$("#buscar").keyup(function(){
  buscar_departamento();
});
$("#buscar").change(function(){
  buscar_departamento();
});
$("#search-btn").click(function(){
  buscar_departamento();
});
$("#num_resultados").keyup(function(){
  buscar_departamento();
});
$("#num_resultados").change(function(){
  buscar_departamento();
});
$("#menu_departamento").addClass('active');
buscar_departamento();
});
</script>
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> {{ $page_title }}</li>
</ol>
@endsection
@section('content')
<center><h1>{{ $page_title }}</h1></center>
<div class='row'>
  <div class='col-md-12'>
  <span id="txt_resultados">
</span>
 <div id="area_pagination"></div>
    
    
  </div><!-- /.col -->
  <div class='col-md-6'>

  </div><!-- /.col -->

</div><!-- /.row -->
@endsection