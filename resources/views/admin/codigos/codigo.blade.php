@extends('layouts.app')
@section('vars')
{!! $page_title = 'Código para evaluación'; 
!!}
@endsection
@section('barra_buscar')
    @include('layouts.partials.buscar')
@endsection
@section('scripts')
<script>
$(document).ready(function(){
$("#buscar").keyup(function(){
  buscar_departamento();
});
$("#buscar").change(function(){
  buscar_departamento();
});
$("#search-btn").click(function(){
  buscar_departamento();
});
$("#num_resultados").keyup(function(){
  buscar_departamento();
});
$("#num_resultados").change(function(){
  buscar_departamento();
});
$("#menu_departamento").addClass('active');
buscar_departamento();
});
</script>
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> {{ $page_title }}</li>
</ol>
@endsection
@section('content')
<center><h1>{{ $page_title }}</h1></center>
<div class='row'>
  <div class='col-md-12'>
<form method="POST" action="{{ route('codigo') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div id="div_token" class="form-group{{ $errors->has('codigo_token') ? ' has-error' : '' }}">
                <label for="codigo_token" class="col-md-4 control-label">Código</label>

                <div class="col-md-6">
                    <input id="codigo_token" autofocus type="text" class="form-control" name="codigo_token">
    
                    @if ($errors->has('codigo_token'))
                        <span class="help-block">
                            <strong>{{ $errors->first('codigo_token') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                Enviar
                </button>
                </div>
                </div>
            </div>
 </form>
    
  </div><!-- /.col -->
  <div class='col-md-6'>

  </div><!-- /.col -->

</div><!-- /.row -->
@endsection