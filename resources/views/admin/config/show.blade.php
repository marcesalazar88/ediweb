@extends('layouts.app')
@section('vars')
{!! $page_title = 'Configuración'; 
!!}
@endsection
@section('scripts')
@section('barra_buscar_cuerpo')
<nav class="navbar navbar-default">
<ol class="breadcrumb navbar-left">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> {{ $page_title }}</li>
</ol>
<div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
<ul class="nav navbar-nav navbar-right">
   
</ul>
</nav>
@endsection
<script>

</script>
@endsection
@section('ruta_de_migas')
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><center><h1>{{ $page_title }}</h1></center></div>

                <div class="card-body">
                    <form method="POST" id="admin_config_update" action="{{ route('admin.config.update',[$config->id]) }}">
                       <meta name="csrf-token" content="{{ csrf_token() }}">
                       <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       <input type="hidden" name="config_id" value="{{ $config->id }}">

                        <!--div class="form-group row">
                            
                            <div class="col-md-6">
                               <input name="notificar_email" type="checkbox" value="{{$config->notificar_email}}" class="form-control-checkbox {{ $errors->has('notificar_email') ? ' is-invalid' : '' }}" placeholder="Enviar notificaciones a email."><label for="notificar_email" class="col-md-4 col-form-label text-md-right">
                              {{ __('Enviar notificaciones a email.') }}
                          </label>

                                @if ($errors->has('notificar_email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('notificar_email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div-->
@php
$hora = (string)date("G:i", strtotime($config->hora_notificacion));
//var_dump($hora);
@endphp
                        <div class="form-group row">
                            <label for="hora_notificacion" class="col-md-4 col-form-label text-md-right">
                              {{ __('Hora de Notificación') }} 
                          </label>
                            <div class="col-md-6">
                               <input name="hora_notificacion" type="time" min="00:00" max="23:59" value="{{$config->hora_notificacion}}" class="form-control{{ $errors->has('hora_notificacion') ? ' is-invalid' : '' }}" placeholder="Hora de Notificación" required>

                                @if ($errors->has('hora_notificacion'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('hora_notificacion') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                     
    
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                              <a href="{{ route('admin.asignatura.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
<input type="submit" name="submit" value="Modificar" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection