@extends('layouts.app')
@section('vars')
{!!
$page_title = 'planes'; 
$route = 'planes'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ route('admin.programa.index') }}"> Programa</a></li>
    <li><a href="{{ route('admin.programa.show',['id' => $planes->prog->id]) }}">Detalles {{ $planes->prog->nom_prog }}</a></li>
    <li class="active"> Modificar plan</li>
</ol>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        password_en_formulario("admin_planes_create");
    required_en_formulario_for("admin_planes_create","red","*")
    });
    </script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    <form method="POST" id="admin_planes_update" action="{{ route('admin.planes.update',[$planes->id]) }}"  enctype="multipart/form-data">
                         <meta name="csrf-token" content="{{ csrf_token() }}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                         <input type="hidden" name="id" value="{{ $planes->id }}">

       <div class="form-group row">
            <label for="programa" class="col-md-4 col-form-label text-md-right">Programa</label>
             <div class="col-md-6">
            <label id="programa" lass="form-control">{{ $planes->prog->nom_prog }} ({{ $planes->prog->cod_prog }})</label>
            <input name="cod_prog" type="hidden" value="{{ $planes->prog->cod_prog }}">
             </div>
       </div>
       <div class="form-group row">
            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

            <div class="col-md-6">
<input id="nombre" type="text" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" required autofocus value="{{ count($errors)>0 ?  old('nombre') : $planes->nombre }}">

                @if ($errors->has('nombre'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('nombre') }}</strong>
                    </span>
                @endif
            </div>
        </div>
                      
                       <div class="form-group row">
                         <div class="col-md-4 ">
                            <label for="adjunto" class="col-form-label text-md-right">{{ __('Acuerdo') }}</label>
                           <br>
 <input type="hidden" name="modificar_adjunto" value="NO">
                            <input type="checkbox" class="" id="modificar_adjunto"  name="modificar_adjunto" value="SI" onclick="if(this.checked){
document.getElementById('adjunto_anterior').style.display = 'none';    
document.getElementById('adjunto_nuevo').style.display = 'initial';
document.getElementById('adjunto').setAttribute('required','required');
}else{
document.getElementById('adjunto_anterior').style.display = 'initial';
document.getElementById('adjunto_nuevo').style.display = 'none';                                         
document.getElementById('adjunto').removeAttribute('required');
}">
                            <label for="modificar_adjunto" class="">{{ __('Cambiar adjunto') }}</label>
                           </div>
                            <div class="col-md-6">
                              <span id="adjunto_anterior">
                              <a href="<?php echo env('APP_URL').$planes->adjunto ?>" target="_blank" style="color: red;font-size: large;"><span class="glyphicon glyphicon-paperclip"></span> Acuerdo</a>
                              </span>
                              <span id="adjunto_nuevo" style="display:none">
                                <input id="adjunto" type="file" accept="application/pdf"  class="form-control{{ $errors->has('adjunto') ? ' is-invalid' : '' }}" name="adjunto" value="{{ old('adjunto') }}" autofocus>
                              </span>
                                @if ($errors->has('adjunto'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('adjunto') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
<div class="form-group row">
    <label for="fecha" class="col-md-4 col-form-label text-md-right">{{ __('Fecha') }}</label>
  <div class="col-md-6">
      <input id="fecha" type="date" class="form-control{{ $errors->has('fecha') ? ' is-invalid' : date('Y-m-d') }}" name="fecha" value="{{ count($errors)>0 ?  old('fecha') : $planes->fecha }}" required autofocus>

      @if ($errors->has('fecha'))
          <span class="invalid-feedback">
              <strong>{{ $errors->first('fecha') }}</strong>
          </span>
      @endif
  </div>
</div>
<div class="form-group row">
    <label for="observaciones" class="col-md-4 col-form-label text-md-right">{{ __('Observaciones') }}</label>
    <div class="col-md-6">
        <input id="observaciones" type="text" class="form-control{{ $errors->has('observaciones') ? ' is-invalid' : '' }}" name="observaciones" value="{{ count($errors)>0 ?  old('observaciones') : $planes->observaciones }}" required>
        @if ($errors->has('observaciones'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('observaciones') }}</strong>
            </span>
        @endif
    </div>
</div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <div class="col-md-6 offset-md-4">
                                  
<a href="{{ route('admin.planes.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
                                    <input type="submit" name="submit" value="Actualizar" class="btn btn-primary">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
