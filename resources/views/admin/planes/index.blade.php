@extends('layouts.app')
@section('vars')
{!! $page_title = 'Planes'; 
!!}
@endsection
@section('barra_buscar_cuerpo')
<nav class="navbar navbar-default">

<div >
 
                    <!-- Left Side Of Navbar -->
<ul class="nav navbar-nav navbar-right" style="margin-right: 0px;">
  <li><a style="margin: 0; padding: 8px;">
    <div class="input-group sidebar-form">
    <label for="buscar">Buscar por programa&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_programa" name="cod_prog" class="js-example-basic-single form-control input_buscar">
      @foreach ($prog as $id => $prog_i)
      @if(Funciones::permiso_depto('cod_prog',$prog_i->cod_prog))
      <option @if($prog_i->cod_prog=="011") selected @endif value="{{$prog_i->cod_prog}}">{{$prog_i->nom_prog}}</option>
       @endif
      @endforeach
    </select>
    </div>
    </a></li>
    <li>
    <a> <input type="hidden" id="buscar" class="form-control" style="">
    <input type="hidden" id="buscar_arreglo" value="SI">
    <input type="hidden" name="flex" class="input_buscar2" value="SI">
    </a></li>
    <li>
      <div class="input-group sidebar-form">
          <label for="num_resultados">Resultados&nbsp;</label><br>
          <span class="input-group-btn" style="display:inline">
          <input title="Número de resultados" class="form-control" style="height: 27px;
    margin-top: 9px;background-color: rgb(255, 255, 255);color: black;width: 70px;border-bottom: 1px none transparent;border-left: 0px solid black;margin-right: 1px;border-right: 1px solid #ccd0d2;border-bottom: 1px solid #ccd0d2;background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 4px;" min="1" id="num_resultados" name="num_resultados"  type="number" value="<?php 
    if (isset($_POST['num_resultados'])){
      echo $_POST['num_resultados'];
    }else{
      echo 3;
    }
    ?>" solonumeros>
          </span>
      </div>
    </li>
    <li style="
    margin: 0px;
    padding: 5px;
">
    </li>
</ul>
</div>
</nav>
@endsection
@section('scripts')
<script>
function preparar_buscar_planes(){
  var contenido = '{';
  var cont = 0;
  var total = $(".input_buscar").length;
  $(".input_buscar").each(function( index ) {
        $(this).each(function( index2 ) {
          contenido += '"'+this.name+'":"'+$(this).val()+'"';
          cont++;
          if(cont != total) contenido += ',';
        });
    });
    contenido += '}'
  $("#buscar").val(btoa(contenido));
  
}
$(document).ready(function(){
$(".input_buscar").keyup(function(){
  preparar_buscar_planes();
  buscar_planes();
});
$(".input_buscar").change(function(){
  preparar_buscar_planes();
  buscar_planes();
});
  preparar_buscar_planes();
  buscar_planes();
$("#buscar").keyup(function(){
  buscar_planes();
});
$("#buscar").change(function(){
  buscar_planes();
});
$("#search-btn").click(function(){
  buscar_planes();
});
$("#num_resultados").keyup(function(){
  buscar_planes();
});
$("#num_resultados").change(function(){
  buscar_planes();
});
$("#menu_configuraciones").addClass('active');
buscar_planes();
});
</script>
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> {{ $page_title }}</li>
</ol>
@endsection
@section('content')
<center><h1>{{ $page_title }}</h1></center>
@if($botones)
<input type="hidden" id="a5d546a503db8d" value="{{ $botones ? __('true') : __('false') }}">
@endif
<div class='row'>
  <div class='col-md-12'>
  <span id="txt_resultados">
</span>
 <div id="area_pagination"></div>
    
    
  </div><!-- /.col -->
  <div class='col-md-6'>

  </div><!-- /.col -->

</div><!-- /.row -->
@endsection