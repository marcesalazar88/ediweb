@extends('layouts.app')
@section('vars')
{!!
$page_title = 'planes'; 
$route = 'planes'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ secure_asset('admin/planes') }}"> {{ $page_title }}</a></li>
    <li class="active"> Registrar</li>
</ol>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        password_en_formulario("admin_planes_create");
    required_en_formulario_for("admin_planes_create","red","*")
    });
    </script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    <form method="POST" id="admin_planes_create" action="{{ route('admin.planes.store') }}"  enctype="multipart/form-data">
                         <meta name="csrf-token" content="{{ csrf_token() }}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">

                       <div class="form-group row">
                            <label for="programa" class="col-md-4 col-form-label text-md-right">Programa</label>
                             <div class="col-md-6">
                            <label id="programa" lass="form-control">{{ $prog->nom_prog }} ({{ $prog->cod_prog }})</label>
                            <input name="cod_prog" type="hidden" value="{{ $prog->cod_prog }}">
                             </div>
                       </div>
                       <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control{{ $errors->has('nombre') ? ' is-invalid' : '' }}" name="nombre" value="{{ old('nombre') }}" required autofocus>

                                @if ($errors->has('nombre'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                       <div class="form-group row">
                            <label for="adjunto" class="col-md-4 col-form-label text-md-right">{{ __('Acuerdo') }}</label>

                            <div class="col-md-6">
                                <input id="adjunto" type="file" accept="application/pdf"  class="form-control{{ $errors->has('adjunto') ? ' is-invalid' : '' }}" name="adjunto" value="{{ old('adjunto') }}" required autofocus>

                                @if ($errors->has('adjunto'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('adjunto') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       <div class="form-group row">
                            <label for="fecha" class="col-md-4 col-form-label text-md-right">{{ __('Fecha') }}</label>

                            <div class="col-md-6">
                                <input id="fecha" type="date" class="form-control{{ $errors->has('fecha') ? ' is-invalid' : date('Y-m-d') }}" name="fecha" value="{{ old('fecha') }}" required autofocus>

                                @if ($errors->has('fecha'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('fecha') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
