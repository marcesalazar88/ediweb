@extends('layouts.app')
@section('vars')
{!! $page_title = 'Programas'; 
!!}
@endsection
@section('barra_buscar_cuerpo')
<nav class="navbar navbar-default">
<ol class="breadcrumb navbar-left">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> {{ $page_title }}</li>
</ol>
<div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
<ul class="nav navbar-nav navbar-right">
    @include('layouts.partials.buscar')
</ul>
</nav>
@endsection
@section('scripts')
<script>
$(document).ready(function(){
$("#buscar").keyup(function(){
  buscar_programa();
});
$("#buscar").change(function(){
  buscar_programa();
});
$("#search-btn").click(function(){
  buscar_programa();
});
$("#num_resultados").keyup(function(){
  buscar_programa();
});
$("#num_resultados").change(function(){
  buscar_programa();
});
$("#menu_programa").addClass('active');
buscar_programa();
});
</script>
@endsection
@section('ruta_de_migas')
@endsection
@section('content')
<center><h1>{{ $page_title }}</h1></center>
<div class='row'>
  <div class='col-md-12'>
  <span id="txt_resultados">
</span>
 <div id="area_pagination"></div>
    
    
  </div><!-- /.col -->
  <div class='col-md-6'>

  </div><!-- /.col -->

</div><!-- /.row -->
@endsection