@extends('layouts.app')
@section('vars')
{!!
$page_title = 'Departamentos'; 
$route = 'departamento'; 
!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li><a href="{{ route('admin.departamento.index') }}"> {{ $page_title }}</a></li>
    <li class="active"><i class="fa fa-plus"></i> Modificar</li>
</ol>
@endsection
@section('scripts')
<script>
    $(document).ready(function() {
        password_en_formulario("admin_departamento_create");
        required_en_formulario_for("admin_departamento_create","red","*")
    });
    
    function validar_area_password(obj){
        document.getElementById('area_password').style.display = obj.checked ? 'block' : 'none';
        document.getElementById('password').value='';
        document.getElementById('password-confirm').value='';
        $('#password').prop('required',obj.checked);
        $('#password-confirm').prop('required',obj.checked);
    }
</script>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"></div>

                <div class="card-body">
                    <form method="POST" id="admin_departamento_create" action="{{ route('admin.departamento.update',[
                    'id' => $depto->id,
                    ]) }}">
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group row">
                            <label for="nom_depto" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="id" type="hidden" class="form-control{{ $errors->has('id') ? ' is-invalid' : '' }}" name="id" value="{{
                                ($errors->has('id')) ?  old('id') : $depto->id }}" required>
                                <input id="nom_depto" type="text" class="form-control{{ $errors->has('nom_depto') ? ' is-invalid' : '' }}" name="nom_depto" value="{{
                                ($errors->has('nom_depto')) ?  old('nom_depto') : $depto->nom_depto }}" required autofocus>
                                @if ($errors->has('nom_depto'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('nom_depto') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group row">

                            <label for="fac_id" class="col-md-4 col-form-label text-md-right">{{ __('Facultad') }}</label>
                            <div class="col-md-6">
                         
                                {!!Form::select('fac_id', $fac, 
                                ($errors->has('fac_id')) ?  old('fac_id') : $depto->fac_id
                                , [
                                'id' => 'fac_id',
                                'class' => 'js-example-basic-single form-control',
                                'placeholder' => 'Seleccione una Facultad',
                                'required',
                                ])!!}
                                
                                
                                @if ($errors->has('fac_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('fac_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

<div class="form-group row">
                            <label for="director" class="col-md-4 col-form-label text-md-right">{{ __('Director') }}</label>

                            <div class="col-md-6">
                                <select id="director" class="js-example-basic-single form-control{{ $errors->has('director') ? ' is-invalid' : '' }}" name="director" required autofocus>
                                    <option value="">Seleccione un usuario</option>
                                    @foreach($usuarios as $director_i)
                                  <?php $roles = explode(",",$director_i->roles);?>
                                    @if(in_array("director",$roles))
                                    <option value="{{ $director_i->ident_usu }}"
                                     @if (count($errors)>0)
                                            @if (old('director')==$director_i->ident_usu) {{ __(' selected ') }} @endif;
                                    @else
                                        @if ($depto->director==$director_i->ident_usu) {{ __(' selected ') }} @endif;
                                    @endif;
                                    >{{ $director_i->name." (".$director_i->ident_usu.")" }}</option>
                                    @endif
                                    @endforeach
                                </select>
                                @if ($errors->has('director'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('director') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                              <a href="{{ route('admin.departamento.index') }}" class="btn btn-secondary hidden-print">Regresar</a>
                                <input type="submit" name="submit" value="Actualizar" class="btn btn-primary">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
