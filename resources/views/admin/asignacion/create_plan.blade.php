@extends('layouts.app') @section('barra_buscar')
<li><a onclick="window.print()" title="Imprimir">
<span class="glyphicon glyphicon-print"></span></a></li>
@endsection @section('ruta_de_migas')
<ol class="breadcrumb navbar-left">
  <li><a href="{{ route('home') }}"> Inicio</a></li>
  <li><a href="{{ route('admin.asignacion.index') }}">Asignación de asignaturas</a></li>
  <li><a href="{{ route('admin.asignacion.create') }}">Registrar</a></li>
  <li class="active"><a href="#">Plan</a></li>
  <li class="active"><a href="#">{{ $planes->nombre }}</a></li>
</ol>
@endsection @section('scripts') @include('reportes.css') @endsection @section('content')
<?php use Illuminate\Support\Arr; ?>
<br>
<hr>
<span id="reporte">
<center>

<h1>
  <center><strong>Plan de estudios - Programa de {{ $planes->prog->nom_prog }}</strong></center><a class="btn btn-success hidden-print"  href="{{ route('admin.planes.show',[$planes->id]) }}" target="_blank"><span title="Editar este plan" class="glyphicon glyphicon-pencil"></span></a>
</h1>
<table border="1" class="table cssresponsive">
  <?php 
  $asignaturas_anteriores=[];
  $masdesegundo = false;
  $semestre = 0;
  $iniciar_con = $periodo_activo == $planes->prog->periodo_ingreso ? 1 : 2;
  ?> @foreach ($planes->planes_asigts as $planes_asigt_i)
  <?php 
  $cont=0;
  $total = count($planes_asigt_i->prerequisitos)>0 ? count($planes_asigt_i->prerequisitos) : 1;
  $num = 1;
  ?> @if($semestre != $planes_asigt_i->semestre)
  <thead></thead>
  <tr>
    <th class="hidden-xs" colspan="6">
      <center>Semestre {{ $planes_asigt_i->semestre }}</center>
    </th>
    <td class="hidden-sm hidden-md hidden-lg" data-label="Semestre">
      {{ $planes_asigt_i->semestre }}
    </td>
  </tr>
  <thead>
  <tr>
    <th>
      <center>Código</center>
    </th>
    <th>
      <center>Asignatura</center>
    </th>
    <th>
      <center>I.H.S.</center>
    </th>
    <th>
      <center>Asignaciones</center>
    </th>
  </tr>
  </thead>
  @endif
  <?php $semestre = $planes_asigt_i->semestre;
    $asignaturas_anteriores[$planes_asigt_i->asigt->codigo_asigt]=[
    'semestre' =>  $planes_asigt_i->semestre,
    'nom_asigt' => $planes_asigt_i->asigt->nom_asigt
  ];
  
  $asignaciones = Funciones::asignaciones($planes->prog->cod_prog, $planes_asigt_i->asigt->codigo_asigt, $periodo_activo->periodo);
  //$num = count($asignaciones);
  ?>
  <tr>
    <td data-label="Código" rowspan="{{$num}}">{{ $planes_asigt_i->asigt->codigo_asigt }}</td>
    <td data-label="Asignatura" rowspan="{{$num}}">{{ $planes_asigt_i->asigt->nom_asigt }}</td>
    <td data-label="I.H.S." rowspan="{{$num}}">{{ $planes_asigt_i->asigt->ihs_asigt }}</td>
    <td data-label="Asignaciones" rowspan="{{$num}}">
    @foreach ($asignaciones as $asignacionesi)
      <p>  
      Docente: {{ $asignacionesi->docente_fk->name }}
      </p>
      <p>
      Grupo: {{ $asignacionesi->grupo }}
      </p><a class="btn btn-danger hidden-print" onclick="return confirm('¿Esta ud seguro que quiere eliminar la asignación de {{ $planes_asigt_i->asigt->nom_asigt }}({{ $planes_asigt_i->asigt->codigo_asigt }}) del docente: {{ $asignacionesi->docente_fk->name }} en el grupo: {{ $asignacionesi->grupo }}?')" href="{{ route('admin.asignacion.delete',[$asignacionesi->id]) }}"><span title="la asignación del docente: {{ $asignacionesi->docente_fk->name }} en el grupo: {{ $asignacionesi->grupo }}" class="glyphicon glyphicon-trash"></span></a>
      <hr>
    @endforeach
      <p>
          <a class="badge hidden-print" data-toggle="modal" data-target="#crear_nueva_asignacion" onclick="document.getElementById('cod_asigt').value = '{{ $planes_asigt_i->asigt->codigo_asigt }}';document.getElementById('nom_asigt').innerHTML = '{{ $planes_asigt_i->asigt->nom_asigt }}';setTimeout(function(){ document.querySelector('#ident_docnt .cke_wysiwyg_frame').contentDocument.body.focus(); }, 1000);" style="    color: #fff;
    background-color: #3097D1 !important;
    border-color: #2a88bd;
    height: 42px;
    width: 42px;
    /*border-radius: 50%;*/
    text-align: center;
    vertical-align: middle;
    vertical-align: -webkit-baseline-middle;
    font-size: 37px;" href="facultad/create" title="Nueva asignación">
+
</a>
      </p>
    </td>
  </tr>
  @endforeach
</table>
<!-- Modal -->
<div id="crear_nueva_asignacion" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nueva Asignación</h4>
      </div>
      <div class="modal-body">
<form method="POST" id="admin_asignacion_create" action="{{ route('admin.asignacion.store') }}">
       <meta name="csrf-token" content="{{ csrf_token() }}">
       <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="row">
        <span style="display:none">
            <input type="text" name="cod_prog" value="{{ $planes->prog->cod_prog }}" required>
            <input type="text" name="redirect" value="back" required>
            <input type="text" id="cod_asigt" name="cod_asigt" value="" required>
            <input type="text" name="per_acad" value="{{$periodo_activo->periodo}}" required>
        </span>
<div class="form-group row">
  <label for="periodo" class="col-md-4 col-form-label text-md-right">{{ __('Periodo') }}</label>
  <div class="col-md-6">
  <span readonly="readonly" class="form-control">{{$periodo_activo->periodo}}</span>
  </div>
</div>
        
<div class="form-group row">
  <label for="prog" class="col-md-4 col-form-label text-md-right">{{ __('Programa') }}</label>
  <div class="col-md-6">
  <span readonly="readonly" class="form-control">{{ $planes->prog->nom_prog }}</span>
  </div>
</div>
        
<div class="form-group row">
  <label for="asigt" class="col-md-4 col-form-label text-md-right">{{ __('Asignatura') }}</label>
  <div class="col-md-6">
  <span readonly="readonly" class="form-control" id="nom_asigt"></span>
  </div>
</div>
      
<div class="form-group row">
 <label for="ident_docnt" class="col-md-4 col-form-label text-md-right">{{ __('Docente') }}</label>

    <div class="col-md-6">
    <select style="width:100%"id="ident_docnt" class="js-example-basic-single form-control{{ $errors->has('ident_docnt') ? ' is-invalid' : '' }}" name="ident_docnt" required autofocus>
    <option value="">Seleccione un docente</option>
    @foreach($usuarios as $ident_docnt_i)
    @if($ident_docnt_i->rol == "docente")
    <option value="{{ $ident_docnt_i->ident_usu }}"
    @if (old('ident_docnt')==$ident_docnt_i->ident_usu) {{ __(' selected ') }} 
    @endif;
    >{{ $ident_docnt_i->name." (".$ident_docnt_i->ident_usu.")" }}</option>
    @endif
    @endforeach
    </select>
    @if ($errors->has('ident_docnt'))
    <span class="invalid-feedback">
    <strong>{{ $errors->first('ident_docnt') }}</strong>
    </span>
    @endif
    </div>
    </div>
		  <div class="form-group row">
                            <label for="flex" class="col-md-4 col-form-label text-md-right">{{ __('Flexibilidad') }}</label>

                            <div class="col-md-6">
                                <select id="flex" class="form-control{{ $errors->has('flex') ? ' is-invalid' : '' }}" name="flex" required autofocus>
                                    <option
                                    @if (old('flex')=="SI") {{ __(' selected ') }} 
                                    @endif;
                                    >SI</option>
                                    <option 
                                    @if (old('flex')!="SI") {{ __(' selected ') }} 
                                    @endif;
                                    >NO</option>
                                </select>

                                @if ($errors->has('flex'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('flex') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                       <div class="form-group row">
                            <label for="grupo" class="col-md-4 col-form-label text-md-right">{{ __('Grupo') }}</label>

                            <div class="col-md-6">
                                <?php
                                $sug_grupo = old('grupo');
                                ?>
                                <input placeholder="Ejemplo:  Grupo 1" id="grupo" type="number" class="form-control{{ $errors->has('grupo') ? ' is-invalid' : '' }}" name="grupo" value="{{ $errors->has('grupo') ? old('grupo') : $sug_grupo }}" required autofocus>
                                @if ($errors->has('grupo'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('grupo') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                       <div class="form-group row">
                            <label for="observaciones" class="col-md-4 col-form-label text-md-right">{{ __('Observaciones') }}</label>

                            <div class="col-md-6">
 <textarea placeholder="" id="observaciones" class="form-control{{ $errors->has('observaciones') ? ' is-invalid' : '' }}" name="observaciones"  >{{ $errors->has('observaciones') ? old('observaciones') : old('observaciones') }}</textarea>

                                @if ($errors->has('flex'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('flex') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
	<div class="form-group row">
  <input type="hidden" name="notificar" value="NO"> 
  <input id="notificarid" type="checkbox" name="notificar" value="SI">
  <label for="notificarid">Enviar notificación al guardar</label>
</div>
     <div class="col-md-12">
			    <center><br>
			    <input class="btn btn-success" type="submit" value="Guardar">
			    </center>
			</div>
		</div>
			</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>

  </div>
</div><!-- Modal -->
</center>
</span>
<a href="{{ route('admin.asignacion.create') }}" class="btn btn-secondary hidden-print">Regresar</a>
@endsection