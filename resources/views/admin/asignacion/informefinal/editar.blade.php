@extends('layouts.app')
@section('vars')
{!!
$facultad = "Ciencias Exactas y Naturales";
$programa = "Licenciatura en Informática";
$facultad = mb_strtoupper($facultad,'utf-8');
$programa = mb_strtoupper($programa,'utf-8');
$page_title = 'Informe Final'; 
!!}
@endsection
@section('barra_buscar')
<!--li><a onclick="window.print()" title="Imprimir">
<span class="glyphicon glyphicon-print">
</a></li-->
@endsection
@section('barra_buscar_cuerpo')
<nav class="navbar navbar-default">
<ol class="breadcrumb navbar-left">
    <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active"><i class="fa fa-user"></i> {{ $page_title }}</li>
</ol>
</nav>
@endsection
@section('scripts')
@include('reportes.css')

<script>
    function sugerir_fechas(select){
        var x = select.selectedIndex;
        var items = select.options;
        if(items[x].value!=""){
            $("#fecha_aprobacion").val($(items[x]).attr('data-fecha_aprobacion'));
            $("#fecha_revision_doc").val($(items[x]).attr('data-fecha_revision_doc'));
            $("#fecha_aprobacion").prop('readonly',true);
            $("#fecha_revision_doc").prop('readonly',true);
            }else{
            $("#fecha_aprobacion").prop('readonly',false);
            $("#fecha_revision_doc").prop('readonly',false);
        }
    }
</script>
<script>
    $().ready(function() 
	{
		$('.pasar').click(function() { return !$('#origen option:selected').remove().appendTo('#destino'); });  
		$('.quitar').click(function() { return !$('#destino option:selected').remove().appendTo('#origen'); });
		$('.pasartodos').click(function() { $('#origen option').each(function() { $(this).remove().appendTo('#destino'); }); });
		$('.quitartodos').click(function() { $('#destino option').each(function() { $(this).remove().appendTo('#origen'); }); });
		$('.seleccionar_tems').click(function() { seleccionar_tems(); });
	});
</script>
@endsection
@section('content')
<span id="reporte">
    
     <form method="POST" id="admin_infofin_create" action="{{ route('admin.informefinal.update',[
                    'id' => $infofin->id_asigt,
                    ]) }}">
                         <meta name="csrf-token" content="{{ csrf_token() }}">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">

<center>
<table border="1">
    <tr>
        <td class="alineado_izquierda">
                 <div class="row">
                 <div class="form-group">
                            <label for="control_fecha_id" class="col-md-3 col-form-label text-md-right">{{ __('Control de fechas') }}</label>
        @if (Auth::user()->rol=='director' or Auth::user()->rol=='admin')
                            <div class="col-md-3">
                                <select title="Sugerencias para fechas" onchange="sugerir_fechas(this)" name="control_fecha_id" id="control_fecha_id" class="js-example-basic-single form-control{{ $errors->has('control_fecha_id') ? ' is-invalid' : '' }}" >
                                    <option  value="">Fechas Personalizadas</option>
                                    @foreach($controlfechas as $controlfechas_i)
                                    <option data-fecha_aprobacion = "{{ $controlfechas_i->fecha_aprobacion }}" data-fecha_revision_doc = "{{ $controlfechas_i->fecha_revision_doc }}" title='' value="{{ $controlfechas_i->id }}" 
                                        @if ($errors->has('control_fecha_id'))
                                            @if (old('control_fecha_id')==$controlfechas_i->id) {{ __(' selected ') }} @endif;
                                        @else
                                            @if ($infofin->control_fecha_id==$controlfechas_i->id) {{ __(' selected ') }} @endif;
                                        @endif;
                                    >{{ $controlfechas_i->nombre_control." (".$controlfechas_i->per_acad.")" }}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('control_fecha_id'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('control_fecha_id') }}</strong>
                                    </span>
                                @endif
        @endif
                            </div>
                        </div>
                    </div>
        </td>
    </tr>
    <tr>
        <td class="alineado_izquierda">
            <label>Fecha de aprobación</label>
            @if (Auth::user()->rol=='director' or Auth::user()->rol=='admin')
              <input id="fecha_aprobacion" name="fecha_aprobacion" type="date" value="{{$infofin->fecha_aprobacion}}"
              @if ($infofin->control_fecha_id!="")
              readonly="readonly"
              @endif
              >
            @else
                {{ Fecha::formato_fecha($infofin->fecha_aprobacion) }}
            @endif
        </td>
    </tr>
    <tr>
        <td class="alineado_izquierda">
                <label>Fecha límite para Revisión docente</label>
            @if (Auth::user()->rol=='director' or Auth::user()->rol=='admin')
              <input id="fecha_revision_doc" name="fecha_revision_doc" type="date" value="{{$infofin->fecha_revision_doc}}" <?php 
              if ($infofin->control_fecha_id!=""):
              echo 'readonly="readonly"';
              endif;
              ?> >
            @else
                {{ Fecha::formato_fecha($infofin->fecha_revision_doc) }}
            @endif
        </td>
    </tr>
</table>

<table border="1">
    <tr>
        <th rowspan="4">
            <center><img src="{{ secure_asset('img/logo-udenar.png')}}"></center>
            <span id="titulo_logo"><center>Universidad <span class="m10">de</span></center><center><strong>Nariño</strong></center></span>
        </th>
        <th rowspan="4">
            <div class="titulo">FORMACIÓN ACADÉMICA</div>
            <div class="titulo">FACULTAD {{ mb_strtoupper( $asigc->prog->depto->fac->nom_fac,'utf-8') }}</div>
            <div class="titulo">PROGRAMA DE {{ mb_strtoupper( $asigc->prog->nom_prog,'utf-8') }}</div>
            <div class="titulo negrita"><strong>INFORME FINAL SOBRE EL DESARROLLO DE LA TEMÁTICA POR ASIGNATURA</strong></div>
        </th>
        <th>Código: FOA-FR-13</th>
    </tr>
    <tr>
        <th>Página 1 de 2</th>
    </tr>
    <tr>
        <th>Versión: 1</th>
    </tr>
    <tr>
        <th>Vigente a partir de: {{Fecha::formato_fecha('2011-01-20')}}</th>
    </tr>

</table></center>
<p><strong>1. INFORMACIÓN DEL CURSO O ASIGNATURA</strong></p>
<center>
<table border="1">
    <tr>
        <td style="width:25%"><strong>Nombre del Docente:</strong></td>
        <td style="width:25%">{{$asigc->docente_fk->name}}</td>
        <td style="width:25%"><strong>Correo Electrónico:</strong></td>
        <td style="width:25%">{{$asigc->docente_fk->email}}</td>
</tr>
<tr>
        <td><strong>Asignatura:</strong></td>
        <td>{{ $asigc->asigt->nom_asigt }}</td>
        <td><strong>Código de la Asignatura:</strong></td>
        <td>{{ $asigc->asigt->codigo_asigt }}</td>
</tr>
<tr>
        <td><strong>Intensidad Horaria:</strong></td>
        <td>{{ $asigc->asigt->ihs_asigt }}</td>
        <td><strong>Semestre/Nivel:</strong></td>
        <td>{{ $asigc->asigt->sem_ofrece_asigt }}</td>
</tr>
<tr>
        <td><strong>Número de Créditos:</strong></td>
        <td>{{ $asigc->asigt->	n_cred_asigt }}</td>
        <td></td>
        <td></td>
</tr>
</table>
</center>
<style>
    .myselect {
  width: 100%;
  overflow: auto;
  text-overflow: ellipsis;
  min-height: 300px;
  max-height: 300px;
}

.myselect option {
  white-space: pre-wrap;
  width: 100%;
  border-bottom: 1px #ccc solid;
  /* This doesn't work. */
}
  select[multiple], select[size] {
    max-width: 100% !important;
}
 #formulario select {
    width: 100% !important;
  }
 #origen option, #destino option  {
    white-space: pre-wrap !important;
    word-break: break-word !important;
    width: 100% !important;
    white-space: -webkit-pre-wrap !important;
    white-space: -moz-pre-wrap !important;
    margin: 5px 5px !important;
}
select[multiple], select[size] {
    height: auto;
    height: -webkit-fill-available;
}
</style>
<script>
document.addEventListener("DOMContentLoaded", function(event) {
  por_cump_desem_gen.addEventListener('input',function(){
    range_por_cump_desem_gen.innerHTML = this.value;
    this.title = this.value+"%";
  });
  seleccionar_tems();
  $("#formulario select option").css("white-space", "pre-wrap;");
});
function seleccionar_tems(){
    var origen = document.getElementById('origen');
    var destino = document.getElementById('destino');
    var temt_cumpl = document.getElementById('temt_cumpl');
    var temt_n_cumpl = document.getElementById('temt_n_cumpl');
    temt_n_cumpl.value = '';
    var no_cumplidas = [];
    for(x in origen.options){
        if (origen.options[x].value!=null)
            no_cumplidas.push(origen.options[x].value);
    }
    temt_n_cumpl.value = no_cumplidas.join(',');
    temt_cumpl.value = '';
    var cumplidas = [];
    for(x in destino.options){
        if (destino.options[x].value!=null)
            cumplidas.push(destino.options[x].value);
    }
    temt_cumpl.value = cumplidas.join(',');
    /*
    temt_cumpl.value = '';
    destino
    */
}

</script>
<br>
<p><strong>2. CUMPLIMIENTO EN DESARROLLO DE LOS CONTENIDOS Y TEMÁTICAS</strong></p>
<center style="margin:0 auto;">
<div class="row" action="" method="post" id="formulario">
<div class="col-md-5">
<p><strong>TEMÁTICA (S) NO CUMPLIDA (S) EN LA ASIGNATURA</strong></p>
<select name="origen[]" class="myselect" id="origen" multiple="multiple" size="8" style=" max-width: 100% !important;">
@foreach ($contasigts as $contasigtsi)
<?php 
   $arr_temt_cumpl = explode(",",$infofin->temt_cumpl);
  $arr_temt_n_cumpl = explode(",",$infofin->temt_n_cumpl);
//if (in_array($contasigtsi->id,$arr_temt_n_cumpl)){
if (!in_array($contasigtsi->id,$arr_temt_cumpl)){
?>
<option style="
    word-break: break-word !important;
    width: 100% !important;
    white-space: pre-wrap !important;
    white-space: -webkit-pre-wrap !important;
    white-space: -moz-pre-wrap !important;
    margin: 5px 5px !important;
" value="{{ $contasigtsi->id }}" title="Seleccione de esta lista las temáticas que cumplió, y utilice los botones para pasarlas a la lista de temáticas cumplidas">{{ strip_tags(str_replace("</"," </",$contasigtsi->tem_cap_cont_asigt)) }}</option>
<?php } ?>
@endforeach
</select>
</div>
<div class="col-md-1">
<p><strong><br></strong></p>
<input type="button" class="btn btn-success seleccionar_tems pasar izq" value="Pasar »"><br />
<input type="button" class="btn btn-primary seleccionar_tems quitar der" value="« Quitar"><br />
<input type="button" class="btn btn-success seleccionar_tems pasartodos izq" value="Todos »"><br />
<input type="button" class="btn btn-primary seleccionar_tems quitartodos der" value="« Todos">
</div>
<div class="col-md-5">
<p><strong>TEMÁTICA (S) APLICADA (S) Y CUMPLIDA (S) EN LA ASIGNATURA</strong></p>
<select name="destino[]" class="myselect" id="destino" multiple="multiple" size="8"  style=" max-width: 100% !important;">
@foreach ($contasigts as $contasigtsi)
<?php $arr_temt_cumpl = explode(",",$infofin->temt_cumpl);
if (in_array($contasigtsi->id,$arr_temt_cumpl)){
?>
<option style="
    word-break: break-word !important;
    width: 100% !important;
    white-space: pre-wrap !important;
    white-space: -webkit-pre-wrap !important;
    white-space: -moz-pre-wrap !important;
    margin: 5px 5px !important;
"  value="{{ $contasigtsi->id }}" title="Seleccione de esta lista las temáticas que no cumplió, y utilice los botones para pasarlas a la lista de temáticas no cumplidas">{{ strip_tags(str_replace("</"," </",$contasigtsi->tem_cap_cont_asigt)) }}</option>
<?php } ?>
@endforeach
</select>
</div>
</div>
</center>
<span style="display:none;">
<p>
        <strong>TEMÁTICA (S) APLICADA (S) Y CUMPLIDA (S) EN LA ASIGNATURA:</strong>
</p>
<p>
        <textarea class="form-control" id="temt_cumpl" name="temt_cumpl">{{ $infofin->temt_cumpl }}</textarea>
</p>
<p>
        <strong>TEMÁTICA (S) NO CUMPLIDA (S) EN LA ASIGNATURA:</strong>
</p>
<p>
            <textarea class="form-control" id="temt_n_cumpl" name="temt_n_cumpl">{{ $infofin->temt_n_cumpl }}</textarea>
</p>
</span>
        
<p><strong>3. PROCESO DE EVALUACIÓN</strong> (Describa las actividades evaluativas y su porcentaje respectivo para definir la nota final del curso)</p>
<?php
$porcentaje_proce_eva = json_decode($infofin->porcentaje_proce_eva,true);
$act_proc_eva = json_decode($infofin->act_proc_eva,true);
?>
<div id="act_eva">
@if (count($act_proc_eva)>0)
@foreach ($act_proc_eva as $id => $act_proc_eva_i)
<div class="row">
    
    
       <div class="col-md-7">
<p><strong>Actividad</strong></p>
<p>
        <input type="text" class="form-control" name="act_proc_eva[]" value="{{ $act_proc_eva_i }}">
</p>
           </div>
        <div class="col-md-4">
            <p><strong>Porcentaje</strong></p>
<p>
   
   <input type="number" step="1" min="0"  max="100" class="form-control" name="porcentaje_proce_eva[]" value="{{ $porcentaje_proce_eva[$id] }}">
</p>
     </div>
     <div class="col-md-1"><br><br><a onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);">Quitar</a></div>
</div>
@endforeach
@endif
</div>

<div class="row">
<div class="col-md-12">
  <script>

      
    function nueva_act_eva(){
      var act_eva = document.getElementById("act_eva");
      var s = '';
      var row = document.createElement("div");
     //s += '<div class="row">';
      s += '<div class="col-md-7">';
s += '<p><strong>Actividad</strong></p>';
s += '<p><input type="text" class="form-control" name="act_proc_eva[]" value=""></p>';
s += '</div>';
        s += '<div class="col-md-4">';
            s += '<p><strong>Porcentaje</strong></p>';
s += '<p><input type="number" step="1" min="0"  max="100" class="form-control" name="porcentaje_proce_eva[]" value=""></p>';
     s += '</div>';
s += '<div class="col-md-1"><br><br><a onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);">Quitar</a></div>';
      row.className = 'row';
      row.innerHTML=s;
      act_eva.append(row);
    }
  </script>
  <button type="button" onclick="nueva_act_eva();">
    Nueva Actividad
  </button>
  </div>
<div class="col-md-12">
  
  <p><strong>Observaciones</strong></p>
<p>
        <textarea class="form-control" name="id_proc_eva">{{ $infofin->id_proc_eva }}</textarea>
</p>
      </div>
          </div>
<p><strong>4. DESCRIPCIÓN DEL CURSO - DESEMPEÑO GENERAL DE LOS ESTUDIANTES</strong> (Describa el grupo de estudiantes ej. el total de estudiantes que asistieron a clases, que porcentaje cupmlió con los compromisos académicos, cuál fué el promedio del grupo, cuál fué la nota mínima , la nota máxima etc.)</p>
    
<hr>
<div class="col-md-3">
            <p><strong>Total de estudiantes matriculados al curso</strong></p>
</div>
<div class="col-md-3">  
  <p>
 <input type="number" step="1" min="0" class="form-control" name="total_est_curso" value="{{ $infofin->total_est_curso }}">
</p>
</div>
         
<div class="col-md-3">
   <p><strong>Porcentaje de los compromisos académicos cupmlidos: </strong></p>
  </div>
 <div class="col-md-3">
   <span id="range_por_cump_desem_gen">{{ $infofin->por_cump_desem_gen }}</span>%
   <input class="ps7" id="por_cump_desem_gen" type="range" style="width:100%;margin-bottom:10px;" class="form-control" name="por_cump_desem_gen" value="{{ $infofin->por_cump_desem_gen }}">
<br>
       </div>
       
<div class="col-md-3">
            <p><strong>Total de estudiantes que asistieron a clases</strong></p>
</div>
<div class="col-md-3">  
  <p>
 <input type="number" step="1" class="form-control" name="total_est_desem_gen" value="{{ $infofin->total_est_desem_gen }}">
</p>
</div>



        <div class="col-md-3">
            <p><strong>Promedio del grupo</strong></p>
            </div>
        <div class="col-md-3">
<p>
  <input type="number"  step=".1" min="0" max="5" class="form-control" name="prom_grup_desem_gen" value="{{ $infofin->prom_grup_desem_gen }}">
</p><br>
        </div>
        <div class="col-md-3">
            <p><strong>Nota promedio de estudiantes que perdieron</strong></p>
            </div>
        <div class="col-md-3">
<p>
   <input type="number"  step=".1" min="0" max="5" class="form-control" name="nota_perdieron_desem_gen" value="{{ $infofin->nota_perdieron_desem_gen }}">
</p>
        </div>
        <div class="col-md-3">
            <p><strong>Nota promedio de estudiantes que aprobaron</strong></p>
            </div>
        <div class="col-md-3">
<p>
  <input type="number"  step=".1" min="0" max="5" class="form-control" name="nota_est_pasaron_desem_gen" value="{{ $infofin->nota_est_pasaron_desem_gen }}">
</p><br>
        </div>
       
       <div class="col-md-3">
  
            <p><strong>Nota máxima</strong></p>
       </div>
  <div class="col-md-3">
            <p>
                <input type="number" step=".1" min="0" max="5" class="form-control" name="nota_max_desem_gen" value="{{ $infofin->nota_max_desem_gen }}">
            </p>
        </div>
        <div class="col-md-3">
            <p><strong>Nota mínima</strong></p>
        </div>
        <div class="col-md-3">
            <p>
<input type="number"  step=".1" min="0" max="5" class="form-control" name="nota_min_desem_gen" value="{{ $infofin->nota_min_desem_gen }}">
            </p><br>
        </div>
        <div class="col-md-12">
            <p><strong>Observaciones acerca del desempeño general del estudiante</strong></p>
<p>
        <textarea class="form-control" name="observ_desem_gen">{{ $infofin->observ_desem_gen }}</textarea>
</p>
        </div>
<hr>  
<br>
<hr>
 <div class="col-md-12">
   
     
<p><strong>5. DESARROLLO DE COMPETENCIAS:</strong> (Describa que competencias desarrolló en sus estudiantes de acuerdo al área que usted planteó en el curso o asignatura)</p>
<p>
        <textarea class="form-control" name="id_desllo_comp">{{ $infofin->id_desllo_comp }}</textarea>
</p>
<p><strong>6. AUTOEVALUACIÓN:</strong> (Describa sus conclusiones y fortalezas en el desarrollo del curso o asignatura)</p>
<p>
        <textarea class="form-control" name="autoeva">{{ $infofin->autoeva }}</textarea>
</p>
<p><strong>7. OBSERVACIONES Y/O SUGERENCIAS:</strong></p>
<p>
        <textarea class="form-control" name="observ">{{ $infofin->observ }}</textarea>
</p>
<p><strong>8. BIBLIOGRAFÍA O TEXTOS RECOMENDADOS</strong> (Relacionar qué documentos se sugieren para adquisición o actualización del material bibliográfico):</p>
<p>
        <textarea class="form-control" name="biblio">{{ $infofin->biblio }}</textarea>
</p>
    </div>
       <div class="col-md-12">
        <p>
            <button type="submit" class="btn btn-primary">
                {{ __('Guardar') }}
            </button>
        </p>
    </div>
</form>
 <span style="display:none;" >

          <input name="estado" value="En Proceso" type="hidden">
          <input name="estado" value="Listo" id="switch-state" type="checkbox" @if($infofin->estado=="Listo") checked @endif >
    {{$infofin->estado}}
    </span>
    <div class="row">
      <div class="col-md-12">
        <center>
        <h2 class="h4">Estado de esta Informe Final</h2>
        <p>
        </p>
        <div class="btn-group">
          <button type="button" onclick="return estado(this, false)" id="enproceso" class="estado btn btn-warning">En Proceso</button>
          <button type="button" onclick="return estado(this, true)" id="listo" class="estado btn btn-success">Listo</button>
        </div>
          </center>
      </div>
    </div>
    <script>
      function estado(obj, estado){
        var label_estado = estado ?  'Listo' : 'En Proceso';
        if(confirm("Esta seguro que desea cambiar a estado "+label_estado)){
               guardar_estado('info_fin', 'estado', label_estado, estado,"switch-state","{{route('estado_doc',[$infofin->id,$infofin->id_asigt])}}");
           }else{
             return false;
           }
      }    
      document.addEventListener("DOMContentLoaded", function(event) {
        alplicar_estilos();
      });
    </script>
@endsection