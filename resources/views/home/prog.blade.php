@extends('layouts.app')

@section('content')
<div class="container">

    <div class="row">
        <div class="col-md-8 col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Programación Temática</div>

                <div class="panel-body">
                    <h2>Facultades</h2>
                    <ul>
                       
                   @foreach ($fac as $item_fac)
                   <li><a href="?fac={{ $item_fac->id }}">{{ $item_fac->nom_fac }}</a></li>
                   @endforeach
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
<style>
.card {
    margin: 3px 1px !important;
}
</style>
</div>
@endsection
