@extends('layouts.app')

@section('content')
 <div class="row">
 <div class="col-sm-3"></div>
 <div class="col-sm-6">
   <center><img src="{{ secure_asset('img/logo-udenar.png')}}" style="
    width: 70px;
"></center>
            <span id="titulo_logo"><center>Universidad <span class="m10">de</span></center><center><strong>Nariño</strong></center></span>
    <h1 class="text-center">PROGRAMACIÓN TEMÁTICA</h1>
    <h4 class="text-center">Gestión y Seguimiento</h4>
    <hr>
    <h2 class="text-center">Iniciar Sesión</h2>

    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}

        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
            <label for="email" class="col-md-4 control-label">Correo Electrónico</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-4 control-label">Contraseña</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <!--div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <div class="checkbox">
                    <label>
                        <input onclick="document.querySelector('#div_token').style.display = this.checked ? 'block' : 'none';if (this.checked){ document.querySelector('#codigo_token').setAttribute('required','required')}else{ document.querySelector('#codigo_token').removeAttribute('required')};" type="checkbox" name="estudiante" {{ old('estudiante') ? 'checked' : '' }}> Estudiante
                    </label>
                </div>
            </div>
        </div-->
        <!--div class="form-group">
            <div class="col-md-6 col-md-offset-4">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Recordarme
                    </label>
                </div>
            </div>
        </div-->

        <div class="form-group">
            <div class="col-md-8 col-md-offset-4">
                <button type="submit" class="btn btn-primary">
                    Acceder
                </button>

                <a class="btn btn-link" href="{{ route('password.request') }}">
                    Olvidó su contraseña?
                </a>
            </div>
        </div>
    </form>
</div>
 <div class="col-sm-3"></div>
 </div>
@endsection
