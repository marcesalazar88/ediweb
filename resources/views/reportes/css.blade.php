<style>
    table{
        text-align:center;
        width:100%;
    }
    table tr th, table tr td{
        padding:5px;
    }
    .titulo{
        text-align:center;
        font-weight:lighter !important;
    }
    .negrita{
         font-weight:bold !important;
    }
    .reporte *{
         font-family: "Humanst521 BT";
         font-weight:lighter;
    }
    #titulo_logo{
        text-align:center;
        margin:0 auto;
        font-weight:lighter !important;
    }
    #titulo_logo strong{
        font-weight:bold;   
    }
    .m10{
        font-weight:lighter;
        font-size:.9em;
    }
    .alineado_izquierda{
        text-align:left;
    }
    .borde_superior{
        border-top:1px solid black;
    }
    .borde_inferior{
        border-bottom:1px solid black;
    }
</style>