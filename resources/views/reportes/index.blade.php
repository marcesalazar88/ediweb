
@extends('layouts.app')

@section('style')
<style>
.adminmenu li a{
    color: #000000 !important;
    font-weight: bold !important;
}
</style>
@endsection
@section('ruta_de_migas')
<nav class="navbar navbar-default">
<ol class="breadcrumb navbar-left">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> Reportes</li>
</ol>
<div class="collapse navbar-collapse" id="app-navbar-collapse">
</nav>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-xs-12">
            
<span>
<!--
reportes.buscar.12324
<meta name="csrf-token" content="{{ csrf_token() }}">
<input type="hidden" name="_token" value="{{ csrf_token() }}">
-->
<form nethod="post" action="{{ route('reportes.buscar.post') }}">
 <label for="codigo">Verificar Docuento. <i style="color:gray">Escriba el código para comprobar su documento</i></label>
        <div class="input-group">
            <span class="input-group-addon" data-toggle="modal" data-target="#lectorQR"><i class="glyphicon glyphicon-qrcode"></i></span>
            <input autocomplete="off" type="text" id="codigo" name="codigo" value="" class="form-control" required aria-label="Escriba el número de comprobante en su ticket">
            <span class="input-group-addon">
                <button type="submit">
                <i class="glyphicon glyphicon-search"></i>
                </button>
                </span>
        </div>
</span>
</form>
        </div>
    </div>
</div>

<!-- Modal -->
<div id="lectorQR" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Lector QR</h4>
      </div>
      <div class="modal-body">
        
     <div class="col-md-12">
            <label><input type="checkbox" checked id="vibrar">Vibrar</label>
            <span id="log"></span>
            <div class="card">
                <div class="card-header"><span id="aviso">Listo para leer</span><br>
                <span id="actual" class="float-left" style="
    color: red;
    font-size: 24px;
    font-weight: bold;
"></span>
                
                <span id="total" class="float-right" style="
    color: red;
    font-size: 24px;
    font-weight: bold;
"></span>
                </div>

                <div class="card-body" style = "text-align: center;">
                    <video style="position: relative;width: 100%;transform: scaleX(-1);" muted playsinline id="qr-video"></video>
                </div>
                <div class="card-footer">
                    <select class="form-control" id="inversion-mode-select">
                    <option value="both">Scan both</option>
                    <option value="original">Scan original (dark QR code on bright background)</option>
                    <option value="invert">Scan with inverted colors (bright QR code on dark background)</option>
                    </select>
                    <b>Detected QR code: </b>
<span id="cam-qr-result">None</span>
<b>Last detected at: </b>
<span id="cam-qr-result-timestamp"></span>
<span style="display:none;">
    
<h1>Scan from File:</h1>
<input type="file" id="file-selector">
<b>Detected QR code: </b>
<span id="file-qr-result">None</span>
</span>
                </div>
            </div>
        </div><!--fin contetn-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<script type="module">
    import QrScanner from "{{ secure_asset('lib/qr-scanner/qr-scanner.min.js')}}";
    QrScanner.WORKER_PATH = "{{ secure_asset('lib/qr-scanner/qr-scanner-worker.min.js')}}";

    const video = document.getElementById('qr-video');
    const camHasCamera = document.getElementById('cam-has-camera');
    const camQrResult = document.getElementById('cam-qr-result');
    const camQrResultTimestamp = document.getElementById('cam-qr-result-timestamp');
    const fileSelector = document.getElementById('file-selector');
    const fileQrResult = document.getElementById('file-qr-result');
    const aviso = document.getElementById('aviso');
    function setResult(label, result) {
        if (typeof old_result !== 'undefined') var old_result = '';
        //window.open(result);
        label.textContent = result;
        camQrResultTimestamp.textContent = new Date().toString();
        label.style.color = 'teal';
        clearTimeout(label.highlightTimeout);
        label.highlightTimeout = setTimeout(() => label.style.color = 'inherit', 100);
        if (old_result!=result)
        document.location.href = result;
        old_result = result;
    }
    
    if (typeof window.result_viejo === 'undefined') window.result_viejo = '';
    function setResult2(camQrResult, result){
        if (window.result_viejo != result){
            setResult(camQrResult, result);
            result_viejo = result;
        }
    }

    // ####### Web Cam Scanning #######
    QrScanner.hasCamera().then(hasCamera => camHasCamera.textContent = hasCamera);

    const scanner = new QrScanner(video, result => setResult2(camQrResult, result));
    scanner.start();

    document.getElementById('inversion-mode-select').addEventListener('change', event => {
        scanner.setInversionMode(event.target.value);
    });

    // ####### File Scanning #######

    fileSelector.addEventListener('change', event => {
        const file = fileSelector.files[0];
        if (!file) {
            return;
        }
        QrScanner.scanImage(file)
            .then(result => setResult(fileQrResult, result))
            .catch(e => setResult(fileQrResult, e || 'No QR code found.'));
    });
function vibrar(){
    var vibrar = document.getElementById('vibrar');
    if (vibrar.checked){
        
        if (window.navigator && window.navigator.vibrate) { 
            try {
                  // En caso de ser compatible 
                    navigator.vibrate (0);
                    navigator.vibrate (1000); 
                }
                catch(error) {
                   vibrar.click();
                }
                            
        } else { 
            vibrar.click();
            // En caso de no ser compatible 
            console.log('Tu dispositivo no soporta la API de vibración');
        }
    }
}
</script>
  
<style>
     canvas {
        display: none;
    }
    hr {
        margin-top: 32px;
    }
    input[type="file"] {
        display: block;
        margin-bottom: 16px;
    }
    div {
        margin-bottom: 16px;
    }
</style>
@endsection