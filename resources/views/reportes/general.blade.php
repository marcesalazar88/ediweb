@extends('layouts.app')
@section('barra_buscar')
<li><a onclick="window.print()" title="Imprimir">
<span class="glyphicon glyphicon-print"></span>
</a></li>
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> Reporte General</li>
</ol>
@endsection
@section('scripts')
<script>
  /*
function preparar_buscar_rep_geral(){
  var contenido = '{';
  var cont = 0;
  var total = $(".input_buscar").length;
  $(".input_buscar").each(function( index ) {
        $(this).each(function( index2 ) {
          contenido += '"'+this.name+'":"'+$(this).val()+'"';
          cont++;
          if(cont != total) contenido += ',';
        });
    });
    contenido += '}'
  $("#buscar").val(btoa(contenido));
  
}
$(document).ready(function(){
$(".input_buscar").keyup(function(){
  preparar_buscar_rep_geral();
  buscar_rep_geral();
});
$(".input_buscar").change(function(){
  preparar_buscar_rep_geral();
  buscar_rep_geral();
});

  
$("#buscar").change(function(){
  buscar_rep_geral();
});
$("#search-btn").click(function(){
  buscar_rep_geral();
});
$("#num_resultados").keyup(function(){
  buscar_rep_geral();
});
$("#num_resultados").change(function(){
  buscar_rep_geral();
});
$("#menu_rep_geral").addClass('active');
  preparar_buscar_rep_geral();
  buscar_rep_geral();
});
$(".tablinks").mouseup(function(){
buscar_rep_geral();
});

function buscar_rep_geral(valor='',page =1){
if (valor=='') valor = $("#buscar").val();
var resultados = $("#num_resultados").val();
var ba = $("#buscar_arreglo").val();
var num_data = 13;
var parametros = {};
$.ajax({
                dataType: 'json',
                data:  parametros,
                url:   `?json=1&ba=${ba}&page=${page}&resultados=${resultados}&valor_clave=${valor}`,
                type:  'get',
                beforeSend: function () {
                    $("#txt_resultados").html(`<tr><td colspan="${num_data}">Procesando, espere por favor...</td></tr>`);
                },
                success:  function (response) {
                    //var datos = response;//ojo Crud devuelve .data con paginate
                    //resultado = response;
                    resultado = response.resultado.data;
table = '';
table += '<table border="1" class="cssresponsive">';
table += '<thead>';
table += '<tr>';
table += '<th>Nombre</th>';
table += '<th>Apellido</th>';
table += '<th>Sede</th>';
table += '<th>Programa</th>';
table += '<th>Semestre(s)</th>';
table += '<th>Código de Asignatura</th>';
table += '<th>Nombre de Asignatura</th>';
table += '<th>Grupo</th>';
table += '<th>Horas Prácticas</th>';
table += '<th>Horas Teóricas</th>';
table += '<th>Programación Temática</th>';
table += '<th>Seguimiento</th>';
table += '<th>Informe Final</th>';
table += '</tr>';
table += '</thead>';
//for (i = 0; i < resultado.length; i++) { 
for (i in resultado){
if (resultado[i].nums_semestre== undefined) resultado[i].nums_semestre = '';
if (resultado[i].horas_practicas == null) resultado[i].horas_practicas = '';
if (resultado[i].horas_teoricas == null) resultado[i].horas_teoricas = '';
table += '<tr>';
table +=  `<td data-label="Nombre">${resultado[i].docente_fk.nombre}</td>`;
table +=  `<td data-label="Apellido">${resultado[i].docente_fk.apellido}</td>`;
table +=  `<td data-label="Sede">${resultado[i].prog.sede}</td>`;
table +=  `<td data-label="Programa">${resultado[i].prog.nom_prog}</td>`;
table +=  `<td data-label="Semestre(s)">${resultado[i].nums_semestre}</td>`;
table +=  `<td data-label="Código de Asignatura">${resultado[i].cod_asigt}</td>`;
table +=  `<td data-label="Nombre de Asignatura">${resultado[i].asigt.nom_asigt}</td>`;
table +=  `<td data-label="Grupo">${resultado[i].grupo}</td>`;
table +=  `<td data-label="Horas Prácticas">${resultado[i].horas_practicas}</td>`;
table +=  `<td data-label="Horas Teóricas">${resultado[i].horas_teoricas}</td>`;
table +=  `<td data-label="Programación Temática">${iconos(resultado[i].progtem_estado) }</td>`;
table +=  `<td data-label="Seguimiento">${iconos(resultado[i].seguimiento_estado) }</td>`;
table +=  `<td data-label="Informe Final">${iconos(resultado[i].infofinal_estado) }</td>`;
table += '</tr>';
            }//endforeach 
            //console.log(resultado);
table += '</table>';
var reporte = document.getElementById('reporte');
reporte.innerHTML=table;
ajustar_pagina(response,'rep_geral');

document.getElementById('chartdivpt').innerHTML='';
//console.log(response);
var nom_series_pt = ["Programación Temática Pendiente","Programación Temática en trámite","Programación Temática Finalizado"];
graficar("chartdivpt",response.grafico_ordenado.datos_pt,nom_series_pt);

document.getElementById('chartdivse').innerHTML='';
//console.log(response);
var nom_series_se = ["Programación Temática Pendiente","Programación Temática en trámite","Programación Temática Finalizado"];
graficar("chartdivse",response.grafico_ordenado.datos_se,nom_series_se);

document.getElementById('chartdivif').innerHTML='';
console.log(response);
var nom_series_if = ["Programación Temática Pendiente","Programación Temática en trámite","Programación Temática Finalizado"];
graficar("chartdivif",response.grafico_ordenado.datos_if,nom_series_if);

            }//success
    });
 
 */
/*
*/
}
function iconos(num){
   switch(num){
        case 0 : 
            return '<span class="color-danger glyphicon glyphicon-remove"></span>';
        break;
        case 1 : 
            return '<span class="color-success glyphicon glyphicon-ok"></span>';
        break;
        case 2 : 
            return '<span class="color-warning glyphicon glyphicon-warning-sign"></span>';
        break;
   }
}
setTimeout(function(){ openCity(event, 's1'); }, 1000);
</script>
@include('reportes.css')
<style>
.input-buscar{
    height: 27px;
    margin-top: 9px;
    background-color: rgb(255, 255, 255);
    color: black;
    width: 170px;
    border: 1px solid rgb(170, 170, 170);
    margin-right: 1px;
    border-radius: 4px;
}
    .no-print.btn{
        display:none;  
    }
    .color-danger {
        color: #bf5329 !important;
    }
    .color-success {
        color: #196c4b !important;
    }
    .color-warning {
        color: #cbb956 !important;
    }
@media print{
    .no-print{
        display:none;        
    }
}
</style>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
@endsection
@section('barra_buscar_cuerpo')
<nav class="navbar navbar-default">
    <center><h1>Buscar</h1></center>
<div class="" id="">
                    <!-- Left Side Of Navbar -->
<ul class="nav navbar-nav navbar-right">
      <li><a style="margin: 0; padding: 8px;">
    <div class="center-xs input-group sidebar-form">
    <label for="buscar">Asignatura&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_asignaturas" name="cod_asigt" class="js-example-basic-single form-control input_buscar">
      <option value="">Todas las asignaturas</option>
      @foreach ($asignaturas as $id => $asignaturas_i)
      <option value="{{$asignaturas_i->codigo_asigt}}" <?php 
      if (isset($_POST['buscar_asignaturas']) and $_POST['buscar_asignaturas'] == $asignaturas_i->codigo_asigt){
      echo " selected ";
    } ?> >{{$asignaturas_i->nom_asigt}}</option>
      @endforeach
    </select>
    </div>
    </a></li>
    <li><a style="margin: 0; padding: 8px;">
    <div class="center-xs input-group sidebar-form">
    <label for="buscar">Docente&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_docentes" name="ident_docnt" class="js-example-basic-single form-control input_buscar">
      <option value="">Todos los docentes</option>
      @foreach ($docentes as $id => $docentes_i)
      <option value="{{$docentes_i->ident_usu}}" <?php 
      if (isset($_POST['buscar_docentes']) and $_POST['buscar_docentes'] == $docentes_i->ident_usu){
      echo " selected ";
    } ?> >{{$docentes_i->nombre}} {{$docentes_i->apellido}}</option>
      @endforeach
    </select>
    </div>
    </a></li>
    <li><a style="margin: 0; padding: 8px;">
    <div class="center-xs input-group sidebar-form">
    <label for="buscar">Periodo&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_periodos" name="per_acad" class="js-example-basic-single form-control input_buscar">
      <option value="">Todos los periodos</option>
      @foreach ($periodos as $id => $periodos_i)
      <option value="{{$periodos_i->periodo}}" <?php 
      if (isset($_POST['buscar_periodos']) and $_POST['buscar_periodos'] == $periodos_i->periodo){
      echo " selected ";
      }
      if (empty($_POST) and $periodo_activo->periodo == $periodos_i->periodo){
        echo " selected ";
      }
      ?>>{{$periodos_i->periodo}}</option>
      @endforeach
    </select>
    </div>
    </a></li>
    <li><a style="margin: 0; padding: 8px;">
    <div class="center-xs input-group sidebar-form">
    <label for="buscar">Programa&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_programa" name="prog.cod_prog" class="js-example-basic-single form-control input_buscar">
      <option value="">Todos los programas</option>
      @foreach ($progs as $id => $prog_i)
      <option value="{{$prog_i->cod_prog}}" <?php 
      if (isset($_POST['buscar_programa']) and $_POST['buscar_programa'] == $prog_i->cod_prog){
      echo " selected ";
    } ?> >{{$prog_i->nom_prog}}</option>
      @endforeach
    </select>
    </div>
    </a></li>
     <!--li><a style="margin: 0; padding: 8px;">
    <div class="center-xs input-group sidebar-form">
    <label for="buscar">Estudiante&nbsp;</label><br>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar_estudiantes" name="estudiante" class="js-example-basic-single form-control input_buscar">
      <option value="">Todos los estudiantes</option>
      @foreach ($estudiantes as $id => $estudiantes_i)
      <option value="{{$estudiantes_i->ident_usu}}" <?php 
      if (isset($_POST['buscar_estudiantes']) and $_POST['buscar_estudiantes'] == $estudiantes_i->ident_usu){
      echo " selected ";
    } ?> >{{$estudiantes_i->nombre}} {{$estudiantes_i->apellido}}</option>
      @endforeach
    </select>
    </div>
    </a></li-->
    <!--li>
    <a>  
    <div class="center-xs input-group sidebar-form">
    <label for="buscar">Semestre&nbsp;</label><br>
    <select name="asigt.sem_ofrece_asigt" class="js-example-basic-single form-control input_buscar" placeholder="Buscar por Semestre">
        <option value="">Todos</option>
        <option>Primero</option>
        <option>Segundo</option>
        <option>Tercero</option>
        <option>Cuarto</option>
        <option>Quinto</option>
        <option>Sexto</option>
        <option>Septimo</option>
        <option>Octavo</option>
        <option>Noveno</option>
        <option>Decimo</option>
        <option>Trece</option>
    </select>
    </div>
    </a></li-->
    <li>
    <a>
        <div class="center-xs input-group sidebar-form" style="
    margin-top: -5px;
">
        <label for="buscar">Sede&nbsp;</label><br>
        <input type="search" class="input-buscar input_buscar" name="prog.sede" placeholder="Buscar por Sede" style="
    margin-top: 0px;
">
        </div>
    </a>
    </li>
    <!--li>
    <a>
        <div class="center-xs input-group sidebar-form">
        <label for="buscar">Grupo&nbsp;</label><br>
        <input type="search" class="input-buscar input_buscar" name="grupo" placeholder="Buscar por grupo">
        </div>
    </a>
    </li-->
    <!--li>
    <a>
        <div class="center-xs input-group sidebar-form">
        <label for="buscar">Horas Prácticas&nbsp;</label><br>
        <input type="number" class="input-buscar input_buscar" name="prog_temt.horas_practicas " placeholder="Buscar por grupo">
        </div>
    </a>
    </li-->
    <!--li>
    <a>
        <div class="center-xs input-group sidebar-form">
        <label for="buscar">Horas Teóricas&nbsp;</label><br>
        <input type="number" class="input-buscar input_buscar" name="prog_temt.horas_teoricas " placeholder="Buscar por grupo">
        </div>
    </a>
    </li-->
    <!--li>
    <a> 
    <input type="hidden" id="buscar_arreglo" value="1">
    <input type="hidden" name="flex" class="input_buscar2" value="SI">
    </a>
    </li-->
    <li>
      <div class="center-xs input-group sidebar-form">
          <label for="num_resultados">Resultados&nbsp;</label><br>
          <span class="input-group-btn" style="display:inline">
            <input type="hidden" id="buscar" class="form-control">
          <input title="Número de resultados" class="form-control" style="height: 27px;
    margin-top: 9px;background-color: rgb(255, 255, 255);color: black;width: 70px;border-bottom: 1px none transparent;border-left: 0px solid black;margin-right: 1px;border-right: 1px solid #ccd0d2;border-bottom: 1px solid #ccd0d2;background-color: #fff;
    border: 1px solid #aaa;
    border-radius: 4px;" min="1" id="num_resultados" name="num_resultados"  type="number" value="<?php 
    if (isset($_POST['num_resultados'])){
      echo $_POST['num_resultados'];
    }else{
      echo 3;
    }
    ?>" solonumeros>
          </span>
      </div>
    </li>
    <li style="
    margin: 0px;
    padding: 5px;
">
    </li>
</ul>
</nav>
@endsection
@section('content1')
 @if (Auth::user()->rol == "admin")
         <tr>
            <td colspan="2"><input type="text"  class="form-control" placeholder="Buscar por nombre o apellido"></td>
            <td></td>
            <td><input type="text"  class="form-control" placeholder="Buscar por Programa"></td>
            <td>
                
            </td>
            <td><input type="text"  class="form-control" placeholder="Buscar por Código de Asignatura"></td>
            <td><input type="text"  class="form-control" placeholder="Buscar por Nombre de Asignatura"></td>
            
            <td><input type="text"  class="form-control" placeholder="Buscar por Grupo"></td>
            <td></td>
            <td></td>
            <td>  <select class="form-control" placeholder="Buscar por Programación Temática">
                    <option value="">Seleccione</option>
                    <option value="1">Presentada</option>
                    <option value="0">No Presentada</option>
                </select>
            </td>
            <td><select class="form-control" placeholder="Buscar por Seguimiento">
                    <option value="">Seleccione</option>
                    <option value="1">Presentada</option>
                    <option value="0">No Presentada</option>
                </select></td>
            <td><select class="form-control" placeholder="Buscar por Informe Final">
                    <option value="">Seleccione</option>
                    <option value="1">Presentada</option>
                    <option value="0">No Presentada</option>
                </select></td>
        </tr>
        @endif
@endsection
@section('content')
<center>
<div class="tab hidden-print">
  <button class="tablinks btn btn-primary" onclick="openCity(event, 's1')">Resultados</button>
  <button class="tablinks btn btn-secondary" onclick="openCity(event, 's2')">Gráfico Programación Temática</button>
  <button class="tablinks btn btn-info" onclick="openCity(event, 's3')">Gráfico Seguimiento</button>
  <button class="tablinks btn btn-success" onclick="openCity(event, 's4')">Gráfico Informe Final</button>
</div>
</center>
<center>
<div id="s1" class="tabcontent">
<span id="reporte">
        <table border="1">
        <tr>
            <th>Nombre</th>
            <th>Sede</th>
            <th>Programa</th>
            <th><span class="hidden-print">Semestre(s)</span><span class="visible-print">Sem</span></th>
            <th>Código de Asignatura</th>
            <th>Nombre de Asignatura</th>
            <th>Grupo</th>
            <th><span class="hidden-print">Horas Prácticas</span><span class="visible-print">H/P</span></th>
            <th><span class="hidden-print">Horas Teóricas</span><span class="visible-print">H/T</span></th>
            <th><span class="hidden-print">Programación Temática</span><span class="visible-print">PT</span></th>
            <th><span class="hidden-print">Seguimiento</span><span class="visible-print">S</span></th>
            <th><span class="hidden-print">Informe Final</span><span class="visible-print">IF</span></th>
        </tr>
        <?php 
        $resultado = $resultado['resultado']; 
        ?>
        @foreach ($resultado as $id => $resultado_i)
        <tr>
            <td>{{ $resultado[$id]->docente_fk->nombre }} {{ $resultado[$id]->docente_fk->apellido }}</td>
            <td>{{ $resultado[$id]->prog->sede }}</td>
            <td>{{ $resultado[$id]->prog->nom_prog }}</td>
            <td>{{ $resultado[$id]->nums_semestre }}</td>
            <td>{{ $resultado[$id]->cod_asigt }}</td>
            <td>{{ $resultado[$id]->asigt->nom_asigt }}</td>
            <td>{{ $resultado[$id]->grupo }}</td>
            <td>{{ $resultado[$id]->horas_practicas }}</td>
            <td>{{ $resultado[$id]->horas_teoricas }}</td>
            <td><?php echo $resultado->iconos[$resultado[$id]->progtem_estado] ?></td>
            <td><?php echo $resultado->iconos[$resultado[$id]->seguimiento_estado] ?></td>
            <td><?php echo $resultado->iconos[$resultado[$id]->infofinal_estado] ?></td>
        </tr>
        @endforeach 
            </table>
        </span>
<center>
<span id="txt_resultados"></span>
<span id="area_pagination">
{{ $resultado->links('layouts.paginate_post') }}
</span>
</center>
    </center>
</div><!-- fin s1-->
<!--
<th><span class="hidden-print">Semestre(s)</span><span class="visible-print">Sem</span></th>
<th><span class="hidden-print">Horas Prácticas</span><span class="visible-print">H/P</span></th>
<th><span class="hidden-print">Horas Teóricas</span><span class="visible-print">H/T</span></th>
<th><span class="hidden-print">Programación Temática</span><span class="visible-print">PT</span></th>
<th><span class="hidden-print">Seguimiento</span><span class="visible-print">S</span></th>
<th><span class="hidden-print">Informe Final</span><span class="visible-print">IF</span></th>
-->

<!-- Styles -->
<style>
#chartdiv {
  width: 100%;
  height: 500px;
}
</style>
<!-- Chart code -->
<script>
var datos_pt = [
<?php foreach($asignaturas_grafico as $asignatura){?>  
  {
    category: "<?php echo $asignatura['nom_asigt'] ?>",
    value1: <?php echo floor(100*$asignatura['progtem_estado0']/$asignatura['progtem_estadot']); ?>,
    value2: <?php echo floor(100*$asignatura['progtem_estado1']/$asignatura['progtem_estadot']); ?>,
    value3: <?php echo floor(100*$asignatura['progtem_estado2']/$asignatura['progtem_estadot']); ?>
  },
<?php } ?>
];
var nom_series_pt = ["Programación Temática Pendiente","Programación Temática en trámite","Programación Temática Finalizado"];
graficar("chartdivpt",datos_pt,nom_series_pt);

var datos_se = [
<?php foreach($asignaturas_grafico as $asignatura){?>  
  {
    category: "<?php echo $asignatura['nom_asigt'] ?>",
    value1: <?php echo floor(100*$asignatura['seguimiento_estado0']/$asignatura['seguimiento_estadot']); ?>,
    value2: <?php echo floor(100*$asignatura['seguimiento_estado2']/$asignatura['seguimiento_estadot']); ?>,
    value3: <?php echo floor(100*$asignatura['seguimiento_estado1']/$asignatura['seguimiento_estadot']); ?>
  },
<?php } ?>
];
var nom_series_se = ["Seguimiento Pendiente","Seguimiento en trámite","Seguimiento Finalizado"];
graficar("chartdivse",datos_se,nom_series_se);

var datos_if = [
<?php foreach($asignaturas_grafico as $asignatura){?>  
  {
    category: "<?php echo $asignatura['nom_asigt'] ?>",
    value1: <?php echo floor(100*$asignatura['infofinal_estado0']/$asignatura['infofinal_estadot']); ?>,
    value2: <?php echo floor(100*$asignatura['infofinal_estado1']/$asignatura['infofinal_estadot']); ?>,
    value3: <?php echo floor(100*$asignatura['infofinal_estado2']/$asignatura['infofinal_estadot']); ?>
  },
<?php } ?>
];
var nom_series_if = ["Seguimiento Pendiente","Seguimiento en trámite","Seguimiento Finalizado"];
graficar("chartdivif",datos_if,nom_series_if);

function graficar(id_div,datos,nom_series){
am4core.ready(function() {
// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

var chart = am4core.create(id_div, am4charts.XYChart);
chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

chart.data = datos;

chart.colors.step = 2;
chart.padding(30, 30, 10, 30);
chart.legend = new am4charts.Legend();

var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "category";
categoryAxis.renderer.grid.template.location = 0;

var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
valueAxis.min = 0;
valueAxis.max = 100;
valueAxis.strictMinMax = true;
valueAxis.calculateTotals = true;
valueAxis.renderer.minWidth = 50;


var series1 = chart.series.push(new am4charts.ColumnSeries());
series1.columns.template.width = am4core.percent(80);
series1.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series1.name = nom_series[0];
series1.dataFields.categoryX = "category";
series1.dataFields.valueY = "value1";
series1.dataFields.valueYShow = "totalPercent";
series1.dataItems.template.locations.categoryX = 0.5;
series1.stacked = true;
series1.tooltip.pointerOrientation = "vertical";

var bullet1 = series1.bullets.push(new am4charts.LabelBullet());
bullet1.interactionsEnabled = false;
bullet1.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet1.label.fill = am4core.color("#ffffff");
bullet1.locationY = 0.5;

var series2 = chart.series.push(new am4charts.ColumnSeries());
series2.columns.template.width = am4core.percent(80);
series2.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series2.name = nom_series[1];
series2.dataFields.categoryX = "category";
series2.dataFields.valueY = "value2";
series2.dataFields.valueYShow = "totalPercent";
series2.dataItems.template.locations.categoryX = 0.5;
series2.stacked = true;
series2.tooltip.pointerOrientation = "vertical";

var bullet2 = series2.bullets.push(new am4charts.LabelBullet());
bullet2.interactionsEnabled = false;
bullet2.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet2.locationY = 0.5;
bullet2.label.fill = am4core.color("#ffffff");

var series3 = chart.series.push(new am4charts.ColumnSeries());
series3.columns.template.width = am4core.percent(80);
series3.columns.template.tooltipText =
  "{name}: {valueY.totalPercent.formatNumber('#.00')}%";
series3.name = nom_series[2];
series3.dataFields.categoryX = "category";
series3.dataFields.valueY = "value3";
series3.dataFields.valueYShow = "totalPercent";
series3.dataItems.template.locations.categoryX = 0.5;
series3.stacked = true;
series3.tooltip.pointerOrientation = "vertical";

var bullet3 = series3.bullets.push(new am4charts.LabelBullet());
bullet3.interactionsEnabled = false;
bullet3.label.text = "{valueY.totalPercent.formatNumber('#.00')}%";
bullet3.locationY = 0.5;
bullet3.label.fill = am4core.color("#ffffff");

chart.scrollbarX = new am4core.Scrollbar();

}); // end am4core.ready()
}
</script>
<?php
/*
<pre>
print_r($asignaturas_grafico) 
</pre>
*/?>
<!-- HTML -->
 <div id="s2" class="tabcontent" style="display:block;">
<h1 class="hidden-print">Programación Temática</h1>
<div id="chartdivpt" class="hidden-print chartdiv" style="height: 500px;"></div>
<!-- HTML -->
   </div><!-- fin s2-->
<div id="s3" class="tabcontent" style="display:block;">
<h1 class="hidden-print">Seguimiento</h1>
<div id="chartdivse" class="hidden-print chartdiv" style="height: 500px;"></div>
<!-- HTML -->
 </div><!-- fin s3-->
<div id="s4" class="tabcontent" style="display:block;">
<h1 class="hidden-print">Informe Final</h1>
<div id="chartdivif" class="hidden-print chartdiv" style="height: 500px;"></div>
</div><!-- fin s4-->
<style>
.chartdiv {
  width: 100% !important;
  height: 500px !important;
}
</style>
@endsection