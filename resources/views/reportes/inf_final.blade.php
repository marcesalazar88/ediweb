@extends('layouts.app')
@section('vars')
{!!
$facultad = mb_strtoupper($facultad,'utf-8');
$programa = mb_strtoupper($programa,'utf-8');

!!}
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> Informe Final</li>
</ol>
@endsection
@section('barra_buscar')
<li><a onclick="window.print()" title="Imprimir">
<span class="glyphicon glyphicon-print">
</a></li>
@endsection
@section('scripts')
@include('reportes.css')
@endsection
@section('content')
@include('reportes.partials.inf_final')
  @endsection