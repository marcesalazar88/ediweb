<title>INFORME FINAL</title>
<span id="reporte">
<center>
<table border="1" style="width: 100%;">
    <tr>
        <th rowspan="4">
            <center><img src="{{ secure_asset('img/logo-udenar.png')}}"></center>
            <span id="titulo_logo"><center>Universidad <span class="m10">de</span></center><center><strong>Nariño</strong></center></span>
        </th>
        <th rowspan="4">
            <div class="titulo">FORMACIÓN ACADÉMICA</div>
            <div class="titulo">{{ $facultad }}</div>
            <div class="titulo">PROGRAMA DE {{ $programa }}</div>
            <div class="titulo negrita"><strong>INFORME FINAL SOBRE EL DESARROLLO DE LA TEMÁTICA POR ASIGNATURA</strong></div>
        </th>
        <th>Código: FOA-FR-13</th>
    </tr>
    <tr>
        <th>Página 1 de 2</th>
    </tr>
    <tr>
        <th>Versión: 1</th>
    </tr>
    <tr>
        <th>Vigente a partir de: {{Fecha::formato_fecha('2011-01-20')}}</th>
    </tr>

</table></center>
<p><strong>1. INFORMACIÓN DEL CURSO O ASIGNATURA</strong></p>
<center>
<table style="width: 100%;" border="1">
    <tr>
        <td style="width:20%"><strong>Nombre del Docente:</strong></td>
        <td style="width:20%">{{$asigc->docente_fk->name}}</td>
        <td style="width:20%"><strong>Correo Electrónico:</strong></td>
        <td style="width:20%">{{$asigc->docente_fk->email}}</td>
        <td style="width:20%" rowspan="4"><?php $ruta = route('reportes.buscar')."?codigo=".$infofin->codigo; ?>
<center>
<img style="width:35mm;height:35mm;" src="{{ route('qrcode.create',[300])}}?url={{$ruta}}">
            </center></td>
</tr>
<tr>
        <td><strong>Asignatura:</strong></td>
        <td>{{ $asigc->asigt->nom_asigt }}</td>
        <td><strong>Código de la Asignatura:</strong></td>
        <td>{{ $asigc->asigt->codigo_asigt }}</td>
</tr>
<tr>
        <td><strong>Intensidad Horaria:</strong></td>
        <td>{{ $asigc->asigt->ihs_asigt }}</td>
        <td><strong>Semestre/Nivel:</strong></td>
        <td>{{ $asigc->asigt->sem_ofrece_asigt }}</td>
</tr>
<tr>
        <td><strong>Número de Créditos:</strong></td>
        <td>{{ $asigc->asigt->n_cred_asigt }}</td>
        <td></td>
        <td></td>
</tr>
</table>
</center>
<p><strong>2. CUMPLIMIENTO EN DESARROLLO DE LOS CONTENIDOS Y TEMÁTICAS</strong></p>
<table style="width: 100%;">
<tr>
        <td style="width:50%"><strong>TEMÁTICA (S) APLICADA (S) Y CUMPLIDA (S) EN LA ASIGNATURA:</strong></td>
        <td style="width:50%"><strong>TEMÁTICA (S) NO CUMPLIDA (S) EN LA ASIGNATURA:</strong></td>
</tr>
<tr>
        <td>
            <ul>
            <?php $arr_temt_cumpl = explode(",",$infofin->temt_cumpl); ?>
            @foreach ($contasigts as $contasigtsi)
            <?php if (in_array($contasigtsi->id,$arr_temt_cumpl)){ ?>
            <li style="word-break: break-word;"><?php echo $contasigtsi->tem_cap_cont_asigt;?></li>
            <?php } ?>
            @endforeach
            </ul>
        </td>
        <td>
            <ul>
            <?php $arr_temt_n_cumpl = explode(",",$infofin->temt_n_cumpl); ?>
            @foreach ($contasigts as $contasigtsi)
            <?php if (in_array($contasigtsi->id,$arr_temt_n_cumpl)){ ?>
            <li style="word-break: break-word;"><?php echo $contasigtsi->tem_cap_cont_asigt;?></li>
            <?php } ?>
            @endforeach
            </ul>
        </td>
</tr>
</table>
<p><strong>3. PROCESO DE EVALUACIÓN</strong> (Describa las actividades evaluativas y su porcentaje respectivo para definir la nota final del curso)</p>

<?php
$porcentaje_proce_eva = json_decode($infofin->porcentaje_proce_eva,true);
$act_proc_eva = json_decode($infofin->act_proc_eva,true);
?>
<div id="act_eva">
@if (count($act_proc_eva)>0)
@foreach ($act_proc_eva as $id => $act_proc_eva_i)
<div class="row">       
<div class="col-md-7">
<p><strong>Actividad</strong></p>
<p>
        {{ $act_proc_eva_i }}
</p>
           </div>
        <div class="col-md-4">
            <p><strong>Porcentaje</strong></p>
<p>
   
   {{ $porcentaje_proce_eva[$id] }}%
</p>
     </div>
</div>
@endforeach
@endif
</div>
<p>{{ $infofin->id_proc_eva }}</p>
<p><strong>4. DESCRIPCIÓN DEL CURSO - DESEMPEÑO GENERAL DE LOS ESTUDIANTES</strong> (Describa el grupo de estudiantes ej. el total de estudiantes que asistieron a clases, que porcentaje cupmlió con los compromisos académicos, cuál fué el promedio del grupo, cuál fué la nota mínima , la nota máxima etc.)</p>
<p>El desempeño general de los estudiantes, quienes se encuenran matriculados {{$infofin->total_est_curso}} estudiantes, de los cuales {{ $infofin->total_est_desem_gen }} asistieron, la nota maxima del curso fue {{$infofin->nota_max_desem_gen}}, la mínima fue {{$infofin->nota_min_desem_gen }}, el promedio del grupo {{$infofin->prom_grup_desem_gen}}, teniendo en cuenta a los estudiantes que perdieron la nota promedio fue {{$infofin->nota_perdieron_desem_gen}}, de los que aprobaron el promedio fue {{$infofin->nota_est_pasaron_desem_gen}}; El orcentaje de los compromisos académicos cupmlidos fué de {{ $infofin->por_cump_desem_gen }}%</p>
<br>
<p>Observaciones acerca del desempeño general del estudiante: {{ $infofin->observ_desem_gen }}</p>

<br>
<p><strong>5. DESARROLLO DE COMPETENCIAS:</strong> (Describa que competencias desarrolló en sus estudiantes de acuerdo al área que usted planteó en el curso o asignatura)</p>
<p>{{ $infofin->id_desllo_comp }}</p>
<p><strong>6. AUTOEVALUACIÓN:</strong> (Describa sus conclusiones y fortalezas en el desarrollo del curso o asignatura)</p>
<p>{{ $infofin->autoeva }}</p>
<p><strong>7. OBSERVACIONES Y/O SUGERENCIAS:</strong></p>
<p>{{ $infofin->observ }}</p>
<p><strong>8. BIBLIOGRAFÍA O TEXTOS RECOMENDADOS</strong> (Relacionar qué documentos se sugieren para adquisición o actualización del material bibliográfico):</p>
<p>{{ $infofin->biblio }}</p>
<br>
<br>
<center>
<p><strong> {{ $asigc->docente_fk->name }}</strong></p>
<p>DOCENTE</p>
</center>
<br>
<footer>
 <p>Para verificar este documento, ingrese a la dirección {{route('reportes.buscar')}} donde podrá escanear el código QR o ingresar el siguiente código: <strong>{{ $infofin->codigo }}</strong></p>
</footer>