@extends('layouts.app')
@section('vars')
<?php
$page_title = 'Planes'; 
?>
@endsection

@section('barra_buscar_cuerpo')
<nav class="navbar navbar-default">

<div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
<ul class="nav navbar-nav navbar-right">
    <li><a style="margin: 0; padding: 8px;">
    <div class="input-group sidebar-form">
      <label for="buscar">Seleccione un programa&nbsp;</label>
    <select style="background-color: #FFF;color:black;width:170px;display:inline" id="buscar" class="js-example-basic-single form-control">
      @foreach ($prog as $id => $prog_i)
      <option @if($prog_i->cod_prog=="011") selected @endif value="{{$prog_i->cod_prog}}">{{$prog_i->nom_prog}}</option>
      @endforeach
    </select>
    </div>
    </a></li>
    <li>
          <span class="input-group-btn" style="display:inline">
          <input title="Número de resultados" class="form-control" style="    height: 27px;
    margin-top: 9px;background-color: rgb(255, 255, 255);color: black;width: 70px;border-bottom: 1px none transparent;border-left: 0px solid black;margin-right: 1px;border-right: 1px solid #ccd0d2;border-bottom: 1px solid #ccd0d2;" min="1" id="num_resultados"  type="number" value="3" solonumeros>
          </span>
    </li>
</ul>
</div>
</nav>
@endsection
@section('scripts')
<script>
$(document).ready(function(){
$("#buscar").keyup(function(){
  buscar_planes();
});
$("#buscar").change(function(){
  buscar_planes();
});
$("#search-btn").click(function(){
  buscar_planes();
});
$("#num_resultados").keyup(function(){
  buscar_planes();
});
$("#num_resultados").change(function(){
  buscar_planes();
});
$("#menu_planes").addClass('active');
buscar_planes();
});
</script>
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home')}}"><i class="fa fa-dashboard"></i> Inicio</a></li>
    <li class="active"><i class="fa fa-user"></i> {{ $page_title }}</li>
</ol>
@endsection
@section('content')
<center><h1>{{ $page_title }}</h1></center>
@if($botones)
<input type="hidden" id="a5d546a503db8d" value="{{ $botones ? __('true') : __('false') }}">
@endif
<div class='row'>
  <div class='col-md-12'>
  <span id="txt_resultados">
</span>
 <div id="area_pagination"></div>
    
    
  </div><!-- /.col -->
  <div class='col-md-6'>

  </div><!-- /.col -->

</div><!-- /.row -->
@endsection