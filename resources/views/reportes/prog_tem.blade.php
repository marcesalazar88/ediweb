@extends('layouts.app')
@section('barra_buscar')
<li><a onclick="window.print()" title="Imprimir">
<span class="glyphicon glyphicon-print"></span>
</a></li>
@endsection
@section('ruta_de_migas')
<ol class="breadcrumb">
    <li><a href="{{ route('home') }}"> Inicio</a></li>
    <li class="active"> Programación Temática</li>
</ol>
@endsection
@section('scripts')
@include('reportes.css')
 <style>
  .contasigt, .contasigt td, .contasigt td p, .contasigt td *{
    overflow-wrap: break-word !important;
    }
  
    .no-print.btn{
        display:none;  
    }
@media print{
    .no-print{
        display:none;        
    }
}
</style>
@endsection
@section('content')
<span id="reporte">
<center>
<table border="1">
    <tr>
        <th rowspan="4">
            <center><img src="{{ secure_asset('img/logo-udenar.png')}}"></center>
            <span id="titulo_logo"><center>Universidad <span class="m10">de</span></center><center><strong>Nariño</strong></center></span>
        </th>
        <th rowspan="4">
            <div class="titulo">FORMACIÓN ACADÉMICA</div>
            <div class="titulo">FACULTAD {{ mb_strtoupper($asigt->nom_fac,'utf-8') }}</div>
            <div class="titulo">PROGRAMA DE {{ mb_strtoupper($asigt->nom_prog,'utf-8') }}</div>
            <div class="titulo negrita"><strong>PROGRAMACIÓN TEMÁTICA ASIGNATURA</strong></div>
        </th>
        <th>Código: FOA-FR-07</th>
    </tr>
    <tr>
        <th>Página 1 de 2</th>
    </tr>
    <tr>
        <th>Versión: 4</th>
    </tr>
    <tr>
        <th>Vigente a partir de: {{ Fecha::formato_fecha('2011-01-18')}}</th>
    </tr>

</table></center>
<p><strong>1. IDENTIFICACIÓN DE LA ASIGNATURA:</strong></p>
<center>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>NOMBRE DEL DOCENTE:</strong> {{ $asigt->name }}</td>
        <td rowspan="3"><?php if($asigt->codigo and $asigt->codigo!=""){
          
          ?>
          <?php $ruta = route('reportes.buscar')."?codigo=".$asigt->codigo; ?>
<center>
<img style="width:35mm;height:35mm;" src="{{ route('qrcode.create',[300])}}?url={{$ruta}}">
            </center>
<?php } ?>
</td>
    </tr>
    <tr>
        <td class="alineado_izquierda"><strong>Correo Electrónico:</strong> {{ $asigt->email }}</td>
    </tr>

    <tr>
        <td colspan="1" class="alineado_izquierda">
          <strong>NOMBRE DE LA ASIGNATURA EN CURSO: </strong> {{ $asigt->nom_asigt }}
        </td>
    </tr>
</table>
</center>
<br>
<center>
<table border="1">
    <tr>
        <td>Código de la Asignatura:</td>
        <td colspan="4"> {{ $asigt->codigo_asigt }}
        <button style="float:right;" type="button" class="no-print btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></button>
        </td>
    </tr>
    <tr>
        <td>Semestres a los Cuales se ofrece:</td>
        <td colspan="4"> {{ $asigt->sem_ofrece_asigt }}</td>
    </tr>
    <tr>
        <td>Intensidad Horaria Semanal: {{ $asigt->ihs_asigt }}, Número de Créditos: {{ $asigt->n_cred_asigt }}</td>
        <td>Teórica:{{ $asigt->horas_practicas }}</td>
        <td>Práctica:{{ $asigt->horas_teoricas }}</td>
        <td>Adicionales:{{ $asigt->horas_adicionales }}</td>
        <td>Horas Totales:{{ $asigt->horas_practicas + $asigt->horas_teoricas + $asigt->horas_adicionales }}</td>
    </tr>
</table>
</center>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>2. JUSTIFICACIÓN:
        <button style="float:right;" type="button" class="no-print btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></button>
        <br></strong>{{ $asigt->just_asigt }}
        </td>
    </tr>
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>3. OBJETIVOS: 
        <button style="float:right;" type="button" class="no-print btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></button>
        <br>
        <p>3.1 Objetivo General</p>
        <p>{{ $asigt->obj_gen }}</p>
        <p>3.2 Objetivos Específicos</p>
        <p><?php echo $asigt->obj_esp ?></p>
        </strong>
        </td>
    </tr>
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>4. METODOLOGÍA:</strong>
        <button style="float:right;" type="button" class="no-print btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></button>
        <br>
        <?php echo  $asigt->metodologia ?>
        </td>
    </tr>
</table>
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>5. CRITERIOS DE EVALUACIÓN:</strong>
        <button style="float:right;" type="button" class="no-print btn btn-primary"><span class="glyphicon glyphicon-pencil"></span></button>
        <br>
        <?php echo  $asigt->crit_eva ?>
        </td>
    </tr>
</table>
<br>
<p><strong>6. CONTENIDO DE LA ASIGNATURA:</strong></p>
<table class="contasigt" border="1">
    <tr>
        <td><strong>Horas ó Créditos</strong></td>
        <td><strong>Tema ó Capitulo</strong></td>
        </td>
    </tr>
    <?php 
    $total_horas = 0;
    ?>
        @foreach ($asigt->asigc->asigt->contasigts as $cont_asig_i)
    <tr>
        <td>{{ $cont_asig_i->hras_cont_asigt }}</td>
        <td><?php echo $cont_asig_i->tem_cap_cont_asigt ?></td>
    </tr>
    <?php 
    $total_horas += $cont_asig_i->hras_cont_asigt;
    ?>
    @endforeach
     <tr>
        <td><strong>Total: {{ $total_horas }}</strong></td>
        <td><strong></strong></td>
    </tr>
    
</table>
<br>
<p><strong>7. PUNTO ADICIONAL Y OPCIONAL QUE APLICA A AQUELLOS PROGRAMAS QUE UTILIZAN OTROS FACTORES EN LA PROGRAMACIÓN TEMÁTICA POR ASIGNATURA, Ej. Competencias, Habilidades, etc</strong></p>
<button style="float:right;" type="button" class="no-print btn btn-primary"><span class="glyphicon glyphicon-plus"></span></button>
@if(count($asigt->punto_adics)>0)
<table border="1">
    <tr>
        <td><strong>Item</strong></td>
        <td><strong>Descripción</strong></td>
    </tr>
    @foreach ($asigt->punto_adics as $punto_adic)
    <tr>
        <td><strong>{{ $punto_adic->item }}</strong></td>
        <td><strong>{{ $punto_adic->valor }}</strong></td>
    </tr>
    @endforeach
</table>
@else
<p>No hay puntos adicionales</p>
@endif
<br>
<table border="1">
    <tr>
        <td class="alineado_izquierda"><strong>8. REFERENCIAS BIBLIOGRÁFICAS</strong>
        <button style="float:right;" type="button" class="no-print btn btn-primary"><span class="glyphicon glyphicon-plus"></span></button>
        </td>
    </tr>
    <tr class="alineado_izquierda">
        <td>
            <ul>
              @foreach($asigt->refe_biblios as $biblio)
              <li>{{ $biblio->refe_biblio }}</li>
              @endforeach
            </ul>
        </td>
    </tr>
</table>
<br>
<center>
<p><strong> {{ $asigt->name }}</strong></p>
<p>DOCENTE</p>
</center>
<br>
<footer>
 <p>Para verificar este documento, ingrese a la dirección {{route('reportes.buscar')}} donde podrá escanear el código QR o ingresar el siguiente código: <strong>{{ $asigt->codigo }}</strong></p>
</footer>
</span>
</span>
<?php if(isset($_GET['imprimir'])){ ?>
<script>
    window.print();
</script>
<?php } ?>
@endsection