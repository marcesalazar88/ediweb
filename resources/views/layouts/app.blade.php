<?php
$periodo_activo = Funciones::periodo_activo();
?><!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- -->
    @if (env('EDIV_LIMPIAR_CACHE')==true)
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
    <meta http-equiv="Pragma" content="no-cache" />
    <meta http-equiv="Expires" content="0" /> 
    @endif
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Ediv') }}</title>

    <!-- Styles -->
    <link href="{{ asset('lib/bootstrap-3.3.7-dist/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('lib/bootstrap-switch/bootstrap-switch.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('fonts/fontawesome/css/all.css') }}" rel="stylesheet"> <!--load all styles -->
    
    <link href="{{ asset('css/ediv.css') }}" rel="stylesheet">
    <link href="{{ asset('css/calendario.css') }}" rel="stylesheet">
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    <style>
      .btn.lista {
           width: 85.5px !important;
            margin: 2px 0px !important;
      }
    </style>
    <!--amcharts-->
<script src="{{ asset('lib/amcharts/core.js') }}"></script>
<script src="{{ asset('lib/amcharts/charts.js') }}"></script>
<script src="{{ asset('lib/amcharts/themes/material.js') }}"></script>
<script src="{{ asset('lib/amcharts/themes/animated.js') }}"></script>
<script src="{{ asset('lib/amcharts/lang/de_DE.js') }}"></script>
<!--amcharts-->
@yield('style')
</head>
<body>
    <div id="app">
        @include('layouts.partials.nav')
        <div id="alerts" style="position:relative;z-index:999;width:100%;top:0px;">
        @if (session('notice'))
        <div class="alert alert-info alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('notice') }}
        </div>
        @endif
        @if (session('success'))
        <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('success') }}
        </div>
        @endif
        @if (session('warning'))
        <div class="alert alert-warning alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('warning') }}
        </div>
        @endif
        @if (session('danger'))
        <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('danger') }}
        </div>
        @endif
        
        @if ($periodo_activo->estado=="" and Auth::user()->rol=="admin")
        <div class="alert alert-warning alert-dismissible">
           <strong>Advertencia!</strong> No hay un periodo activo, para activar un periodo diríjase a <a href="{{route('admin.periodo.index')}}">Periodos</a>.
        </div>
        @endif
        
        </div>
        @hasSection ('content')
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-body">
<label class="label label-success" title="Periodo Activo" style="
    margin-top: -34px;
    float: left;
    margin-left: -15px;
    position: relative;
    z-index: 999;
                                                                 display : inline-flex
">Periodo Activo: {{ $periodo_activo->periodo }}</label>
 @if (Auth::check())
<label class="label label-success" title="Ver Como:" style="
    margin-top: -34px;
    float: right;
    margin-right: -15px;
    position: relative;
    z-index: 999;
                                                      display : inline-flex
"
       id="roles" data-placement="bottom" data-toggle="popover" data-container="body" data-placement="left" data-trigger="click" type="button" data-html="true"
       >Rol: {{ Funciones::rol(Auth::user()->rol) }}</label>
<!--inicio html-->
<div id="popover-roles" class="hide">
  <ul style="list-style:none;">
    @if (Auth::check())
          <?php $user_roles = explode(",",Auth::user()->roles);  ?>
          @foreach ($user_roles as $user_rol)
            <li><a href="{{ route('rol',[$user_rol]) }}">{{ Funciones::rol($user_rol)}}</a></li>
          @endforeach 
    @endif
  </ul>
</div>
<!--fin html-->
                   @endif
                  
                  <span class="hidden-print">
                    @yield('ruta_de_migas')
                  </span>
                  <span class="hidden-print">
                    @yield('barra_buscar_cuerpo')
                  </span>
                    @yield('content')
                </div>
            </div>
        </div>
        @endif
    </div>
<footer>
  <div class="row">
  <div class="col-md-3">
   <center><img src="{{ secure_asset('img/logo-udenar.png')}}" style="
    width: 70px;
"></center>
            <span id="titulo_logo"><center>Universidad <span class="m10">de</span></center><center><strong>Nariño</strong></center></span>
</div>
<div class="col-md-6">
    <p>Sistema Ediweb, Software para la Gestión y Seguimiento de la programación temática por asignatura.</p>
    <p>Angie Ortíz<br> Diana Quiñonez</p>
    <p></p>
</div>
    <div class="col-md-3">
   <center><img src="{{ secure_asset('img/logo-licinfo.png')}}" style="
    width: 100px;
"></center>
</div>
  </div>
  
  </footer>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/funciones.js') }}"></script>
    <script src="{{ asset('lib//bootstrap-switch/bootstrap-switch.js') }}"></script>
    <script src="{{ asset('lib//bootstrap-switch/highlight.js') }}"></script>
    <script src="{{ asset('lib//bootstrap-switch/main.js') }}"></script>
    <script src="{{ asset('lib/select2/select2.min.js') }}"></script>
    <link href="{{ asset('lib/select2/select2.min.css') }}" rel="stylesheet" />
    
   
<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
    $(".js-example-tokenizer").select2({
    tags: true,
    tokenSeparators: [', ', ' ']
})
</script>
<script src="{{ asset('lib/stopExecutionOnTimeout/stopExecutionOnTimeout.js') }} "></script>

<!--
<script src="https://static.codepen.io/assets/common/stopExecutionOnTimeout-de7e2ef6bfefd24b79a3f68b414b87b8db5b08439cac3f1012092b2290c719cd.js"></script>
-->
<script>
    // COPY TO CLIPBOARD
// Attempts to use .execCommand('copy') on a created text field
// Falls back to a selectable alert if not supported
// Attempts to display status in Bootstrap tooltip
// ------------------------------------------------------------------------------

function copyToClipboard(text, el) {
  var copyTest = document.queryCommandSupported('copy');
  var elOriginalText = el.attr('data-original-title');

  if (copyTest === true) {
    var copyTextArea = document.createElement("textarea");
    copyTextArea.value = text;
    document.body.appendChild(copyTextArea);
    copyTextArea.select();
    try {
      var successful = document.execCommand('copy');
      var msg = successful ? text+': Copiado a portapapeles!' : 'Upss, no se ha copiado!';
      el.attr('data-original-title', msg).tooltip('show');
    } catch (err) {
      console.log('Upss, esta desactivada la función ´copy´');
    }
    document.body.removeChild(copyTextArea);
    el.attr('data-original-title', elOriginalText);
  } else {
    // Fallback if browser doesn't support .execCommand('copy')
    window.prompt("Para copiar al portapapeles utilice: Ctrl+C o Command+C", text);
  }
}

$(document).ready(function() {
  // Initialize
  // ---------------------------------------------------------------------

  // Tooltips
  // Requires Bootstrap 3 for functionality
  $('.js-tooltip').tooltip();

  // Copy to clipboard
  // Grab any text in the attribute 'data-copy' and pass it to the 
  // copy function
  $('.js-copy').click(function() {
    var text = $(this).attr('data-copy');
    var el = $(this);
    copyToClipboard(text, el);
  });
});
  
$(document).ready(function() {
  //atajos de teclado
$(document).bind('keydown', function(e) {
tecla=(document.all) ? e.keyCode : e.which;
if (tecla==65 && e.shiftKey && e.altKey){ //Shift + Alt + Tecla A => admin
document.location.href='{{ route('rol','admin') }}';return false;}
if (tecla==68 && e.shiftKey && e.altKey){ //Shift + Alt + Tecla D => director
document.location.href='{{ route('rol','director') }}';return false;}
if (tecla==84 && e.shiftKey && e.altKey){ //Shift + Alt + Tecla T => docente
document.location.href='{{ route('rol','docente') }}';return false;}
if (tecla==69 && e.shiftKey && e.altKey){ //Shift + Alt + Tecla E => estudiante
document.location.href='{{ route('rol','estudiante') }}';return false;}
if (tecla==80 && e.ctrlKey){return false;}//Control + Tecla P => nada
});//fin bind('keydown')
  
$("#roles[data-toggle=popover]").popover({
    html: true, 
	content: function() {
          return $('#popover-roles').html();
        }
});
});//fin ready
</script>
    @yield('scripts')
<script>
  $(document).ready(function() {
  $("#formulario #origen>option").css("white-space",'pre-wrap');
  $("#formulario #destino>option").css("white-space",'pre-wrap');
    
  $("#formulario #origen").css("min-width",'100%');
  $("#formulario #origen").css("height",'auto');
  $("#formulario #origen").css("height",'-webkit-fill-available');
    
  $("#formulario #destino").css("min-width",'100%');
  $("#formulario #destino").css("height",'auto');
  $("#formulario #destino").css("height",'-webkit-fill-available');
 });
</script>
</body>
</html>
