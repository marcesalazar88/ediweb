<div style="text-align: center;">Total Resultados: {{ $paginator->total() }}</div>
@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="disabled"><span>&laquo;</span></li>
        @else
            <?php $idform = uniqid("form_"); ?>
            <form method="post" id="{{ $idform }}" action="{{ $paginator->previousPageUrl() }}" style="display:inline">
                <?php echo Funciones::params_post(); ?>
                <meta name="csrf-token" content="{{ csrf_token() }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
            <li><a onclick="document.getElementById('{{ $idform }}').submit();" rel="prev">&laquo;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active"><span>{{ $page }}</span></li>
                    @else
                    <?php $idform = uniqid("form_"); ?>
                    <form method="post" id="{{ $idform }}" action="{{ $url }}" style="display:inline">
                        <?php echo Funciones::params_post(); ?>
                        <meta name="csrf-token" content="{{ csrf_token() }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </form>
                    <li><a onclick="document.getElementById('{{ $idform }}').submit();">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <?php $idform = uniqid("form_"); ?>
            <form method="post" id="{{ $idform }}" action="{{ $paginator->nextPageUrl() }}" style="display:inline">
                <?php echo Funciones::params_post(); ?>
                <meta name="csrf-token" content="{{ csrf_token() }}">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
            </form>
             <li><a onclick="document.getElementById('{{ $idform }}').submit();" rel="next">&raquo;</a></li>
        @else
            <li class="disabled"><span>&raquo;</span></li>
        @endif
    </ul>
@endif