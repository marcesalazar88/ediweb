Apreciado docente  {{$user->name}},
 
Como Director del departamento de {{$depto->nom_depto}}, le recuerdo que ya se encuentra vigente en el sistema Ediweb la revision y complementacion del Informe Final de la Programación por Asignatura de la cátedra {{$nom_asigt}} para el periodo académico {{$periodo}},

Para complementar la información puede acceder en el siguiente enlace {{env('APP_URL')}}
El docente debe diligenciar el formato hasta el {{$info_fin->fecha_revision_doc}}
