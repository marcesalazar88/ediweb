Para: Profesores {{$depto->nom_depto}} Universidad de Nariño<br>
De {{$director->name}}, Director {{$depto->nom_depto}}<br>
Asunto: Observaciones semestre {{$periodo->periodo}}<br>
<br>
Reciban Uds. un abrazo de bienvenida en el inicio del periodo académico
{{$periodo->periodo}}.<br>
Por un desarrollo adecuado de la labor docente, esta comunicación sin duda alguna es de mucha utilidad para los docentes <strong>que por primera vez ejercen su profesión en esta Unidad Académica;</strong> Esta comunicación podría denominarse la “carta de navegación” para el presente semestre.
 Como primera medida informamos de los formatos que dirigen los procesos o los evidencian, con sus respectivos plazos de entrega y las indicaciones pertinentes:
<br>
<ol>
  <li>Mediante Acuerdo emanado por el Honorable Consejo Superior, el cual es de obligatorio cumplimiento para los docentes de la Institución.</li>
  <li>INDICACIONES GENERALES
  <ul>
  <li>Sin excepciones, el semestre inicia {{$periodo->fecha_inicio}} . Por favor, comencemos de manera puntual con nuestro compromiso institucional.</li>
  <li>Recordemos que LOS HORARIOS SON INMODIFICABLES y las fechas estipuladas en los procesos de formación Académica.</li>
  </ul>
  </li>
  <li>PROGRAMAS DE ASIGNATURAS
  <ul>
  <li>El contenido a desarrollarse en cada asignatura será el suministrado oficialmente por el Departamento.</li>
  <li>Según el Estatuto Estudiantil vigente, el docente en el primer día de clase debe socializar el programa de la asignatura y concertar con el grupo la forma en que se realizará la evaluación, revise muy bien el documento y diligencie la información pertinente en el sistema EDIWEB, hasta el {{$prog_temt->fecha_revision_doc}}</li>
  </ul>
  </li>
  <li>SEGUIMIENTO AL DESARROLLO DE LAS ASIGNATURAS
  <ul>
  <li>El docente debe diligenciar el formato, el representante estudiantil del curso, revisa y aprueba los aspectos de la asignatura en las fechas establecidas por el sistema EDIWEB.</li>  
  </ul>
  </li>
  <li>INFORMES FINALES SOBRE EL DESARROLLO DE CADA CURSO
    <ul>
    <li>Por cada asignatura ofrecida, el docente deberá diligenciar un informe detallado de la forma en que transcurrió el curso: observaciones, sugerencias, porcentajes de cumplimiento de la asignatura, aprobados y reprobados, etc. incluso, si por situaciones ajenas al docente no se pudo desarrollar el total de la temática, es necesario conocer esta situación para implementar algún correctivo posterior, hasta el {{$info_final->fecha_revision_doc}}</li>  
    </ul>
  </li>
  <li>Su asignación es {{$asigc->asigt->nom_asigt}} Grupo: {{$asigc->grupo}}, las modificaciones serán realizadas serán enviadas a su correo</li>
</ol>

<br>
DE NUESTRO COMPROMISO Y CUMPLIMIENTO DEPENDE QUE NUESTRA UNIVERSIDAD Y NUESTROS<br>
<br>
PROGRAMAS SEAN RECONOCIDOS.<br>