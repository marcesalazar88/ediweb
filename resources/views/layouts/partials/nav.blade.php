<?php
use App\Links;
$links = new Links();
$links = $links->all();
if (Auth::check()){ 
Auth::user()->notificaciones_fk();
}
?>
<nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        <img id="logo" height="60px" alt="{{ config('app.name', 'Ediv') }}" src="{{ secure_asset('favicon.ico') }}" rel="stylesheet">
                        
                    </a>
                  
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        @if (Auth::check())
                            <li>
                                <a href="{{ secure_asset('home') }}">
                                     <span class="glyphicon glyphicon-home"></span>
                                     Inicio
                                    </a>
                            </li>
                            @if (Auth::user()->rol=="admin" or Auth::user()->rol=="director")
                            <li id="menu_configuraciones">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="{{ secure_asset('admin') }}">
                                     <span class="glyphicon glyphicon-edit"></span>
                                    Configuraciones
                                    <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                      @if (Auth::user()->rol=="admin")
                                        <li>
                                            <a href="{{ secure_asset('admin/facultad') }}">
                                                <span class="glyphicon glyphicon-list"></span>
                                                Facultades
                                                </a>
                                        </li>
                                        <li>
                                            <a href="{{ secure_asset('admin/departamento') }}">
                                                <span class="glyphicon glyphicon-list-alt"></span>
                                                Departamentos
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.programa.index') }}">
                                                <span class="glyphicon glyphicon-list-alt"></span>
                                                Programas
                                            </a>
                                        </li>
                                        
                                        
                                        <li>
                                            <a href="{{ secure_asset('admin/asignatura') }}">
                                                <span class="glyphicon glyphicon-blackboard"></span>
                                                Asignaturas
                                                </a>
                                        </li>
                                      @endif
                                      <li id="menu_config_planes">
                                            <a href="{{ route('admin.planes.index') }}">
                                           <span class="glyphicon glyphicon-blackboard"></span>
                                          Planes
                                          </a>
                                        </li>
                                        <li id="menu_asignacion">
                                            <a href="{{ secure_asset('admin/asignacion') }}">
                                                <span class="glyphicon glyphicon-tasks"></span>
                                                Asignaciones
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ secure_asset('admin/control') }}">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                                Control de Fechas
                                            </a>
                                        </li>
                                      @if (Auth::user()->rol=="admin")
                                        <li>
                                            <a href="{{ secure_asset('admin/usuarios') }}">
                                                <span class="glyphicon glyphicon-user"></span>
                                                Usuarios
                                            </a>
                                        </li>
                                        <li class="divider"></li>
                                        <li>
                                            <a href="{{ secure_asset('admin/links') }}">
                                                <span class="glyphicon glyphicon-link"></span>
                                                Enlaces
                                                </a>
                                        </li>
                                        <li>
                                            <a href="{{ secure_asset('admin/periodo') }}">
                                                <span class="glyphicon glyphicon-list-alt"></span>
                                                Periodos
                                                </a>
                                        </li>
                                        @endif
                                    </ul>
                            </li>
                            @endif

                            @if (Auth::user()->rol!='estudiante')
                            <li id="menu_rep_geral">
                                <a href="{{ secure_asset('reportes/general') }}">
                                    <span class="glyphicon glyphicon-stats"></span>
                                    Reporte General
                                    </a>
                            </li>
                            @endif
                        @endif
                             <li>
                                <a href="{{ route('reportes.asignacion.flexibilidad') }}">
                                     <span class="	glyphicon glyphicon-share"></span>
                                    Flexibilidad
                                    </a>
                            </li>
                             <li id="menu_planes">
                                <a href="{{ route('reportes.programa.planes') }}">
                                     <span class="glyphicon glyphicon-blackboard"></span>
                                    Planes
                                    </a>
                            </li>
                      
                             <li id="menu_verificar">
                                <a href="{{ route('reportes') }}">
                                     <span class="glyphicon glyphicon-qrcode"></span>
                                    Verificar documento
                                    </a>
                            </li>
                        <li>
                                <a title="Enlaces de Interés" class="dropdown-toggle" data-toggle="dropdown" href="#">
                                     <span class="glyphicon glyphicon-link"></span>
                                    Enlaces
                                    <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        
                                        @foreach ($links as $link)
                                        <li>
                                            <a href="{{ $link->url }}" target="{{ $link->target }}">
                                                <?php echo $link->icono ?>
                                                {{ $link->nombre }}
                                            </a>
                                        </li>
                                        @endforeach
                                    </ul>
                            </li>
                      
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">

                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}"><span class="glyphicon glyphicon-log-in"></span>Iniciar Sesión</a></li>
                            @if (env('AUTOREGISTRO')==true)
                            <li><a href="{{ route('register') }}"><span class="glyphicon glyphicon-user"></span>Registrar</a></li>
                            @endif
                        @else
                        @yield('barra_buscar')
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                  
                                  @if (Auth::check())
                                  @if (Funciones::num()>0)
                                 <span class="badge" style="background-color: red;float:right">{{ Funciones::num() }}</span>
                                   @endif
                                   @endif
                                  
                                 <span class="glyphicon glyphicon-user"></span>
                                   <center>{{ Auth::user()->nombre }}<span class="caret"></span><br>{{ Auth::user()->apellido }}</center>
                                   <center><small style="color: #848181;">{{ Auth::user()->email }}</small></center>
                                </a>
                                <ul class="dropdown-menu" role="menu">
                                  @if (Auth::user()->rol=='admin')
                                    <li><a href="{{ route('config') }}">Configuración</a></li>
                                   @endif
                                    <li><a href="{{ route('usuarios.perfil') }}">Mi Perfil</a></li>
                                    <li><a href="{{ route('notificaciones.index') }}">Mis Notificaciones
                                  @if (Auth::check())
                                      @if (Funciones::num()>0)
                                      <span class="badge" style="background-color: red;left:0px;margin-right:0px;">{{  Funciones::num() }}</span>
                                   @endif
                                      @endif
                                      </a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Salir
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                       @yield('icono_extra_menu')
                        @endif
                    </ul>
                </div>
            </div>
        </nav>